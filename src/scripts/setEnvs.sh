#!/usr/bin/env bash

#simple utility to set environment variables for local development
export KAFKA_BOOTSTRAP_SERVERS=192.168.1.53:9092,192.168.1.53:9093,192.168.1.53:9094
export TOPIC_IN=swisscollections-filtered
export TOPIC_OUT=swisscollections-solrdoc
export APPLICATION_ID=content2searchdocs.app555
export LEECH_XSL_TEMPLATE=leech.xslt
export TRANSFORMER_IMPL="net.sf.saxon.TransformerFactoryImpl"
export ICU_NORMALIZER="ch.swisscollections.alphabeticbrowse.normalizer.ICUCollatorNormalizer"
export MONGO_URI="mongodb://192.168.1.53:29017/?authSource=admin"
#export MONGO_URI=mongodb://xxx:yyy@localhost:29017/?authSource=admin
export MONGO_COLLECTION="sourceDNBGND1"
export MONGO_DB="nativeSources"
export TOC_DB_USER="remote"
export TOC_DB_PASSWD="skipnebis"





