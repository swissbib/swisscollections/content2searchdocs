import ch.swisscollections.searchdocs.Main.XMLTransformation.{specialGreenTransformer, startTransformer, step1Transformer, step2Transformer, weedTransformer}
import ch.swisscollections.searchdocs.helper.{TemplateCreator, TemplateTransformer, XSLTDataObject}
import java.util.{HashMap => JHashMap}

import ch.swisscollections.searchdocs.extensions.SolrStringTypePreprocessor
import ch.swisscollections.searchdocs.utilities.XmlHelpers.usePostTransformations

import scala.io.Source
import scala.xml.{Elem, Node}
import scala.xml.parsing.ConstructingParser


val TRANSFORMERIMPL = "net.sf.saxon.TransformerFactoryImpl"
val holdingsStructure = "collect.holdings.xsl"
val weedholdings = "weedholdings.xsl"
val solrstep1 = "swissbib.solr.step1.xsl"
val specialgreen = "vufind.special.green.xsl"
val solrstep2 = "swissbib.solr.step2.xsl"

val singleRecord =
  """<record><leader>     cam a22     4  4500</leader><controlfield tag="001">005784956</controlfield><controlfield tag="003">CHVBK</controlfield><controlfield tag="005">20191119115632.0</controlfield><controlfield tag="008">110121s2010    gw            00    ger  </controlfield><datafield tag="020" ind1=" " ind2=" "><subfield code="a">978-3-89794-168-7</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(OCoLC)887465597</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(ABN)000557080</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">CH-AaAKB</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Plešnik</subfield><subfield code="D">Marko</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Bosnien und Herzegowina</subfield><subfield code="b">unterwegs zwischen Adria und Save</subfield><subfield code="c">Marko Plešnik</subfield></datafield><datafield tag="250" ind1=" " ind2=" "><subfield code="a">3., aktual. und erw. Aufl.</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">Berlin</subfield><subfield code="b">Trescher</subfield><subfield code="c">2010</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">1 Band</subfield><subfield code="b">Ill.</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="a">Text</subfield><subfield code="b">txt</subfield><subfield code="2">rdacontent/ger</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="a">ohne Hilfsmittel zu benutzen</subfield><subfield code="b">n</subfield><subfield code="2">rdamedia/ger</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="a">Band</subfield><subfield code="b">nc</subfield><subfield code="2">rdacarrier/ger</subfield></datafield><datafield tag="651" ind1=" " ind2="7"><subfield code="a">Bosnien</subfield><subfield code="0">(DE-588)4007826-7</subfield><subfield code="2">gnd</subfield></datafield><datafield tag="651" ind1=" " ind2="7"><subfield code="a">Herzegowina</subfield><subfield code="0">(DE-588)4024643-7</subfield><subfield code="2">gnd</subfield></datafield><datafield tag="655" ind1=" " ind2="7"><subfield code="a">Führer</subfield><subfield code="2">gnd</subfield></datafield><datafield tag="691" ind1=" " ind2="7"><subfield code="B">u</subfield><subfield code="u">914.971</subfield><subfield code="2">abn YN</subfield></datafield><datafield tag="912" ind1=" " ind2="7"><subfield code="a">900</subfield><subfield code="2">CH-AaABN</subfield></datafield><datafield tag="830" ind1=" " ind2=" "><subfield code="a">Nachlass Oscar Cullmann (1902-1999). C, Werke Oscar Cullmanns. III, Undatierte Manuskripte</subfield><subfield code="v">2</subfield><subfield code="w">(HAN)000182106DSV05</subfield></datafield><datafield tag="950" ind1=" " ind2=" "><subfield code="B">ABN</subfield><subfield code="P">100</subfield><subfield code="E">1-</subfield><subfield code="a">Plešnik</subfield><subfield code="D">Marko</subfield></datafield><datafield tag="898" ind1=" " ind2=" "><subfield code="a">BK020000</subfield><subfield code="b">XK020000</subfield><subfield code="c">XK020000</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="B">ABN</subfield><subfield code="C">ABN51</subfield><subfield code="D">ABN01</subfield><subfield code="E">000557080</subfield><subfield code="F">KSZ</subfield><subfield code="b">KSZ</subfield><subfield code="c">KSZFH</subfield><subfield code="j">KSZ 914.971</subfield><subfield code="p">BZZ5509631</subfield><subfield code="q">000557080</subfield><subfield code="r">000010</subfield><subfield code="0">Kantonsschule Zofingen</subfield><subfield code="1">Freihand</subfield><subfield code="3">BOOK</subfield><subfield code="4">11</subfield><subfield code="5">1 Monat</subfield></datafield></record>""".stripMargin

val config = new JHashMap[String,String]()
//config.put("PLUGINS.IN.PRODUCTIONMODE","extensions.SolrStringTypePreprocessor")
config.put("PLUGINS.IN.PRODUCTIONMODE","ch.swisscollections.searchdocs.extensions.SolrStringTypePreprocessor")
config.put("NAV_FIELD_FORM_SYNONYMS_DIR","/swissbib_index/solrDocumentProcessing/MarcToSolr/dist/predoc")
new SolrStringTypePreprocessor().initPlugin(config)



lazy val startTransformer = {
  new TemplateCreator(TRANSFORMERIMPL, holdingsStructure).createTransformerFromResource
}

lazy val weedTransformer = {
  new TemplateCreator(TRANSFORMERIMPL, weedholdings).createTransformerFromResource
}

lazy val step1Transformer = {
  new TemplateCreator(TRANSFORMERIMPL, solrstep1).createTransformerFromResource
}

lazy val specialGreenTransformer = {
  new TemplateCreator(TRANSFORMERIMPL, specialgreen).createTransformerFromResource
}

lazy val step2Transformer = {
  new TemplateCreator(TRANSFORMERIMPL, solrstep2).createTransformerFromResource
}

def parseRecord(line: String): Elem = {
  val cp = ConstructingParser.fromSource(Source.fromString(line),preserveWS=false)
  cp.document().docElem.asInstanceOf[Elem]
}

val holdings = new TemplateTransformer(singleRecord).transform(startTransformer)



val dataObject = new XSLTDataObject
dataObject.additions.put("holdingsStructure", holdings)
dataObject.record = singleRecord

val weedHoldingsTransformation = new TemplateTransformer(dataObject.record).transform(weedTransformer)
dataObject.record = weedHoldingsTransformation

val step1Transformation = new TemplateTransformer(dataObject.record).transform(step1Transformer)
dataObject.record = step1Transformation
val specialGreenTransformation = new TemplateTransformer(dataObject.record).transform(specialGreenTransformer)

dataObject.record = specialGreenTransformation

step2Transformer.setParameter("holdingsStructure", dataObject.additions.get("holdingsStructure"))
val step2Transformation = new TemplateTransformer(dataObject.record).transform(step2Transformer)

println(step2Transformation)

val parsedRecord = parseRecord(step2Transformation)
val processWrappers = usePostTransformations(parsedRecord).asInstanceOf[Node]

println(processWrappers)


