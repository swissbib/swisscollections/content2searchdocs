<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
                <xsl:output indent="no"
                omit-xml-declaration="yes"

    />

    <!--<xsl:output indent="no" cdata-section-elements="field[@name='fullrecord']"/>-->
    <xsl:strip-space elements="*"/>


<!--https://stackoverflow.com/questions/15525629/xslt-match-all-nodes-except-a-specific-one-->

    <xsl:template match="@*|node()[not(self::field[@name='fullrecord']) and not(self::field[@name='holdings']) ]" >

        <xsl:copy>

            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>

    </xsl:template>



    <xsl:template match="field[@name='fullrecord']">
        <field name="fullrecord">
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
            <xsl:value-of disable-output-escaping="yes"  select="."/>
            <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
        </field>
    </xsl:template>

    <xsl:template match="field[@name='holdings']">
        <field name="holdings">
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
            <xsl:value-of disable-output-escaping="yes"  select="."/>
            <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
        </field>
    </xsl:template>

</xsl:stylesheet>
