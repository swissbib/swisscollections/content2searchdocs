<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:swissbib="www.swissbib.org/solr/documentprocessing.plugins"
    exclude-result-prefixes="fn swissbib"
>
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->
    
    <xsl:output method="xml"
        encoding="UTF-8"
        indent="no"
        omit-xml-declaration="yes"
    />

    <xsl:template match="record">
        <xsl:choose>
            <xsl:when test="exists(datafield[@tag='351'][matches(descendant::subfield[@code='c'],'^Bestand', 'i')])">
                <xsl:copy>
                    <xsl:call-template name="hierarchytype">
                        <xsl:with-param name="record" select="." />
                    </xsl:call-template>
                </xsl:copy>
                <xsl:copy-of select="record/node()" />
            </xsl:when>
            <xsl:when test="exists(datafield[@tag='019'][matches(descendant::subfield[@code='a'],'^Übergeordnete Aufnahme = Niveau supérieur = Livello superiore', 'i')])">
                <xsl:copy>
                    <xsl:call-template name="hierarchytype">
                        <xsl:with-param name="record" select="." />
                    </xsl:call-template>
                </xsl:copy>
                <xsl:copy-of select="record/node()" />
            </xsl:when>

            <xsl:when test="exists(datafield[matches(@tag, '773|800|810|830')]/subfield[@code='w'])
                or exists(datafield[@tag='986'][matches(subfield[@code='a']/text(), '^SWISSBIB')])">
                <xsl:copy>
                    <xsl:call-template name="green_special">
                        <xsl:with-param name="record" select="." />
                    </xsl:call-template>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="." />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- fields for use in vufind hierarchy driver (standard linking fields 490) -->
    <xsl:template name="green_special">
        <xsl:param name="record" />
        <xsl:if test="exists(datafield[matches(@tag, '773|800|810|830')]/subfield[@code='w'])">
            <is_hierarchy_id>
                <xsl:value-of select="$record/myDocID" />
            </is_hierarchy_id>
            <is_hierarchy_title>
                <xsl:value-of select="$record/datafield[@tag='245']/subfield[@code='a'][1]" />
            </is_hierarchy_title>
        </xsl:if>

        <xsl:for-each select="datafield[matches(@tag, '800|810|830')]/subfield[@code='w']">
            <hierarchy_parent_id>
                <xsl:value-of select="." />
            </hierarchy_parent_id>
            <hierarchy_parent_title>
                <xsl:value-of select="preceding-sibling::subfield[@code='a'][1]" />
            </hierarchy_parent_title>
            <xsl:choose>
                <xsl:when test="exists(preceding-sibling::subfield[@code='v'])">
                    <title_in_hierarchy>
                        <xsl:choose>
                            <xsl:when test="count($record/datafield[@tag='490'])=1 and exists($record/datafield[@tag='490']/subfield[@code='v'])">
                                    <xsl:value-of select="$record/datafield[@tag='490']/subfield[@code='v'][1]"></xsl:value-of>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="preceding-sibling::subfield[@code='v'][1]"/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:text> : </xsl:text>
                        <xsl:call-template name="hierarchytitle">
                            <xsl:with-param name="record" select="$record" />
                        </xsl:call-template>
                    </title_in_hierarchy>
                </xsl:when>
                <xsl:otherwise>
                    <title_in_hierarchy>
                        <xsl:call-template name="hierarchytitle">
                            <xsl:with-param name="record" select="$record" />
                        </xsl:call-template>
                    </title_in_hierarchy>
                </xsl:otherwise>
            </xsl:choose>
            <hierarchy_sequence>
                <xsl:value-of select="replace(preceding-sibling::subfield[@code='v'][1], '[/]', '.')" />
            </hierarchy_sequence>
        </xsl:for-each>

        <xsl:for-each select="datafield[@tag='773']/subfield[@code='w']">
            <xsl:variable name="pages">
                <xsl:for-each select="preceding-sibling::subfield[@code='g']">
                    <xsl:choose>
                        <xsl:when test="matches(., '^yr:')"/>
                        <xsl:when test="matches(., '^no:')"/>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:variable>
            <hierarchy_parent_id>
                <xsl:value-of select="." />
            </hierarchy_parent_id>
            <hierarchy_parent_title>
                <xsl:value-of select="preceding-sibling::subfield[@code='t'][1]" />
            </hierarchy_parent_title>
            <xsl:choose>
                <xsl:when test="exists($pages)">
                    <title_in_hierarchy>
                        <xsl:value-of select="$pages"/>
                        <xsl:text> : </xsl:text>
                        <xsl:call-template name="hierarchytitle">
                            <xsl:with-param name="record" select="$record" />
                        </xsl:call-template>
                    </title_in_hierarchy>
                </xsl:when>
                <xsl:otherwise>
                    <title_in_hierarchy>
                        <xsl:call-template name="hierarchytitle">
                            <xsl:with-param name="record" select="$record" />
                        </xsl:call-template>
                    </title_in_hierarchy>
                </xsl:otherwise>
            </xsl:choose>
            <hierarchy_sequence>
                <xsl:value-of select="replace(preceding-sibling::subfield[@code='q'][1], '[/]', '.')" />
            </hierarchy_sequence>
        </xsl:for-each>


        <xsl:for-each select="datafield[@tag='986'][matches(subfield[@code='a']/text(), '^SWISSBIB')]/subfield[@code='b']">
            <groupid_isn_mv>
                <xsl:value-of select="." />
            </groupid_isn_mv>
        </xsl:for-each>
        <xsl:copy-of select="$record/node()" />
    </xsl:template>

    <xsl:template name="hierarchytype">
        <xsl:param name="record" />
        <hierarchytype>archival</hierarchytype>
        <xsl:copy-of select="$record/node()" />
    </xsl:template>


    <xsl:template name="hierarchytitle">
        <xsl:param name="record" />
        <xsl:for-each select="$record/datafield[@tag='245']">
            <!-- use 245$a without angle brackets <<The>> -->
            <xsl:value-of select="replace(./subfield[@code='a'], '&lt;&lt;(.*)&gt;&gt;','$1')"></xsl:value-of>
            <xsl:if test="./subfield[@code='c']">
                <xsl:text> / </xsl:text>
                <xsl:value-of select="$record/datafield[@tag='245']/subfield[@code='c']"></xsl:value-of>
            </xsl:if>

        </xsl:for-each>

        <xsl:if test="number($record/year1)">
            <xsl:text> (</xsl:text>
            <xsl:value-of select="$record/year1"></xsl:value-of>
            <xsl:if test="$record/year2 &gt; $record/year1 and $record/year2 != 'uuuu'">
                <xsl:text>-</xsl:text>
                <xsl:value-of select="$record/year2"></xsl:value-of>
            </xsl:if>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>


    
</xsl:stylesheet>