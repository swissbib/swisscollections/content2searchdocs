<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xpath-default-namespace="http://www.loc.gov/MARC21/slim"
                exclude-result-prefixes="xs"
                version="2.0">

    <xsl:output
            method="xml"
            encoding="UTF-8"
            indent="yes"
            omit-xml-declaration="yes"

    />

    <xsl:template match="/">

        <xsl:call-template name="variant_person_title"/>
        <xsl:call-template name="variant_corporate_title"/>
        <xsl:call-template name="variant_title"/>

    </xsl:template>


    <xsl:variable name="newline">
        <xsl:text>&#10;</xsl:text>
    </xsl:variable>

    <xsl:template name="variant_person_title">
        <xsl:for-each select="/record/datafield[@tag='400']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='t'])">
                    <xsl:value-of select="child::subfield[@code='t']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_corporate_title">
        <xsl:for-each select="/record/datafield[@tag='410']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='t'])">
                    <xsl:value-of select="child::subfield[@code='t']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_title">
        <xsl:for-each select="/record/datafield[@tag='430']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='a'])">
                    <xsl:value-of select="child::subfield[@code='a']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>