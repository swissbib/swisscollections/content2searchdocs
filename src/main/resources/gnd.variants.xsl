<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xpath-default-namespace="http://www.loc.gov/MARC21/slim"
                exclude-result-prefixes="xs"
                version="2.0">

    <xsl:output
            method="xml"
            encoding="UTF-8"
            indent="yes"
            omit-xml-declaration="yes"

    />

    <xsl:template match="/">

        <xsl:call-template name="variant_person"/>
        <xsl:call-template name="variant_corporate"/>
        <xsl:call-template name="variant_conference"/>
        <xsl:call-template name="variant_subject"/>
        <xsl:call-template name="variant_place"/>
        <xsl:call-template name="macs"/>

    </xsl:template>


    <xsl:variable name="newline">
        <xsl:text>&#10;</xsl:text>
    </xsl:variable>

    <xsl:template name="variant_person">
        <xsl:for-each select="/record/datafield[@tag='400']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='a'])">
                    <xsl:value-of select="child::subfield[@code='a']/text()"/>
                </xsl:if>
                <xsl:if test="exists(child::subfield[@code='b'])">
                    <xsl:text>##xx##</xsl:text>
                    <xsl:value-of select="child::subfield[@code='b']/text()"/>
                </xsl:if>
                <xsl:if test="exists(child::subfield[@code='c'])">
                    <xsl:text>##xx##</xsl:text>
                    <xsl:value-of select="child::subfield[@code='c']/text()"/>
                </xsl:if>
                <xsl:if test="exists(child::subfield[@code='d'])">
                    <xsl:text>##xx##</xsl:text>
                    <xsl:value-of select="child::subfield[@code='d']/text()"/>
                </xsl:if>
                <xsl:if test="exists(child::subfield[@code='x'])">
                    <xsl:text>##xx##</xsl:text>
                    <xsl:value-of select="child::subfield[@code='x']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_corporate">
        <xsl:for-each select="/record/datafield[@tag='410']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='a'])">
                    <xsl:value-of select="child::subfield[@code='a']/text()"/>
                </xsl:if>
                <xsl:if test="exists(child::subfield[@code='b'])">
                    <xsl:text>##xx##</xsl:text>
                    <xsl:value-of select="child::subfield[@code='b']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_conference">
        <xsl:for-each select="/record/datafield[@tag='411']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='a'])">
                    <xsl:value-of select="child::subfield[@code='a']/text()"/>
                </xsl:if>
                <xsl:if test="exists(child::subfield[@code='b'])">
                    <xsl:text>##xx##</xsl:text>
                    <xsl:value-of select="child::subfield[@code='b']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_subject">
        <xsl:for-each select="/record/datafield[@tag='450']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='a'])">
                    <xsl:value-of select="child::subfield[@code='a']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_place">
        <xsl:for-each select="/record/datafield[@tag='451']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='a'])">
                    <xsl:value-of select="child::subfield[@code='a']/text()"/>
                </xsl:if>
                <xsl:if test="exists(child::subfield[@code='x'])">
                    <xsl:text>##xx##</xsl:text>
                    <xsl:value-of select="child::subfield[@code='x']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="macs">
        <xsl:for-each select="/record/datafield[@tag='750']">
            <xsl:variable name="values">
                <xsl:if test="exists(child::subfield[@code='a'])">
                    <xsl:value-of select="child::subfield[@code='a']/text()"/>
                </xsl:if>
                <xsl:value-of select="$newline"/>
            </xsl:variable>
            <xsl:value-of select="$values"/>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>