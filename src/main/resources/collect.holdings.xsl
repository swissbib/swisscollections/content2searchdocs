<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
>
    
    <xsl:output
        omit-xml-declaration="yes"
        />


    <xsl:template match="@*|node()">
            <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
   
   <xsl:template match="record">
       <record>
           <xsl:apply-templates/>
       </record>
   </xsl:template>

    <xsl:template match="datafield[@tag='852']">
        <xsl:if test="matches(subfield[@code='b'],
        '^AKB$|^A382$|^SGARK$|^A100$|^A115$|^A116$|^A117$|^A118$|^A125$|^A380$|^SGKBV$|^SGSTI$|^A381$|^Z01$|^Z02$|^Z03$|^Z04$|^Z05$|^Z06$|^Z07$|^Z08$|^UMWI$|^E30$|^B400$|^B404$|^B410$|^B412$|^B415$|^B452$|^B464$|^B465$|^B466$|^B467$|^B500$|^B521$|^B542$|^B552$|^B554$|^B555$|^B589$|^LUZHB$|^LUSBI$|^A150$|^ZBcollections$')">
            <xsl:element name="datafield" >
                <xsl:attribute name="tag">852</xsl:attribute>
                <xsl:attribute name="ind1">
                    <xsl:value-of select="@ind1"/>
                </xsl:attribute>
                <xsl:attribute name="ind2">
                    <xsl:value-of select="@ind2"/>
                </xsl:attribute>
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:if>
        <!-- special case B583 : split in two libraries based on $c -->
        <xsl:if test="matches(subfield[@code='b'],'^B583$')">
            <xsl:choose>
                <xsl:when test="matches(subfield[@code='c'],'583RO')">
                    <!-- for Rorschach archive we deliver library code B583RO -->
                    <xsl:element name="datafield" >
                        <xsl:attribute name="ind2">
                            <xsl:value-of select="@ind2" />
                        </xsl:attribute>
                        <xsl:attribute name="ind1">
                            <xsl:value-of select="@ind1" />
                        </xsl:attribute>
                        <xsl:attribute name="tag">852</xsl:attribute>
                        <xsl:element name="subfield">
                            <xsl:attribute name="code">b</xsl:attribute>
                            <xsl:text>B583RO</xsl:text>
                        </xsl:element>
                        <xsl:copy-of select="subfield[@code='c']" />
                        <xsl:copy-of select="subfield[@code='j']" />
                        <xsl:copy-of select="subfield[@code='z']" />
                        <xsl:copy-of select="subfield[@code='9']" />
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <!-- when not 583RO, then it is the standard Bern, UB Bibliothek Medizingeschichte -->
                    <xsl:element name="datafield" >
                        <xsl:attribute name="tag">852</xsl:attribute>
                        <xsl:attribute name="ind1">
                            <xsl:value-of select="@ind1"/>
                        </xsl:attribute>
                        <xsl:attribute name="ind2">
                            <xsl:value-of select="@ind2"/>
                        </xsl:attribute>
                        <xsl:apply-templates/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>

    </xsl:template>

    <xsl:template match="datafield[@tag='947']">
        <xsl:if test="matches(subfield[@code='b'],
        '^AKB$|^A382$|^SGARK$|^A100$|^A115$|^A116$|^A117$|^A118$|^A125$|^A380$|^SGKBV$|^SGSTI$|^A381$|^Z01$|^Z02$|^Z03$|^Z04$|^Z05$|^Z06$|^Z07$|^Z08$|^UMWI$|^E30$|^B400$|^B404$|^B410$|^B412$|^B415$|^B452$|^B464$|^B465$|^B466$|^B467$|^B500$|^B521$|^B542$|^B552$|^B554$|^B555$|^B583$|^B589$|^LUZHB$|^LUSBI$|^A150$|^ZBcollections$|^AFREE$')">
            <xsl:copy-of select="."/>
            <xsl:apply-templates/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="datafield[@tag='949']">
        <xsl:if test="matches(subfield[@code='b'],
        '^AKB$|^A382$|^SGARK$|^A100$|^A115$|^A116$|^A117$|^A118$|^A125$|^A380$|^SGKBV$|^SGSTI$|^A381$|^Z01$|^Z02$|^Z03$|^Z04$|^Z05$|^Z06$|^Z07$|^Z08$|^UMWI$|^E30$|^B400$|^B404$|^B410$|^B412$|^B415$|^B452$|^B464$|^B465$|^B466$|^B467$|^B500$|^B521$|^B542$|^B552$|^B554$|^B555$|^B589$|^LUZHB$|^LUSBI$|^A150$|^ZBcollections$')">
            <xsl:element name="datafield" >
                <xsl:attribute name="tag">949</xsl:attribute>
                <xsl:attribute name="ind1">
                    <xsl:value-of select="@ind1" />
                </xsl:attribute>
                <xsl:attribute name="ind2">
                    <xsl:value-of select="@ind2" />
                </xsl:attribute>
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="matches(subfield[@code='b'],'^B583$')">
            <xsl:choose>
                <xsl:when test="matches(subfield[@code='c'],'583RO')">
                    <!-- for Rorschach archive we deliver library code B583RO -->
                    <xsl:element name="datafield" >
                        <xsl:attribute name="ind2">
                            <xsl:value-of select="@ind2" />
                        </xsl:attribute>
                        <xsl:attribute name="ind1">
                            <xsl:value-of select="@ind1" />
                        </xsl:attribute>
                        <xsl:attribute name="tag">949</xsl:attribute>
                        <xsl:element name="subfield">
                            <xsl:attribute name="code">b</xsl:attribute>
                            <xsl:text>B583RO</xsl:text>
                        </xsl:element>
                        <xsl:copy-of select="subfield[@code='c']" />
                        <xsl:copy-of select="subfield[@code='h']" />
                        <xsl:copy-of select="subfield[@code='i']" />
                        <xsl:copy-of select="subfield[@code='j']" />
                        <xsl:copy-of select="subfield[@code='p']" />
                        <xsl:copy-of select="subfield[@code='q']" />
                        <xsl:copy-of select="subfield[@code='y']" />
                        <xsl:copy-of select="subfield[@code='z']" />
                        <xsl:copy-of select="subfield[@code='9']" />
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <!-- when not 583RO, then it is the standard Bern, UB Bibliothek Medizingeschichte -->
                    <xsl:element name="datafield" >
                        <xsl:attribute name="tag">949</xsl:attribute>
                        <xsl:attribute name="ind1">
                            <xsl:value-of select="@ind1" />
                        </xsl:attribute>
                        <xsl:attribute name="ind2">
                            <xsl:value-of select="@ind2" />
                        </xsl:attribute>
                        <xsl:apply-templates/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="datafield[matches(@tag, '852|949')]/subfield">
        <xsl:if test="not(@code = 'x')">
            <xsl:copy-of select="." />
        </xsl:if>
    </xsl:template>
    
    
</xsl:stylesheet>