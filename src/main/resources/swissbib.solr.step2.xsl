<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                exclude-result-prefixes="fn">
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->

    <xsl:output method="xml"
                encoding="UTF-8"
                indent="no"
                omit-xml-declaration="yes"
    />

    <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
        <desc>
            Dieses Skript ist die zweite Stufe (step2) der Verarbeitung im Document-Processing zur Aufbereitung
            der Daten vor der eigentlich Indexierung.
            Siehe Kurzbeschreibung im Step1, Dokumentation im wiki, Issues, etc.

            ************************
            2020 UB Basel
            ************************

        </desc>
    </doc>

    <xsl:param name="holdingsStructure" select="''"/>

    <!--=================
        CALLING TEMPLATES
        =================-->
    <xsl:template match="/">
        <doc>

            <xsl:call-template name="id_type">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="lang_country">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="classifications">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="format">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="bibid">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="series_hierarchy">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="callnumber">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="institution">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="itemnote">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="itemnoteTxt">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="location">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="filter">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="ctrlnum">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="coordinates">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="publishDate">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="freshness">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="publplace">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="authors">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="author_first">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="titles">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="title_browse">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="title_old">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="title_new">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="series">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="publisher">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="pubplace">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="descriptionlevel">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="add_fields">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="localcode">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subpers_lcsh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtitle_lcsh">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subtop_lcsh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_lcsh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_lcsh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_mesh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtime_mesh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_mesh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_mesh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_mesh">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_stw">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subtitle_stw">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subtime_stw">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_stw">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_stw">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_stw">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_bgs">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_gnd">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subtitle_gnd">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subtime_gnd">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_gnd">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_gnd">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_gnd">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subenrichment_gnd">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subenrichment_gnd_related">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subpers_rero">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtime_rero">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_rero">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_rero">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_rero">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_idref">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtime_idref">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_idref">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_idref">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_idref">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_idsbb">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtitle_idsbb">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subtime_idsbb">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_idsbb">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_idsbb">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_idsbb">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_idszbz">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtitle_idszbz">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="subtime_idszbz">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_idszbz">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_idszbz">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_idszbz">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_sbt">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtime_sbt">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_sbt">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_sbt">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_sbt">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subpers_jurivoc">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtime_jurivoc">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_jurivoc">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subgeo_jurivoc">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_jurivoc">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subtop_ethudk">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="medium_ids">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="medium_rero">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subform_swisscollections">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="auth_controlnumber">
                <xsl:with-param name="fragment" select="record"/>
            </xsl:call-template>

            <xsl:call-template name="subundef">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="sublocal">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="fulltext">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="proctime_fullrecord">
                <xsl:with-param name="nativeXML" select="record" />
            </xsl:call-template>

            <xsl:call-template name="createHoldings" />

            <xsl:call-template name="tectonics">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="businessDocumentation">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="topicTectonic">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="bibliographies">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="bibliographies_special">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="facet_bgs_media">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="facet_bgs_publplace">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="facet_bgs_text_type">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="facet_bgs_collection">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="filter_bgs">
                <xsl:with-param name="fragment" select="record" />
             </xsl:call-template>

            <xsl:call-template name="callno_bgs">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>

            <xsl:call-template name="format_additional_bgs">
                <xsl:with-param name="fragment" select="record" />
            </xsl:call-template>
        </doc>
    </xsl:template>

    <!--======================
        CALLED NAMED TEMPLATES
        ======================-->

    <xsl:template name="id_type">
        <xsl:param name="fragment" />
        <field name="id">
            <xsl:value-of select="$fragment/myDocID" />
        </field>
        <field name="recordtype">marc</field>
        <field name="view_str_mv">
            <xsl:choose>
                <xsl:when test="matches($fragment/myDocID, '^99.*5501$')">sc</xsl:when>
                <xsl:when test="matches($fragment/myDocID, '^ZBC')">sc</xsl:when>
                <xsl:otherwise>bgs</xsl:otherwise>
            </xsl:choose>
        </field>
    </xsl:template>

    <!-- codes: language / country of origin of publication -->
    <xsl:template name="lang_country">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <!-- remove undefined values (|||, und) from index -->
            <xsl:for-each select="$fragment/datafield[@tag='041']/subfield[@code='a']/text()">
                <xsl:choose>
                    <xsl:when test="matches(., '\|\|\||und')" />
                    <xsl:when test="string-length(.) &gt; 3" />
                    <xsl:otherwise>
                        <xsl:value-of select="concat(., '##xx##')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$fragment/controlfield[@tag='008']">
                <xsl:variable name="lang" select="substring(text()[1],36,3)"/>
                <xsl:choose>
                    <xsl:when test="matches($lang, '\|\|\||und')" />
                    <xsl:otherwise>
                        <xsl:value-of select="concat($fragment/substring(controlfield[@tag='008'][1],36,3), '##xx##')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>


        </xsl:variable>

        <xsl:choose>
            <xsl:when test="matches($fragment/myDocID, '^99.*5501$')">
                <xsl:call-template name="prepareDedup">
                    <xsl:with-param name="fieldname" select="'language'" />
                    <xsl:with-param name="fieldValues" select ="$forDeduplication" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="matches($fragment/myDocID, '^ZBC')">
                <xsl:call-template name="prepareDedup">
                    <xsl:with-param name="fieldname" select="'language'" />
                    <xsl:with-param name="fieldValues" select ="$forDeduplication" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="prepareDedup">
                    <xsl:with-param name="fieldname" select="'language_bgs_strl_mv'" />
                    <xsl:with-param name="fieldValues" select ="$forDeduplication" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>


        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/controlfield[@tag='008']">
                <xsl:value-of select="concat(substring(text(),16,3), '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='044']/subfield[@code='a']/text()">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <!--<xsl:variable name="uniqueSeqValues" select="swissbib:startDeduplication($forDeduplication)"/>-->
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'origcountry_isn_mv'"/>
            <xsl:with-param name="fieldValues" select="$forDeduplication"/>
        </xsl:call-template>
    </xsl:template>


    <!-- classifications -->
    <xsl:template name="classifications">
        <xsl:param name="fragment" />
        <!-- subject category codes (MARC field 072) -->
        <xsl:for-each select="$fragment/datafield[@tag='072']/subfield[@code='a']">
            <xsl:variable name="source" select="following-sibling::subfield[@code='2']/text()" />
            <field name="{concat('classif_', $source)}">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <!-- UDC fields, standard and non-standard (11.10.2012 / osc) -->
        <xsl:for-each select="$fragment/datafield[@tag='080']/subfield[@code='a']">
            <field name="classif_udc">
                <xsl:value-of select="concat(., following-sibling::subfield[@code='x'][1], following-sibling::subfield[@code='x'][2], following-sibling::subfield[@code='x'][3])" />
            </field>
        </xsl:for-each>
        <!-- DDC fields, standard and non-standard (11.10.2012 / osc) -->
        <xsl:for-each select="$fragment/datafield[@tag='082']/subfield[@code='a']">
            <field name="classif_ddc">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='082'][matches(descendant::subfield[@code='2'][1], '^15$', 'i')]/subfield[@code='a']">
            <field name="classif_ddc_15">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <!-- RVK / ZDBS classifications  -->
        <xsl:for-each select="$fragment/datafield[@tag='084']/subfield[@code='a']">
            <xsl:if test="matches(following-sibling::subfield[@code='2'][1], 'rvk', 'i')">
                <field name="classif_rvk">
                    <xsl:value-of select="." />
                </field>
            </xsl:if>
            <xsl:if test="matches(following-sibling::subfield[@code='2'][1], 'zdbs', 'i')">
                <field name="classif_zdbs">
                    <xsl:value-of select="." />
                </field>
            </xsl:if>
            <xsl:if test="matches(following-sibling::subfield[@code='2'][1], 'sdnb', 'i')">
                <field name="sdnb_str_mv">
                    <xsl:value-of select="." />
                </field>
            </xsl:if>
        </xsl:for-each>
        <!-- selected local classifications with source code)-->
        <xsl:for-each select="$fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1], 'ubs-FA', 'i')]/subfield[@code='e'] |
                                  $fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1], 'ube-GA', 'i')]/subfield[@code='e']">
            <xsl:variable name="source" select="replace(following-sibling::subfield[@code='2']/text(),' ', '')"/>
            <field name="{concat('classif_', $source)}">
                <xsl:value-of select="."/>
            </field>
        </xsl:for-each>
        <!-- local classifications (without source code) -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1], 'ubs-FA', 'i')]/subfield[@code='e'] |
                                  $fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1], 'ube-GA', 'i')]/subfield[@code='e']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>


        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'classif_local'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


    </xsl:template>


    <!-- format for facet and display -->
    <xsl:template name="format">
        <xsl:param name="fragment" />

        <!-- format field for display -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/formatParent">
                <xsl:choose>
                    <xsl:when test="exists($fragment/formatSpecific) and exists($fragment/formatSpecific_archival)">
                        <xsl:for-each select="$fragment/formatSpecific">
                            <xsl:value-of select="concat(., '##xx##')" />
                        </xsl:for-each>
                        <xsl:for-each select="$fragment/formatSpecific_archival">
                            <xsl:value-of select="concat(., '##xx##')" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="exists($fragment/formatSpecific)">
                        <xsl:for-each select="$fragment/formatSpecific">
                            <xsl:value-of select="concat(., '##xx##')" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="exists($fragment/formatSpecific_archival)">
                        <xsl:if test=". != 'AR'">
                            <xsl:value-of select="concat($fragment/formatParent, '##xx##')" />
                        </xsl:if>
                        <xsl:for-each select="$fragment/formatSpecific_archival">
                            <xsl:value-of select="concat(., '##xx##')" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test=". != 'BE'">
                            <xsl:value-of select="concat(., '##xx##')" />
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$fragment/formatSpecific_bibliography">
                <xsl:value-of select="concat(., '##xx##')"/>
            </xsl:for-each>
            <xsl:for-each select="$fragment/formatAdditional">
                <xsl:value-of select="concat(., '##xx##')"/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'format_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <!-- format field parent for display -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/formatParent">
                <xsl:choose>
                    <xsl:when test="exists($fragment/formatParent_archival)">
                        <xsl:value-of select="concat($fragment/formatParent_archival, '##xx##')" />
                    </xsl:when>
                    <xsl:when test="$fragment/formatSpecific = 'letter'">
                        <xsl:value-of select="concat('AR', '##xx##')" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(., '##xx##')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'format_parent_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

            <xsl:choose>
                <xsl:when test="matches($fragment/myDocID, '^99.*5501$')">
                    <xsl:call-template name="prepareDedup">
                        <xsl:with-param name="fieldname" select="'icon_code_str_mv'" />
                        <xsl:with-param name="fieldValues" select ="$forDeduplication" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="matches($fragment/myDocID, '^ZBC')">
                    <xsl:call-template name="prepareDedup">
                        <xsl:with-param name="fieldname" select="'icon_code_str_mv'" />
                        <xsl:with-param name="fieldValues" select ="$forDeduplication" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>

        <!-- format field for hierarchical facet, based on combination of formatParent and formatSpecific -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/formatParent">
                <xsl:value-of select="concat('0/', ., '/', '##xx##')"/>
                <xsl:choose>
                    <xsl:when test="following-sibling::formatSpecific">
                        <xsl:for-each select="following-sibling::formatSpecific">
                            <xsl:value-of select="concat('1/', preceding-sibling::formatParent, '/', ., '/', '##xx##')"/>
                        </xsl:for-each>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$fragment/formatParent_archival">
                <xsl:value-of select="concat('0/', ., '/', '##xx##')"/>
                <xsl:choose>
                    <xsl:when test="following-sibling::formatSpecific_archival">
                        <xsl:for-each select="following-sibling::formatSpecific_archival">
                            <xsl:value-of select="concat('1/', preceding-sibling::formatParent_archival, '/', ., '/', '##xx##')"/>
                        </xsl:for-each>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$fragment/formatParent_bibliography">
                <xsl:value-of select="concat('0/', ., '/', '##xx##')"/>
                <xsl:choose>
                    <xsl:when test="following-sibling::formatSpecific_bibliography">
                        <xsl:for-each select="following-sibling::formatSpecific_bibliography">
                            <xsl:value-of select="concat('1/', preceding-sibling::formatParent_bibliography, '/', ., '/', '##xx##')"/>
                        </xsl:for-each>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
            <!-- formats which are placed in an additional place in the hierarchy -->
            <xsl:for-each select="$fragment/formatSpecific">
                <xsl:choose>
                    <xsl:when test="matches(.,'^letter')">
                        <xsl:value-of select="concat('0/AR/', '##xx##')"/>
                        <xsl:value-of select="concat('1/AR', '/', ., '/', '##xx##')"/>
                    </xsl:when>
                    <xsl:when test="matches(.,'^musicms')">
                        <xsl:value-of select="concat('0/MS/', '##xx##')"/>
                        <xsl:value-of select="concat('1/MS', '/', ., '/', '##xx##')"/>
                    </xsl:when>
                    <xsl:when test="matches(.,'^mapms')">
                        <xsl:value-of select="concat('0/MS/', '##xx##')"/>
                        <xsl:value-of select="concat('1/MS', '/', ., '/', '##xx##')"/>
                    </xsl:when>
                    <xsl:when test="matches(.,'^einblatt')">
                        <xsl:value-of select="concat('0/PR/', '##xx##')"/>
                        <xsl:value-of select="concat('1/PR', '/', ., '/', '##xx##')"/>
                        <xsl:value-of select="concat('0/IO/', '##xx##')"/>
                        <xsl:value-of select="concat('1/IO', '/', ., '/', '##xx##')"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
            <!-- formats which can't be placed in the hierarchy depending on the parent -->
            <xsl:for-each select="$fragment/formatAdditional">
                <xsl:choose>
                    <xsl:when test="matches(., 'typoskript')">
                        <xsl:value-of select="concat('0/AR/', '##xx##')"/>
                        <xsl:value-of select="concat('1/AR', '/', ., '/', '##xx##')"/>
                    </xsl:when>
                    <xsl:when test="matches(.,'autograph')">
                        <xsl:value-of select="concat('0/MS/', '##xx##')"/>
                        <xsl:value-of select="concat('1/MS', '/', ., '/', '##xx##')"/>
                    </xsl:when>
                    <xsl:when test="matches(.,'objekt')">
                        <xsl:value-of select="concat('0/IO/', '##xx##')"/>
                        <xsl:value-of select="concat('1/IO', '/', ., '/', '##xx##')"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>

        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'format_hierarchy_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


        <!-- rdacontent 336 -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='336'][matches(descendant::subfield[@code='2'][1],'^rdacontent','i')]/subfield[@code='b']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'rdacontent_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <!-- rdamedia 337 -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='337'][matches(descendant::subfield[@code='2'][1],'^rdamedia','i')]/subfield[@code='b']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'rdamedia_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


        <!-- rdacarrier 338 -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='338'][matches(descendant::subfield[@code='2'][1],'^rdacarrier','i')]/subfield[@code='b']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'rdacarrier_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- bibliographic identifiers -->
    <xsl:template name="bibid">
        <xsl:param name="fragment"/>
        <xsl:for-each select="$fragment/datafield[@tag='015']/subfield[@code='a']">
            <field name="nbn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='016']/subfield[@code='a']">
            <field name="nbacn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <!--<xsl:variable name="createISBNFacade" select="java-isbn-ext:new()" />-->
        <xsl:for-each select="$fragment/datafield[@tag='020']/subfield[@code='a']">
            <field name="isbn">
                <xsl:value-of select="." />
            </field>
            <xsl:variable name="currentISBN" select="." />
            <!--<xsl:variable name="vISBNVariation" select="java-isbn-ext:getAlternativeISBN($createISBNFacade, $currentISBN)"/>-->

            <preparedisbnalternative><xsl:value-of select="$currentISBN"/></preparedisbnalternative>
            <!--
            <xsl:call-template name="createUniqueFields">
                <xsl:with-param name="fieldname" select="'variant_isbn_isn_mv'" />
                <xsl:with-param name="fieldValues" select="$vISBNVariation"/>
            </xsl:call-template>
            -->
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='020']/subfield[@code='z']">
            <field name="cancisbn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='022']/subfield[@code='a']">
            <field name="issn">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='022']/subfield[@code='y']">
            <field name="incoissn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='022']/subfield[@code='z']">
            <field name="cancissn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='024'][@ind1='2']/subfield[@code='a']">
            <field name="ismn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='024'][@ind1='2']/subfield[@code='z']">
            <field name="cancismn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='024'][@ind1='7'][matches(descendant::subfield[@code='2'],'doi', 'i')]/subfield[@code='a']">
            <field name="doi_isn_mv">
                <xsl:value-of select="replace(., '\s', '')" />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='024'][@ind1='7'][matches(descendant::subfield[@code='2'],'URN', 'i')]/subfield[@code='a']">
            <field name="urn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='024'][matches(@ind1, '0|1|3|4|7|8')]/subfield[@code='a']">
            <field name="other_id_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='028']/subfield[matches(@code, 'a|b')]">
            <field name="publisher_id_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='773']/subfield[@code='o']">
            <field name="hostotherID_str_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='773']/subfield[@code='x']">
            <field name="hostissn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='773']/subfield[@code='y']">
            <field name="hostisbn_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>
    </xsl:template>


    <!-- for use in vufind hierarchy driver -->
    <xsl:template name="series_hierarchy">
        <xsl:param name="fragment" />
        <xsl:for-each select="$fragment/hierarchytype |
                              $fragment/is_hierarchy_id |
                              $fragment/is_hierarchy_title |
                              $fragment/hierarchy_top_id |
                              $fragment/hierarchy_top_title |
                              $fragment/hierarchy_parent_id |
                              $fragment/hierarchy_parent_title |
                              $fragment/title_in_hierarchy |
                              $fragment/hierarchy_sequence |
                              $fragment/groupid_isn_mv">
            <xsl:variable name="fieldname" select="name()" />
            <field name="{$fieldname}">
                <xsl:copy-of select="text()" />
            </field>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="callnumber">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='852']/subfield[@code='j']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='852']/subfield[@code='h']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='949']/subfield[@code='j']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='949']/subfield[@code='h']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='949']/subfield[@code='i']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'callnumber'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


        <xsl:for-each select="$fragment/datafield[matches(@tag, '852|949')]/subfield[@code='b']">
            <xsl:variable name="library" select="."/>
            <xsl:variable name="forDeduplication">
                <xsl:for-each select="following-sibling::subfield[matches(@code, 'h|i|j')]">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:for-each>
                <xsl:for-each select="preceding-sibling::subfield[matches(@code, 'h|i|j')]">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:for-each>
            </xsl:variable>

            <xsl:call-template name="prepareDedup">
                <xsl:with-param name="fieldname" select="concat('callnumber_', $library, '_str_mv')"/>
                <xsl:with-param name="fieldValues" select="$forDeduplication"/>
            </xsl:call-template>

            <xsl:call-template name="prepareDedup">
                <xsl:with-param name="fieldname" select="concat('callnumber_', $library, '_brw_mv')"/>
                <xsl:with-param name="fieldValues" select="$forDeduplication"/>
            </xsl:call-template>

        </xsl:for-each>

    </xsl:template>


    <xsl:template name="institution">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='852']/subfield[@code='b']">
                <xsl:if test=". != '' and . != 'A118'">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='947']/subfield[@code='b']">
                <xsl:if test=". != '' and . != 'AFREE'">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='AVD']/subfield[@code='g']">
                <xsl:if test="matches(., '^Z01$|Z02$|^Z03$|^Z04$|^Z05$|^Z06$|^Z07$|^Z08$|^UMWI$')">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'institution'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


    </xsl:template>

    <xsl:template name="itemnote">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='949']/subfield[@code='x']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'itemnote_isn_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="itemnoteTxt">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='949']/subfield[matches(@code, 'x|y')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='852']/subfield[matches(@code, 'x|y')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'itemnote_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="location">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='852']/subfield[@code='c']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='949']/subfield[@code='c']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'location_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="filter">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='900'][matches(descendant::subfield[@code='f'][1], 'Z01emanuscripta')]">
                <xsl:text>ONL##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='900'][matches(descendant::subfield[@code='a'][1], '^IDSZ1Z01erara|^ZBConline')]">
                <xsl:text>ONL##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='338'][matches(descendant::subfield[@code='b'], '^cr')]">
                <xsl:text>ONL##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='856'][matches(descendant::subfield[@code='u'], 'urn.ub.unibe.ch.*full:google')]">
                <xsl:text>ONL##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='024'][matches(descendant::subfield[@code='a'], '10.3931/e-rara')]">
                <xsl:text>ONL##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='024'][matches(descendant::subfield[@code='a'], '10.7891/e-manuscripta')]">
                <xsl:text>ONL##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='900']/subfield[@code='a']">
                <xsl:choose>
                    <xsl:when test="matches(., 'Metadata rights reserved')">
                        <xsl:text>MDRR##xx##</xsl:text>
                    </xsl:when>
                    <xsl:when test="matches(., 'HANunikat', 'i')">
                        <xsl:text>HAN##xx##</xsl:text>
                    </xsl:when>
                    <xsl:otherwise />
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'filter_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="coordinates">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='034']/subfield[matches(@code, 'b|d|e|f|g')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'coordinate_strl_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="ctrlnum">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='035']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='035'][matches(descendant::subfield[@code='a'][1], '^\(EXLNZ-41SLSP_NETWORK\)')]/subfield[@code='a']">
                <xsl:value-of select="concat(replace(., '\(EXLNZ-41SLSP_NETWORK\)', ''), '##xx##')" />
            </xsl:for-each>
            <!-- IZ ID full -->
            <xsl:for-each select="$fragment/datafield[@tag='986']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!-- IZ ID without prefix -->
            <xsl:for-each select="$fragment/datafield[@tag='986']/subfield[@code='a']">
                <xsl:value-of select="concat(substring-after(., ')'), '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'ctrlnum'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="publishDate">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/year">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'publishDate'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/sortyear">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'publishDateSort'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='046']/subfield[matches(@code, 'b|c|d|e')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'date_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <!-- to make it possible to search letter exact date in simple search -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='046']/subfield[matches(@code, 'b|c|d|e')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'date_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


    </xsl:template>

    <xsl:template name="freshness">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/freshness">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'freshness'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="publplace">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='751']">
                <xsl:for-each select="child::subfield[@code='a']">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:for-each>
                <xsl:for-each select="child::subfield[@code='g']">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'publplace_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'publplace_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'publplace_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


        <!-- added entries from GND -->
        <xsl:for-each select="$fragment/datafield[@tag='751'][matches(descendant::subfield[@code='0'][1], '^\(DE-588\)', 'i')]/subfield[@code='0']">
            <xsl:variable name="gndnumber" select="text()" />

            <xsl:element name="gndreferencesconcatenated">
                <xsl:attribute name="fieldName" select="'publplace_additional_gnd_txt_mv'"/>
                <xsl:attribute name="fieldValue" select="$gndnumber"/>
            </xsl:element>



        </xsl:for-each>
        <!-- relator specific search fields -->
        <xsl:for-each select="$fragment/datafield[@tag='751']/subfield[@code='4']">
            <xsl:variable name="relatorcode" select="." />
            <xsl:variable name="forDeduplication">
                <xsl:for-each select="preceding-sibling::subfield[@code='a']">
                    <xsl:value-of select="." />
                    <xsl:text>##xx##</xsl:text>
                </xsl:for-each>
            </xsl:variable>

            <xsl:call-template name="prepareDedup">
                <xsl:with-param name="fieldname" select="concat('publplace_', $relatorcode, '_txt_mv')" />
                <xsl:with-param name="fieldValues" select ="$forDeduplication" />
            </xsl:call-template>

        </xsl:for-each>

        <!-- facet for place of publication -->

        <xsl:for-each select="$fragment/datafield[@tag='264']/subfield[@code='a'] | $fragment/datafield[@tag='751']/subfield[@code='a']">

            <xsl:variable name="forDeduplication">
                <xsl:if test=". != '-' and
                              . !='[S.l.]' and
                              . !='[s.l.]' and
                              . !='[S. l.]' and
                              . !='[s. l.]' and
                              . !='s. l.' and
                              . !='[S.l]' and
                              . !='[...]' and
                              . !='...' and
                              . !='[Var.loc.]' and
                              . !='[var.loc.]' and
                              . !='[Var.loc]' and
                              . !='[var.loc]' and
                              . !='[Var.loc. ]' and
                              . !='[var.loc. ]' and
                              . !='[O.O.]' and
                              . !='[o.O.]' and
                              . !='O.O.' and
                              . !='o.O.' and
                              . !='Div.' and
                              . !='div.' and
                              . !='[Div.]' and
                              . !='[div.]' and
                              . !='[Div. Erscheinungsorte]' and
                              . !='[Ohne Ort]' and
                              . !='Versch. Orte' and
                              . !='[Versch. Orte]' and
                              . !='[Erscheinungsort nicht ermittelbar]' and
                              . !='[Absendeort nicht ermittelbar]' and
                              . !='[Entstehungsort nicht ermittelbar]' and
                              . !='[Herstellungsort nicht ermittelbar]' and
                              . !='[Lieu de publication non identifié]'">
                <xsl:value-of select="replace(replace(.,'ß', 'ss', 'i'), '\[|\]|\?| etc.| \[etc.\]', '', 'i')" />
                </xsl:if>
            </xsl:variable>


            <xsl:element name="createNavFieldCombined">
                <xsl:attribute name="fieldName" select="'navPublPlace_str_mv'"/>
                <xsl:attribute name="fieldValue" select="$forDeduplication"/>
            </xsl:element>

            <xsl:element name="createNavFieldCombined">
                <xsl:attribute name="fieldName" select="'navPublPlace_brw_mv'"/>
                <xsl:attribute name="fieldValue" select="$forDeduplication"/>
            </xsl:element>

            <xsl:call-template name="prepareDedup">
                <xsl:with-param name="fieldname" select="'publplace_txt_mv'" />
                <xsl:with-param name="fieldValues" select ="$forDeduplication" />
            </xsl:call-template>
        </xsl:for-each>


    </xsl:template>


    <!-- main and added entries -->
    <xsl:template name="authors">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <!-- personal entries -->
            <xsl:for-each select="$fragment/datafield[matches(@tag, '100|700|800')]/subfield[@code='a'] |
                                  $fragment/datafield[@tag='880'][matches(child::subfield[@code='6'],'100|700|800')]/subfield[@code='a'] ">
                <xsl:value-of select="." />
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <!-- corporate entries -->
            <xsl:for-each select="$fragment/datafield[matches(@tag, '110|710|810')]/subfield[@code='a'] |
                                  $fragment/datafield[@tag='880'][matches(child::subfield[@code='6'],'110|710|810')]/subfield[@code='a'] ">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <!-- meeting entries -->
            <xsl:for-each select="$fragment/datafield[matches(@tag, '111|711|811')]/subfield[@code='a'] |
                                  $fragment/datafield[@tag='880'][matches(child::subfield[@code='6'],'111|711|811')]/subfield[@code='a'] ">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='e']">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'author'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>



        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[matches(@tag, '100|700|800')]/subfield[matches(@code, '[b-df-su-z8]')] ">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '110|710|810')]/subfield[matches(@code, '[c-df-su-z]')] ">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '111|711|811')]/subfield[matches(@code, '[b-df-gk-su-z]')] ">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'author_additional'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[matches(@tag, '979')][matches(descendant::subfield[@code='P'], '100|110|111')]/subfield[@code='a'] ">
                <xsl:choose>
                    <xsl:when test="matches(following-sibling::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="following-sibling::subfield[@code='0']"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="." />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'author_main_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <!-- added entries for GND -->
        <xsl:for-each select="$fragment/datafield[matches(@tag, '[17][01][01]')][matches(descendant::subfield[@code='0'][1], '^\(DE-588\)', 'i')]/subfield[@code='0']">
            <xsl:variable name="gndnumber" select="text()" />

            <xsl:element name="gndreferencesconcatenated">
                <xsl:attribute name="fieldName" select="'author_additional_gnd_txt_mv'"/>
                <xsl:attribute name="fieldValue" select="$gndnumber"/>
            </xsl:element>


        </xsl:for-each>
        <!-- relator specific search fields -->
        <xsl:for-each select="$fragment/datafield[matches(@tag, '[17][01][01]')]/subfield[@code='4']">
            <xsl:variable name="relatorcode" select="." />
            <xsl:variable name="forDeduplication">
                <xsl:for-each select="preceding-sibling::subfield[@code='a']">
                    <xsl:value-of select="." />
                    <xsl:for-each select="following-sibling::subfield[matches(@code, '[b-df-su-z]')]">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                    <xsl:text>##xx##</xsl:text>
                </xsl:for-each>
            </xsl:variable>

            <xsl:call-template name="prepareDedup">
                <xsl:with-param name="fieldname" select="concat('author_', $relatorcode, '_txt_mv')" />
                <xsl:with-param name="fieldValues" select ="$forDeduplication" />
            </xsl:call-template>

        </xsl:for-each>

        <xsl:for-each select="$fragment/datafield[@tag='979']/subfield[@code='4']">
            <xsl:variable name="relatorcode" select="." />
            <xsl:variable name="forDeduplication">
                <xsl:for-each select="preceding-sibling::subfield[@code='a']">
                    <xsl:choose>
                        <xsl:when test="matches(following-sibling::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                            <xsl:value-of select="following-sibling::subfield[@code='0']"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="." />
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>##xx##</xsl:text>
                </xsl:for-each>
            </xsl:variable>

            <xsl:call-template name="prepareDedup">
                <xsl:with-param name="fieldname" select="concat('author_', $relatorcode, '_brw_mv')" />
                <xsl:with-param name="fieldValues" select ="$forDeduplication" />
            </xsl:call-template>

        </xsl:for-each>

        <xsl:for-each select="$fragment/datafield[matches(@tag, '979')][matches(descendant::subfield[@code='P'], '[17][01][01]')]/subfield[@code='a'] ">
            <xsl:variable name="forDeduplication">
                <xsl:choose>
                    <xsl:when test="matches(following-sibling::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="following-sibling::subfield[@code='0']"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="." />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="prepareDedup">
                <xsl:with-param name="fieldname" select="'author_brw_mv'" />
                <xsl:with-param name="fieldValues" select ="$forDeduplication" />
            </xsl:call-template>

        </xsl:for-each>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='979']/subfield[@code='4']">
                <xsl:choose>
                    <xsl:when test=". != 'rcp' and . != 'aut' and . != 'art' and . != 'cmp' and . != 'ctg' and . != 'dub' and . != 'edt' and . != 'fmk' and . != 'isb' and . != 'lbt' and . != 'lyr' and . != 'pht' and . != 'cre' and . != 'pra' and . != 'rsp' and . != 'com' and . != 'scl' and . != 'cor' and . != 'col' and . != 'ann' and . != 'ins' and . != 'crr' and . != 'pfr' and . != 'arr' and . != 'pat' and . != 'ato' and . != 'scl' and . != 'bnd' and . != 'prf' and . != 'cnd' and . != 'itr' and . != 'sng' and . != 'mus' and . != 'pbl' and . != 'prt' and . != 'bsl' and . != 'plt' and . != 'prm' and . != 'fmk' and . != 'pht' and . != 'hnr' and . != 'mfr' and . != 'ilu' and . != 'ill' and . != 'ctg' and . != 'cmp' and . != 'art' and . != 'ltg' and . != 'clt' and . != 'ppm' and . != 'pra' and . != 'etr' and . != 'rsp' and . != 'fac' and . != 'scr' and . != 'nrt' and . != 'egr' and . != 'fmo' and . != 'dnr' and . != 'sll' and . != 'dte' and . != 'dto' and . != 'ins' and . != 'com' and . != 'own' and . != 'dpt' and . != 'len'">
                        <xsl:for-each select="preceding-sibling::subfield[@code='a']">
                            <xsl:choose>
                                <xsl:when test="matches(following-sibling::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                                    <xsl:value-of select="following-sibling::subfield[@code='0']"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="." />
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                        <xsl:text>##xx##</xsl:text>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='700']">
                <xsl:choose>
                    <xsl:when test="exists(child::subfield[@code='4'])"/>
                    <xsl:when test="exists(child::subfield[@code='t'])"/>
                    <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="child::subfield[@code='0'][1]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="namePortionPerson"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='710']">
                <xsl:choose>
                    <xsl:when test="exists(child::subfield[@code='4'])"/>
                    <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="child::subfield[@code='0'][1]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="namePortionCorporate"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='711']">
                <xsl:choose>
                    <xsl:when test="exists(child::subfield[@code='4'])"/>
                    <xsl:when test="exists(child::subfield[@code='t'])"/>
                    <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="child::subfield[@code='0'][1]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="child::subfield[@code='a']"/>
                        <xsl:if test="exists(child::subfield[@code='n']/text())">
                            <xsl:for-each select="child::subfield[@code='n']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='c']/text())">
                            <xsl:for-each select="child::subfield[@code='c']">
                                <xsl:value-of select="concat(', ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='e']/text())">
                            <xsl:for-each select="child::subfield[@code='e']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='g']/text())">
                            <xsl:for-each select="child::subfield[@code='g']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='d']/text())">
                            <xsl:for-each select="child::subfield[@code='d']">
                                <xsl:value-of select="concat(' (', ., ')')"/>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'contributor_brw_mv'"/>
            <xsl:with-param name="fieldValues" select="$forDeduplication"/>
        </xsl:call-template>

        <!-- generic author facet-->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/navAuthor">
                <xsl:value-of select="concat(replace(., '\[forme avant 2007\]', '', 'i'), '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navAuthor_full'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

        <!-- hierarchical author facet -->
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='979']/subfield[@code='a']">
                <xsl:value-of select="concat('0/', replace(., '/', '\\/'), '/', '##xx##')" />
                <xsl:if test="following-sibling::subfield[@code='4'] != ''">
                    <xsl:for-each select="following-sibling::subfield[@code='4']">
                        <xsl:value-of select="concat('1/', replace(preceding-sibling::subfield[@code='a'], '/', '\\/'), '/', . , '/', '##xx##')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="matches(following-sibling::subfield[@code='P'], '6[01][01]')">
                    <xsl:value-of select="concat('1/', replace(., '/', '\\/'), '/top/', '##xx##')" />
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navAuthor_hierarchical'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>


        <xsl:for-each select="$fragment/sortauthor">
            <field name="author_sort">
                <xsl:value-of select="substring(replace(., '[\W]', ''), 1,25)" />
            </field>
        </xsl:for-each>
    </xsl:template>

    <!-- for the autocomplete suggestions -->
    <xsl:template name="author_first">
        <xsl:param name="fragment" />
        <xsl:variable name="author_first_value">
            <xsl:choose>
                <xsl:when test="$fragment/datafield[matches(@tag, '100|110|111')]">
                    <xsl:for-each select="$fragment/datafield[matches(@tag, '100|110|111')]/subfield[@code='a']">

                        <xsl:if test="following-sibling::subfield[@code='D']">
                            <xsl:value-of select="concat(following-sibling::subfield[@code='D'][1], ' ')" />
                        </xsl:if>
                        <xsl:value-of select="replace(., '\[forme avant 2007\]', '', 'i')" />
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="$fragment/datafield[@tag='950'][matches(child::subfield[@code='P'], '100|110|111')]/subfield[@code='a']">
                        <xsl:for-each select="$fragment/datafield[@tag='950'][position()=1][matches(child::subfield[@code='P'], '100|110|111')]/subfield[@code='a']">
                            <xsl:if test="following-sibling::subfield[@code='D']">
                                <xsl:value-of select="concat(following-sibling::subfield[@code='D'][1], ' ')" />
                            </xsl:if>
                            <xsl:value-of select="replace(., '\[forme avant 2007\]', '', 'i')" />
                        </xsl:for-each>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- warum hier Deduplizierung der values -->
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'author_first'" />
            <xsl:with-param name="fieldValues" select ="$author_first_value" />
        </xsl:call-template>

    </xsl:template>


    <!-- Template zur Erstellung der Titelfelder gemaess VF-Standard -->
    <xsl:template name="titles">
        <xsl:param name="fragment"/>
        <xsl:for-each select="$fragment/datafield[@tag='245']/subfield[@code='a'][empty(following-sibling::subfield[@code='b'])]">
            <field name="title">
                <xsl:value-of select="."/>
            </field>
            <field name="title_short">
                <xsl:value-of select="."/>
            </field>
            <field name="title_sub">
                <xsl:value-of select="following-sibling::subfield[@code='b'][1]"/>
            </field>
        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[@tag='245']/subfield[@code='a'][exists(following-sibling::subfield[@code='b'])]">
            <field name="title">
                <xsl:value-of select="concat(., ' : ', following-sibling::subfield[@code='b'][1])" />
            </field>
            <field name="title_short">
                <xsl:value-of select="." />
            </field>
            <field name="title_sub">
                <xsl:value-of select="following-sibling::subfield[@code='b'][1]" />
            </field>
        </xsl:for-each>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[matches(@tag, '210|222')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '242|243|246|247')]/subfield[matches(@code, 'a|b|n|p')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '130|240|730')]/subfield[matches(@code, 'a|g|m|n|r|f|s|p|k|o')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='245']/subfield[matches(@code, 'n|p')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '505|534|700|710|765|767|775|776|777|786|787')]/subfield[@code='t']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='740']/subfield[matches(@code, 'a|p')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'245')]/subfield[matches(@code, 'a|b')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'246')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'505')]/subfield[@code='t']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'title_alt'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <!-- title variants from GND -->
        <xsl:for-each select="$fragment/datafield[matches(@tag, '7[01][01]')][matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')]/subfield[@code='0']">
            <xsl:variable name="gndnumber" select="text()" />

            <xsl:element name="gndreferencesconcatenatedtitle">
                <xsl:attribute name="fieldName" select="'title_additional_gnd_txt_mv'"/>
                <xsl:attribute name="fieldValue" select="$gndnumber"/>
            </xsl:element>

        </xsl:for-each>
        <xsl:for-each select="$fragment/datafield[matches(@tag, '[17]30')][matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')]/subfield[@code='0']">
            <xsl:variable name="gndnumber" select="text()" />


            <xsl:element name="gndreferencesconcatenatedtitle">
                <xsl:attribute name="fieldName" select="'title_additional_gnd_txt_mv'"/>
                <xsl:attribute name="fieldValue" select="$gndnumber"/>
            </xsl:element>

        </xsl:for-each>
        <xsl:for-each select="$fragment/sorttitle">
            <field name="title_sort">
                <xsl:value-of select="substring(replace(., '[\W]', ''), 1,25)" />
            </field>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="title_browse">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='130']">
                <xsl:choose>
                    <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="child::subfield[@code='0']"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="child::subfield[@code='a']" />
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '240|490')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '245|246')]">
                <xsl:value-of select="child::subfield[@code='a']" />
                <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
                    <xsl:value-of select="concat(' : ', child::subfield[@code='b'][1])"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='505']/subfield[@code='t']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='596' and @ind1='3' and @ind2='0']/subfield[@code='t']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '600')]">
                <xsl:if test="exists(child::subfield[@code='t'])">
                    <xsl:choose>
                        <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                            <xsl:value-of select="child::subfield[@code='0']"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="child::subfield[@code='t']" />
                            <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
                                <xsl:value-of select="concat('. ', child::subfield[@code='p'][1])"/>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>##xx##</xsl:text>
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='630']">
                <xsl:choose>
                    <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="child::subfield[@code='0']"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="child::subfield[@code='a']" />
                        <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
                            <xsl:value-of select="concat('. ', child::subfield[@code='p'][1])"/>
                        </xsl:if>
                        <xsl:text>##xx##</xsl:text>
                        <xsl:if test="child::subfield[@code='t'] and child::subfield[@code='p']/text() != ''">
                            <xsl:value-of select="child::subfield[@code='t']" />
                            <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
                                <xsl:value-of select="concat('. ', child::subfield[@code='p'][1])"/>
                            </xsl:if>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='700']">
                <xsl:if test="exists(child::subfield[@code='t'])">
                    <xsl:choose>
                        <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="child::subfield[@code='0']"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="titlePortion"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='730']">
                <xsl:choose>
                    <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                        <xsl:value-of select="child::subfield[@code='0']"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="titlePortion_uniform"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'title_browse_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='700']">
                <xsl:if test="exists(child::subfield[@code='t'])">
                    <xsl:choose>
                        <xsl:when test="matches(child::subfield[@code='0'][1], '^\(DE-588\)', 'i')">
                            <xsl:value-of select="child::subfield[@code='0']"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="namePortionPerson"/>
                            <xsl:value-of select="' / '"/>
                            <xsl:call-template name="titlePortion"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>##xx##</xsl:text>
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='100']">
                <xsl:if test="exists(following-sibling::datafield[@tag='240'])">
                    <xsl:call-template name="namePortionPerson"/>
                    <xsl:value-of select="' / '"/>
                    <xsl:for-each select="following-sibling::datafield[@tag='240']">
                        <xsl:call-template name="titlePortion_uniform"/>
                    </xsl:for-each>
                    <xsl:text>##xx##</xsl:text>
                </xsl:if>
                <xsl:call-template name="namePortionPerson"/>
                <xsl:value-of select="' / '"/>
                <xsl:for-each select="following-sibling::datafield[@tag='245']">
                    <xsl:call-template name="titlePortion_subtitle"/>
                </xsl:for-each>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='110']">
                <xsl:call-template name="namePortionCorporate"/>
                <xsl:value-of select="' / '"/>
                <xsl:for-each select="following-sibling::datafield[@tag='245']">
                    <xsl:call-template name="titlePortion_subtitle"/>
                </xsl:for-each>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>

        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'work_browse_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>


    <xsl:template name="namePortionPerson">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
            <xsl:value-of select="concat(' ', replace(child::subfield[@code='b'][1], '[,.]', ''), '.')"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='c'] and child::subfield[@code='c']/text() != ''">
            <xsl:value-of select="concat(', ', child::subfield[@code='c'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='d'] and child::subfield[@code='d']/text() != ''">
            <xsl:value-of select="concat(' (', child::subfield[@code='d'][1], ')')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="namePortionCorporate">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="exists(child::subfield[@code='b']/text())">
            <xsl:for-each select="child::subfield[@code='b']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='g']/text())">
            <xsl:value-of select="concat(' (', child::subfield[@code='g'][1], ')')"/>
        </xsl:if>
    </xsl:template>

    <!-- use for title entries with title in $t -->
    <xsl:template name="titlePortion">
        <xsl:value-of select="child::subfield[@code='t']" />
        <xsl:if test="child::subfield[@code='g'] and child::subfield[@code='g']/text() != ''">
            <xsl:for-each select="child::subfield[@code='g']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='m'] and child::subfield[@code='m']/text() != ''">
            <xsl:for-each select="child::subfield[@code='m']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='n'] and child::subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='r'] and child::subfield[@code='r']/text() != ''">
            <xsl:value-of select="concat(', ', child::subfield[@code='r'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='f'] and child::subfield[@code='f']/text() != ''">
            <xsl:value-of select="concat('. ', child::subfield[@code='f'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='s'] and child::subfield[@code='s']/text() != ''">
            <xsl:for-each select="child::subfield[@code='s']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='k'] and child::subfield[@code='k']/text() != ''">
            <xsl:for-each select="child::subfield[@code='k']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='o'] and child::subfield[@code='o']/text() != ''">
            <xsl:value-of select="concat(' ; ', child::subfield[@code='o'][1])"/>
        </xsl:if>
    </xsl:template>

    <!-- use for uniform title entries where title is in $a -->
    <xsl:template name="titlePortion_uniform">
        <xsl:value-of select="child::subfield[@code='a']" />
        <xsl:if test="child::subfield[@code='t'] and child::subfield[@code='t']/text() != ''">
            <xsl:value-of select="concat('. ', child::subfield[@code='t'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='g'] and child::subfield[@code='g']/text() != ''">
            <xsl:for-each select="child::subfield[@code='g']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='m'] and child::subfield[@code='m']/text() != ''">
            <xsl:for-each select="child::subfield[@code='m']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='n'] and child::subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='r'] and child::subfield[@code='r']/text() != ''">
            <xsl:value-of select="concat(', ', child::subfield[@code='r'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='f'] and child::subfield[@code='f']/text() != ''">
            <xsl:value-of select="concat('. ', child::subfield[@code='f'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='s'] and child::subfield[@code='s']/text() != ''">
            <xsl:for-each select="child::subfield[@code='s']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='k'] and child::subfield[@code='k']/text() != ''">
            <xsl:for-each select="child::subfield[@code='k']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='o'] and child::subfield[@code='o']/text() != ''">
            <xsl:value-of select="concat(' ; ', child::subfield[@code='o'][1])"/>
        </xsl:if>
    </xsl:template>

    <!-- use for titles with subtitle -->
    <xsl:template name="titlePortion_subtitle">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
            <xsl:value-of select="concat(' : ', child::subfield[@code='b'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='n'] and child::subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="title_old">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='780']/subfield[@code='t']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'title_old'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="title_new">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='785']/subfield[@code='t']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'title_new'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- series titles -->
    <xsl:template name="series">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='490']/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='v']">
                    <xsl:value-of select="concat(' ', .)" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='773']/subfield[@code='t']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'series'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:for-each select="$fragment/datafield[matches(@tag, '773|8[013][01]')]/subfield[@code='w']">
            <field name="series_ctrlnum_isn_mv">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>

    </xsl:template>

    <xsl:template name="publisher">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[matches(@tag, '26[04]')]/subfield[@code='b']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'publisher_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="pubplace">
    <xsl:param name="fragment" />
    <xsl:variable name="forDeduplication">
        <xsl:for-each select="$fragment/datafield[matches(@tag, '26[04]')]/subfield[@code='a']">
            <xsl:value-of select="concat(., '##xx##')" />
        </xsl:for-each>
    </xsl:variable>

    <xsl:call-template name="prepareDedup">
        <xsl:with-param name="fieldname" select="'pubplace_txt_mv'" />
        <xsl:with-param name="fieldValues" select ="$forDeduplication" />
    </xsl:call-template>

    </xsl:template>

    <!-- HAN Description Level as number for sorting in Archives online -->
    <xsl:template name="descriptionlevel">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='351']">
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Bestand')">
                    <xsl:value-of select="7" />
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Teilbestand')">
                    <xsl:value-of select="6" />
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Serie')">
                    <xsl:value-of select="5" />
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Teilserie')">
                    <xsl:value-of select="4" />
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Dossier')">
                    <xsl:value-of select="3" />
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Teildossier')">
                    <xsl:value-of select="2" />
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Dokument')">
                    <xsl:value-of select="1" />
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'description_level_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='351']">
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Hauptabteilung')">
                    <xsl:text>hauptabteilung</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Abteilung')">
                    <xsl:text>abteilung</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Bestand')">
                    <xsl:text>bestand</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Teilbestand')">
                    <xsl:text>teilbestand</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Serie')">
                    <xsl:text>serie</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Teilserie')">
                    <xsl:text>teilserie</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Dossier')">
                    <xsl:text>dossier</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Teildossier')">
                    <xsl:text>teildossier</xsl:text>
                </xsl:if>
                <xsl:if test="matches(descendant::subfield[@code='c'][1], '^Dokument')">
                    <xsl:text>dokument</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'description_level_name_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- additional content, anything not indexed somewhere else: -->
    <!-- 245, 250, 255, 260, 264, 300, 340, 348, 351, 355, 382, 500, 501, 502, 504, 505, 506, 507, 508, 510, 511, 513, 516, 518, 520, 521,
         522, 524, 525, 530, 533, 534, 536, 538, 540, 541, 544, 545, 546, 555, 561, 562, 563, 581, 583, 584, 585, 590, 595, 596, 773, 800, 810, 811, 830, 852, 856, 880, 947, 949-->
    <xsl:template name="add_fields">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='245']/subfield[@code='c']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='250']/subfield[matches(@code, 'a|b')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='254']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='255']/subfield[matches(@code, 'a|b|c|d|e|f|g')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '26[04]')]/subfield[matches(@code, 'a|b|c')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='300']/subfield[matches(@code, 'a|b|c|e')]">
                <xsl:value-of select="concat(replace(.,
                                             '[\d]+|bd|vol|band|volume|tome',
                                             '',
                                             'i'),
                                             '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='340']/subfield[matches(@code, 'a|b|c|d|e|i')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='348']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '35[15]')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='382']/subfield[matches(@code, 'a|b|d|p|v')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='773']/subfield[matches(@code, 'd|g')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '8[01][01]')]/subfield[matches(@code, 't|v')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='830']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='830']/subfield[@code='n']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='830']/subfield[@code='p']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='830']/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'264')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'264')]/subfield[@code='b']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '50[01478]')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='502']/subfield[matches(@code, 'a|b|c|g')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='505']/subfield[matches(@code, 'a|g|r')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='506']/subfield[matches(@code, 'a|c|u')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '51[1368]')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='510']/subfield[matches(@code, 'a|b|c|u')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='520']/subfield[matches(@code, 'a|b')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '52[1245]')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '530')]/subfield[matches(@code, 'a|u')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '533')]/subfield[matches(@code, 'a|b|c|d|e|n')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '534')]/subfield[matches(@code, 'n|p|t|c')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '53[68]')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '540')]/subfield[matches(@code, 'a|u')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '541')]/subfield[matches(@code, 'a|b|c|d|e|f')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '544')]/subfield[@code='n']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '54[56]')]/subfield[matches(@code, 'a|b')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '540')]/subfield[matches(@code, 'a|u')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '56[1-3]')]/subfield[matches(@code, 'a|b|c|d|e')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '58[145]')]/subfield[matches(@code, 'a')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '583')]/subfield[matches(@code, 'a|b|f|h|i|l')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='590']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='595']/subfield[matches(@code, 'd|e|f|g')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='596']/subfield[matches(@code, 'a|b|c|d|e|f|g|n|t|r|i|s|v')]">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='852']/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '94[79]')]/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '949')]/subfield[@code='q']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='856']/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'245')]/subfield[@code='c']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'250')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'250')]/subfield[@code='b']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'505')]/subfield[@code='g']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'490')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='880'][starts-with(descendant::subfield[@code='6'],'490')]/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'addfields_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[matches(@tag, '540')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'terms_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="localcode">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[matches(@tag, '900|990|998')]/subfield">
                <xsl:if test="matches(@code, '[a-x]')">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='993'][matches(descendant::subfield[@code='x'][1], 'uzb-ZG', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'localcode'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='990'][matches(descendant::subfield[@code='a'][1], '^zuebi', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'turcode_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'localcode'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- ====
         LCSH
         ==== -->
    <xsl:template name="subpers_lcsh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='0']/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <!-- Umgang mit $t klaeren, eigener Eintrag nur aus $a und $t fuer Facette waere moeglich (s.u. bei Koerp) -->
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='0']/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='0']/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_lcsh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtitle_lcsh">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='630'][@ind2='0']/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='p']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='p']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtitle_lcsh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


    </xsl:template>

    <xsl:template name="subtop_lcsh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='0']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='0']/subfield[@code='x']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_lcsh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_lcsh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='0']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='0']/subfield[@code='z']">
                <!--<xsl:for-each select="$fragment/datafield[starts-with(@tag, '6')][@ind2='0']/subfield[@code='z']">-->
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_lcsh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_lcsh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='0']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='0']/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_lcsh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- ====
     MESH
     ==== -->
    <xsl:template name="subpers_mesh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='2']/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='2']/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='2']/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat('(', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_mesh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_mesh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='2']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='2']/subfield[@code='y']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_mesh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtop_mesh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='2']/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='x'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_mesh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_mesh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='2']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='2']/subfield[@code='z']">
                <!--<xsl:for-each select="$fragment/datafield[starts-with(@tag, '6')][@ind2='2']/subfield[@code='z']">-->
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_mesh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_mesh">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='2']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='2']/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_mesh'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subpers_stw">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_stw'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_subpers_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_subpers_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

    </xsl:template>

    <xsl:template name="subtitle_stw">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='630'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='g'][1], ')')" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtitle_stw'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_stw">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_stw'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_time_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_time_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

    </xsl:template>

    <xsl:template name="subtop_stw">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='x'][1], ')')"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_stw'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_topic_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_topic_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

    </xsl:template>

    <xsl:template name="subgeo_stw">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='x'][1], ')')"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_stw'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_geo_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_geo_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="subform_stw">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^stw', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_stw'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_bgs">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^bgs-genre', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_bgs'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subpers_gnd">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_gnd'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='0']">
                <xsl:value-of select="." />
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='0']">
                <xsl:value-of select="." />
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='0']">
                <xsl:value-of select="." />
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_subpers_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtitle_gnd">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='630'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='g'][1], ')')" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtitle_gnd'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_gnd">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_gnd'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_time_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_time_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

    </xsl:template>

    <xsl:template name="subtop_gnd">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='x'][1], ')')"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_gnd'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_topic_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='0']">
                <xsl:value-of select="." />
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_topic_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_gnd">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='x'][1], ')')"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_gnd'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_geo_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'browse_geo_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_all'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>
    </xsl:template>

    <!-- spaeter ausbauen bzw. besser fuer die Facettierung vorbereiten -->
    <xsl:template name="subform_gnd">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_gnd'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- Anreicherung des Index mit GND-Nebenvarianten, Feld wird vorläufig lediglich
     in 'subfull' indexiert (siehe copyField-Direktiven im Schema) (15.01.2013) -->
    <xsl:template name="subenrichment_gnd">
        <xsl:param name="fragment" />
        <xsl:for-each select="$fragment/datafield[matches(@tag, '^[6][0-5]\d')][@ind2='7'][matches(descendant::subfield[@code='2'][1], '^gnd', 'i')]/subfield[@code='0']">
            <xsl:variable name="gndnumber" select="text()" />

            <xsl:element name="gndreferencesconcatenated">
                <xsl:attribute name="fieldName" select="'subgnd_enriched'"/>
                <xsl:attribute name="fieldValue" select="$gndnumber"/>
            </xsl:element>


        </xsl:for-each>
    </xsl:template>

    <!-- Anreicherung des Index mit ausgewählten in Beziehung stehenden Entitäten, siehe gnd.related.xsl -->
    <xsl:template name="subenrichment_gnd_related">
        <xsl:param name="fragment" />
        <xsl:for-each select="$fragment/datafield[matches(@tag, '^[6][01][0]')][@ind2='7'][matches(descendant::subfield[@code='2'][1], '^gnd', 'i')]/subfield[@code='0'] |
                              $fragment/datafield[matches(@tag, '^651')][@ind2='7'][matches(descendant::subfield[@code='2'][1], '^gnd', 'i')]/subfield[@code='0']">
            <xsl:variable name="gndnumber" select="text()" />


            <xsl:element name="gndreferences5xxconcatenated">
                <xsl:attribute name="fieldName" select="'related_gnd_txt_mv'"/>
                <xsl:attribute name="fieldValue" select="$gndnumber"/>
            </xsl:element>



        </xsl:for-each>
    </xsl:template>

    <!-- Genre facet for swisscollections -->
    <xsl:template name="subform_swisscollections">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7']/subfield[@code='a']">
                <xsl:choose>
                    <xsl:when test="matches(following-sibling::subfield[@code='2'][1],'^gnd-content', 'i')">
                        <xsl:if test=". != 'Altkarte' and
                              . != 'Atlas' and
                              . != 'Autograf' and
                              . != 'Bildnis' and
                              . != 'Briefsammlung' and
                              . != 'Einblattdruck' and
                              . != 'Fragment' and
                              . != 'Fotografie' and
                              . != 'Globus' and
                              . != 'Grafik' and
                              . != 'Handschrift' and
                              . != 'Kalender' and
                              . != 'Karikatur' and
                              . != 'Karte' and
                              . != 'Modell' and
                              . != 'Papyrus' and
                              . != 'Plakat' and
                              . != 'Plan' and
                              . != 'Postkarte' and
                              . != 'Stadtplan' and
                              . != 'Weltkarte' and
                              . != 'Zeichnung' and
                              . != 'Zeitschrift' and
                              . != 'Zeitung' ">
                            <xsl:value-of select="concat(., '##xx##')" />
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="matches(following-sibling::subfield[@code='2'][1],'^gnd-carrier', 'i')"/>
                    <xsl:when test="matches(.,'\*\*\*|^-$')"/>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(., '##xx##')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='4']/subfield[@code='a']">
                <xsl:if test=". = 'Aufführungsmaterial' or
                              . = 'Autograph' or
                              . = 'Autograph des Bearbeiters' or
                              . = 'Gutachten' or
                              . = 'Notiz' or
                              . = 'Notizbuch' or
                              . = 'Protokoll' or
                              . = 'Rezepte' or
                              . = 'Vorlesung' ">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='965'][matches(descendant::subfield[@code='2'][1],'^uzb-ZG')]/subfield[@code='a']">
                <xsl:if test=". = 'Abstrakte Kunst [Formschlagwort Sondersammlungen]' or
                              . = 'Allegorische Darstellung [Formschlagwort Sondersammlungen]' or
                              . = 'Ansicht [Formschlagwort Sondersammlungen]' or
                              . = 'Ansichtskarte [Formschlagwort Sondersammlungen]' or
                              . = 'Architekturzeichnung [Formschlagwort Sondersammlungen]' or
                              . = 'Büste [Formschlagwort Sondersammlungen]' or
                              . = 'Ex Libris [Formschlagwort Sondersammlungen]' or
                              . = 'Genrebild (Kunst) [Formschlagwort Sondersammlungen]' or
                              . = 'Geometrisch-abstrakte Kunst [Formschlagwort Sondersammlungen]' or
                              . = 'Geschichtsblatt [Formschlagwort Sondersammlungen]' or
                              . = 'Gestisch-abstrakte Kunst [Formschlagwort Sondersammlungen]' or
                              . = 'Grafik [Formschlagwort Sondersammlungen]' or
                              . = 'Grisaille [Formschlagwort Sondersammlungen]' or
                              . = 'Grundriss [Formschlagwort Sondersammlungen]' or
                              . = 'Konkrete Kunst [Formschlagwort Sondersammlungen]' or
                              . = 'Konstruktivistische Kunst [Formschlagwort Sondersammlungen]' or
                              . = 'Landschaftsgraphik [Formschlagwort Sondersammlungen]' or
                              . = 'Landschaftsmalerei [Formschlagwort Sondersammlungen]' or
                              . = 'Luftbild [Formschlagwort Sondersammlungen]' or
                              . = 'Medaille [Formschlagwort Sondersammlungen]' or
                              . = 'Miniaturbildnis [Formschlagwort Sondersammlungen]' or
                              . = 'Modellbogen [Formschlagwort Sondersammlungen]' or
                              . = 'Muster [Formschlagwort Sondersammlungen]' or
                              . = 'Plastik [Formschlagwort Sondersammlungen]' or
                              . = 'Relief (Kunstwerk) [Formschlagwort Sondersammlungen]' or
                              . = 'Scheibenriss [Formschlagwort Sondersammlungen]' or
                              . = 'Scherenschnitt [Formschlagwort Sondersammlungen]' or
                              . = 'Silhouette [Formschlagwort Sondersammlungen]' or
                              . = 'Stillleben [Formschlagwort Sondersammlungen]' or
                              . = 'Titelblatt [Formschlagwort Sondersammlungen]' or
                              . = 'Titelillustration [Formschlagwort Sondersammlungen]' or
                              . = 'Totenmaske [Formschlagwort Sondersammlungen]' or
                              . = 'Vedute [Formschlagwort Sondersammlungen]' or
                              . = 'Vogelschaubild [Formschlagwort Sondersammlungen]' ">
                    <xsl:value-of select="concat(replace(.,'\[Formschlagwort Sondersammlungen\]', ''), '##xx##')"/>
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='965'][matches(descendant::subfield[@code='2'][1],'^uzb-ZK')]/subfield[@code='a']">
                <xsl:if test=". != 'Panorama [Formschlagwort Sondersammlungen]' and
                              . != 'Vogelschaukarte [Formschlagwort Sondersammlungen]' and
                              . != 'Relief (Geländemodell) [Formschlagwort Sondersammlungen]' and
                              . != 'Manuskriptkarte [Formschlagwort Sondersammlungen]' and
                              . != 'Wandkarte [Formschlagwort Sondersammlungen]' and
                              . != 'Kartenwerk [Formschlagwort Sondersammlungen]' ">
                    <xsl:value-of select="concat(replace(.,'\[Formschlagwort Sondersammlungen\]', ''), '##xx##')"/>
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='900'][matches(descendant::subfield[@code='a'][1], '^IDSIxkasualia')]">
                <xsl:value-of select="concat('Kasualia', '##xx##')"/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:element name="createNavFieldForm">
            <xsl:attribute name="fieldName" select="'navSubform'"/>
            <xsl:attribute name="fieldValues" select="$forDeduplication"/>
        </xsl:element>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][matches(descendant::subfield[@code='2'][1],'^idsmusg')]">
                <xsl:if test="child::subfield[@code='a']/text()">
                    <xsl:value-of select="child::subfield[@code='a'][1]"/>
                    <xsl:if test="exists(child::subfield[matches(@code, 'y|z|v')])">
                        <xsl:value-of select="', '"/>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="child::subfield[@code='y']/text()">
                    <xsl:value-of select="child::subfield[@code='y'][1]"/>
                    <xsl:if test="exists(child::subfield[matches(@code, 'z|v')])">
                        <xsl:value-of select="', '"/>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="child::subfield[@code='z']/text()">
                    <xsl:value-of select="child::subfield[@code='z'][1]"/>
                    <xsl:if test="exists(child::subfield[matches(@code, 'v')])">
                        <xsl:value-of select="', '"/>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="child::subfield[@code='v']/text()">
                    <xsl:value-of select="child::subfield[@code='v'][1]"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='655'][matches(descendant::subfield[@code='2'][1],'^idsmusi')]">
                <xsl:value-of select="child::subfield[@code='v'][1]"/>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'musicGenre_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'musicGenre_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'musicGenre_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][matches(descendant::subfield[@code='2'][1],'^gnd-content')]">
                <xsl:if test="child::subfield[@code='a'] ='Bild' or
                      child::subfield[@code='a'] ='Bildband' or
                      child::subfield[@code='a'] ='Bilderbogen' or
                      child::subfield[@code='a'] ='Bildnis' or
                      child::subfield[@code='a'] ='Einblattdruck' or
                      child::subfield[@code='a'] ='Fotografie' or
                      child::subfield[@code='a'] ='Grafik' or
                      child::subfield[@code='a'] ='Kalender' or
                      child::subfield[@code='a'] ='Karikatur' or
                      child::subfield[@code='a'] ='Künstlerbuch' or
                      child::subfield[@code='a'] ='Modell' or
                      child::subfield[@code='a'] ='Postkarte' or
                      child::subfield[@code='a'] ='Zeichnung' ">
                    <xsl:value-of select="child::subfield[@code='a']"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>

        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'formImage_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'formImage_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'formImage_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- source codes according to
     * http://www.loc.gov/standards/sourcelist/standard-identifier.html
     * LOC organization code database (http://www.loc.gov/marc/organizations/orgshome.html)
     * Online Directory of German ISIL and Library Codes (http://sigel.staatsbibliothek-berlin.de/startseite/)
     -->
    <xsl:template name="id_sourcecodes">
        <xsl:choose>
            <xsl:when test="matches(., '^\(DE-588\)', 'i')">
                <xsl:text>gnd</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(DE-101\)', 'i')">
                <xsl:text>dnb</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(DE-603\)', 'i')">
                <xsl:text>hebis</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(isni\)', 'i')">
                <xsl:text>isni</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(orcid\)', 'i')">
                <xsl:text>orcid</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(rid\)', 'i')">
                <xsl:text>rid</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(scopus\)', 'i')">
                <xsl:text>scopus</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(viaf\)', 'i')">
                <xsl:text>viaf</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(RERO\)', 'i')">
                <xsl:text>rero</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(IDREF\)', 'i')">
                <xsl:text>idref</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^jurivoc', 'i')">
                <xsl:text>jurivoc</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(DE-STW\)', 'i')">
                <xsl:text>stw</xsl:text>
            </xsl:when>
            <xsl:when test="matches(., '^\(ETHUDK\)', 'i')">
                <xsl:text>ethudk</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>undef</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- ====
         RERO
         ==== -->
    <xsl:template name="subpers_rero">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_rero'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_rero">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='y']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_rero'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtop_rero">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='x']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_rero'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_rero">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_rero'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_rero">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'rero')]/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_rero'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- IDREF -->

    <xsl:template name="subpers_idref">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_idref'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_idref">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='y']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_idref'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtop_idref">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_idref'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_idref">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_idref'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_idref">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idref')]/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_idref'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- ======
         IDS BB
         ====== -->
    <xsl:template name="subpers_idsbb">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][starts-with(child::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_idsbb'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtitle_idsbb">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='630'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtitle_idsbb'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>


    <xsl:template name="subtime_idsbb">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='y']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_idsbb'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtop_idsbb">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='x']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_idsbb'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_idsbb">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_idsbb'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_idsbb">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idsbb')]/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_idsbb'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- ===
         ZBZ
         === -->
    <xsl:template name="subpers_idszbz">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][starts-with(child::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_idszbz'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtitle_idszbz">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='630'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtitle_idszbz'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_idszbz">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:choose>
                    <xsl:when test="matches(., '^[0-9]{4}')">
                        <xsl:value-of select="concat('Geschichte ', ., '##xx##')" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(., '##xx##')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='y']">
                <xsl:choose>
                    <xsl:when test="matches(., '^[0-9]{4}')">
                        <xsl:value-of select="concat('Geschichte ', ., '##xx##')" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(., '##xx##')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_idszbz'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtop_idszbz">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='x']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_idszbz'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_idszbz">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_idszbz'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_idszbz">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'idszbz')]/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='695'][starts-with(descendant::subfield[@code='2'][1], 'idszbz')]/subfield[@code='a']">
                <xsl:value-of select="concat(replace(., '\[Formschlagwort Sondersammlungen\]', '', 'i'), '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_idszbz'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- ===
     SBT
     === -->
    <xsl:template name="subpers_sbt">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][starts-with(descendant::subfield[@code='2'][1],'sbt')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_sbt'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_sbt">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='y']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_sbt'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtop_sbt">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='x']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_sbt'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_sbt">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_sbt'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_sbt">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'sbt', 'i')]/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_sbt'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- ===
        JURIVOC
        === -->
    <xsl:template name="subpers_jurivoc">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='610'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='b']">
                        <xsl:value-of select="concat(', ', .)"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[@tag='611'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='n']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='n']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='c']">
                        <xsl:value-of select="concat(', ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='e']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='e']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='g']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='g']">
                        <xsl:value-of select="concat(' ', .)" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:for-each select="following-sibling::subfield[@code='d']">
                        <xsl:value-of select="concat(' (', ., ')')" />
                    </xsl:for-each>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subpers_jurivoc'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtime_jurivoc">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='648'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='y']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtime_jurivoc'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subtop_jurivoc">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='x']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_jurivoc'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subgeo_jurivoc">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <!--<xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='z']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>-->
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subgeo_jurivoc'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="subform_jurivoc">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5]')][@ind2='7'][matches(descendant::subfield[@code='2'][1],'jurivoc', 'i')]/subfield[@code='v']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subform_jurivoc'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- ETHUDK, exists only in 650 -->
    <xsl:template name="subtop_ethudk">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^ethudk', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[@code='x'][1], ')')"/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subtop_ethudk'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="medium_ids">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='382'][matches(descendant::subfield[@code='2'][1],'idsmusi', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'submedium_idsmusi_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'submedium_idsmusi_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='382'][matches(descendant::subfield[@code='2'][1],'idsmusi', 'i')]/subfield[@code='a']">
                <xsl:value-of select="." />
                <xsl:if test="following-sibling::*[2][@code='v']">
                    <xsl:value-of select="concat(', ', following-sibling::subfield[2][@code='v'])" />
                </xsl:if>
                <xsl:if test="following-sibling::*[1][@code='n']">
                    <xsl:value-of select="concat(' (', following-sibling::subfield[1][@code='n'], ')')" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>

        </xsl:variable>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSub_medium'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='382'][matches(descendant::subfield[@code='2'][1],'idsmusi', 'i')]">
                <xsl:for-each select="child::subfield[@code='a']">
                    <xsl:value-of select="." />
                    <xsl:if test="following-sibling::subfield[2][@code='v']">
                        <xsl:value-of select="concat(', ', following-sibling::subfield[2][@code='v'])" />
                    </xsl:if>
                    <xsl:if test="following-sibling::subfield[1][@code='n']">
                        <xsl:value-of select="concat(' (', following-sibling::subfield[1][@code='n'], ')')" />
                    </xsl:if>
                    <xsl:if test="exists(following-sibling::subfield[@code='a'])">
                        <xsl:value-of select="', '"/>
                    </xsl:if>
                </xsl:for-each>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'submedium_idsmusi_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='382'][matches(descendant::subfield[@code='2'][1],'idsmusi', 'i')]">
                <xsl:for-each select="child::subfield[@code='a']">
                    <xsl:value-of select="." />
                    <xsl:if test="following-sibling::subfield[1][@code='n']">
                        <xsl:value-of select="concat(' ', following-sibling::subfield[1][@code='n'])" />
                    </xsl:if>
                    <xsl:if test="following-sibling::subfield[2][@code='v']">
                        <xsl:value-of select="concat(' ', following-sibling::subfield[2][@code='v'])" />
                    </xsl:if>
                    <xsl:if test="exists(following-sibling::subfield[@code='a'])">
                        <xsl:value-of select="' '"/>
                    </xsl:if>
                </xsl:for-each>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'submedium_idsmusi_full_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="medium_rero">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='382'][matches(descendant::subfield[@code='2'][1],'reromusi', 'i')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'submedium_reromusi_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- template to index authority control numbers of entry fields -->
    <xsl:template name="auth_controlnumber">
        <xsl:param name="fragment" />
        <xsl:for-each select="$fragment/datafield/subfield[@code='0']">
            <xsl:choose>
                <xsl:when test="parent::datafield[matches(@tag, '100|110|111|130|700|710|711|730|751')]/subfield[@code='0']">
                    <xsl:variable name="source">
                        <xsl:call-template name="id_sourcecodes"/>
                    </xsl:variable>
                    <xsl:variable name="id" select="substring-after(., ')')" />
                    <field name="{concat('from_', $source, '_isn_mv')}">
                        <xsl:value-of select="$id" />
                    </field>
                </xsl:when>
                <xsl:when test="parent::datafield[matches(@tag, '^[6][0-5]\d')]/subfield[@code='0']">
                    <xsl:variable name="source">
                        <xsl:call-template name="id_sourcecodes"/>
                    </xsl:variable>
                    <xsl:variable name="id" select="substring-after(., ')')"/>
                    <field name="{concat('about_', $source, '_isn_mv')}">
                        <xsl:value-of select="$id" />
                    </field>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>


    <!-- allgemeines Feld subundef, wird nicht in Facette navSubidsbb kopiert (25.05.2012/osc)
     erweitert für Felder 653 (4.9.2012/osc) -->
    <xsl:template name="subundef">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[matches(@tag, '^6[0-5][0-9]')][@ind2='4']/subfield[@code='a'] |
                                  $fragment/datafield[matches(@tag, '^653')]/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##')" />
                <xsl:if test="following-sibling::subfield[@code='b']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='b'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='c']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='c'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='d']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='d'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='t']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='t'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='v']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='v'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='x'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='y']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='y'][1])" />
                </xsl:if>
                <xsl:if test="following-sibling::subfield[@code='z']/text()">
                    <xsl:value-of select="concat(' - ', following-sibling::subfield[@code='z'][1])" />
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'subundef'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- Lokalbeschlagwortung (690) -->
    <xsl:template name="sublocal">
        <xsl:param name="fragment" />
        <xsl:for-each select="$fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A1', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A2', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A3', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A4', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A5', 'i')]/subfield[@code='e'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ubs-AC', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BC', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BD', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BE', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='965'][matches(descendant::subfield[@code='2'][1], 'uzb-ZG', 'i')]/subfield[@code='a'] ">
            <xsl:variable name="source" select="replace(following-sibling::subfield[@code='2']/text(),' ', '')" />
            <field name="{concat('sublocal_', $source, '_txt_mv')}">
                <xsl:value-of select="." />
            </field>
        </xsl:for-each>

        <xsl:for-each select="$fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A1', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A2', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A3', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A4', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A5', 'i')]/subfield[@code='e'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ubs-AC', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BC', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BD', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BE', 'i')]/subfield[@code='a'] |
                              $fragment/datafield[@tag='965'][matches(descendant::subfield[@code='2'][1], 'uzb-ZG', 'i')]/subfield[@code='a']">
            <xsl:variable name="source" select="replace(following-sibling::subfield[@code='2']/text(),' ', '')" />
            <field name="{concat('sublocal_', $source, '_str_mv')}">
                <xsl:value-of select="replace(., ' \[Formschlagwort Sondersammlungen\]', '')" />
            </field>
            <field name="{concat('sublocal_', $source, '_brw_mv')}">
                <xsl:value-of select="replace(., ' \[Formschlagwort Sondersammlungen\]', '')" />
            </field>
        </xsl:for-each>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A1', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A2', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A3', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A4', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'han-A5', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ids-music', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ubs-W1', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ubs-W2', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ubs-W3', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ubs-AC', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BC', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BD', 'i')]/subfield |
                                  $fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], 'ube-BE', 'i')]/subfield |
                                  $fragment/datafield[@tag='960']/subfield |
                                  $fragment/datafield[@tag='965'][matches(descendant::subfield[@code='2'][1], 'uzb-ZG', 'i')]/subfield |
                                  $fragment/datafield[@tag='965'][matches(descendant::subfield[@code='2'][1], 'uzb-ZH', 'i')]/subfield |
                                  $fragment/datafield[@tag='965'][matches(descendant::subfield[@code='2'][1], 'uzb-ZK', 'i')]/subfield ">
                <xsl:if test="matches(@code, '[a-e|t|o|v|x]')">
                    <xsl:value-of select="concat(., '##xx##')" />
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'sublocal'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='600'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='0']">
                <xsl:if test="exists($fragment/datafield[@tag='990'][matches(subfield[@code='a'][1], '^zuebi', 'i')])">
                    <xsl:value-of select="."/>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'turperson_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='651'][@ind2='7'][matches(descendant::subfield[@code='2'][1],'^gnd', 'i')]/subfield[@code='a']">
                <xsl:if test="exists($fragment/datafield[@tag='990'][matches(subfield[@code='a'][1], '^zuebi', 'i')])">
                    <xsl:value-of select="." />
                    <xsl:if test="following-sibling::subfield[@code='g']/text()">
                        <xsl:for-each select="following-sibling::subfield[@code='g']">
                            <xsl:value-of select="concat(', ', .)"/>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="following-sibling::subfield[@code='x']/text()">
                        <xsl:value-of select="concat(' (', following-sibling::subfield[@code='x'][1], ')')"/>
                    </xsl:if>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'turgeo_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'turgeo_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='700'][matches(subfield[@code='4'][1], 'hnr')]/subfield[@code='a']">
                <xsl:if test="exists($fragment/datafield[@tag='655'][matches(subfield[@code='a'], '^Nachruf$')])">
                    <xsl:choose>
                        <xsl:when test="following-sibling::subfield[@code='0']">
                            <xsl:value-of select="following-sibling::subfield[@code='0']"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                            <xsl:if test="following-sibling::subfield[@code='b']/text()">
                                <xsl:value-of select="concat(', ', following-sibling::subfield[@code='b'][1])"/>
                            </xsl:if>
                            <xsl:if test="following-sibling::subfield[@code='c']/text()">
                                <xsl:for-each select="following-sibling::subfield[@code='c']">
                                    <xsl:value-of select="concat(', ', .)"/>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="following-sibling::subfield[@code='d']/text()">
                                <xsl:value-of select="concat(' (', following-sibling::subfield[@code='d'][1], ')')"/>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'nekrolog_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1],'^zbs-bib', 'i')][matches(descendant::subfield[@code='e'][1],'^11$', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'sobibagent_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1],'^zbs-bib', 'i')][matches(descendant::subfield[@code='e'][1],'^12$', 'i')]/subfield[@code='a']">
                <xsl:value-of select="."/>
                <xsl:text>##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'sobibgeo_brw_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>


    <!-- new extensions -->
    <!-- fetching TOC fulltext -->
    <xsl:template name="fulltext">
        <xsl:param name="fragment"/>
        <!-- reduziert (18.08.2011/osc) -->
        <xsl:variable name="di" select="$fragment/myDocID"/>
        <!-- only get fulltexts for Bildungsgeschichte -->
        <xsl:choose>
            <xsl:when test="matches($fragment/myDocID, '^99.*5501$')"/>
            <xsl:when test="matches($fragment/myDocID, '^ZBC')"/>
            <xsl:otherwise>
                <!-- ich muss hier noch etwas einbauen, dass das Feld nicht aufgebaut wird, wenn kein Text zurueckgeliefert wird -->
                <xsl:for-each select="$fragment/uri856">
                    <xsl:variable name="url856" select="."/>
                    <preparedfulltext><xsl:value-of select="$url856"/></preparedfulltext>
                </xsl:for-each></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- time of processing, full marc record -->
    <xsl:template name="proctime_fullrecord">
        <xsl:param name="nativeXML"/>
        <field name="time_processed">
            <xsl:value-of select="concat(adjust-dateTime-to-timezone(current-dateTime(), (PT0H)), 'Z')" />
        </field>
        <field name="fullrecord">
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[&lt;?xml version="1.0"?&gt;</xsl:text>
            <xsl:copy-of select="$nativeXML/fsource/record"/>
            <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
        </field>
    </xsl:template>

    <!-- complete holdings structure as part of the bib record-->
    <xsl:template name="createHoldings">
        <field name="holdings">
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="$holdingsStructure" />
            <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
        </field>
    </xsl:template>

    <xsl:template name="prepareDedup">
        <xsl:param name="fieldname"/>
        <xsl:param name="fieldValues"/>
        <xsl:element name="prepareddedup">
            <xsl:element name="field">
                <xsl:attribute name="name">
                    <xsl:value-of select="$fieldname"/>
                </xsl:attribute>
                <xsl:value-of select="$fieldValues"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template name="tectonics">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <!-- we only build tectonics when this is a "Bestand" -->
            <xsl:if test="matches($fragment/datafield[@tag='351'][1]/subfield[@code='c'], '^Bestand')">
                <!-- we only build tectonics when there is an "Aktenbildner" -->
                <xsl:for-each select="$fragment/datafield[matches(@tag, '100|110|111|700|710|711')]/subfield[@code='4']">
                    <xsl:if test="matches(., '^cre$|^cmp$')">
                        <!-- First Level : Library -->
                        <xsl:for-each select="distinct-values($fragment/datafield[@tag='949']/subfield[@code='b'] |
                                              $fragment/datafield[@tag='852']/subfield[@code='b'] |
                                              $fragment/datafield[@tag='947']/subfield[@code='b'])
                        ">
                            <xsl:variable name="level0" select="." />
                            <xsl:if test="matches($level0, '^A100$')">
                                <!-- 2nd Level UB Basel :  -->
                                <!-- tectonics based on parent record id -->
                                <xsl:for-each select="$fragment/datafield[@tag='830']">
                                    <xsl:choose>
                                        <!-- Signatur NL: Nachlässe -->
                                        <xsl:when test="(./subfield[@code='a']='Signatur NL: Nachlässe') or (./subfield[@code='a']='Druckschriften-Signaturen. kr, Abteilung kr: Musikdrucke und -sammlungen') or (./subfield[@code='a']='Druckschriften-Signaturen. kk, Abteilung kk: Musik- und weitere Drucke. IX, Abteilung kk IX') or (./subfield[@code='a']='Abteilung Archiv: Archiv-Signaturen')">
                                            <xsl:variable name="level1">Nachlässe</xsl:variable>
                                            <xsl:for-each select="$fragment/datafield[@tag='100']/subfield[@code='4']">
                                                <xsl:if test="matches(.,'^cre$|^cmp$')">
                                                    <xsl:variable name="level2">Personen</xsl:variable>
                                                    <xsl:variable name="level3">
                                                        <xsl:call-template name="creator">
                                                            <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:call-template name="tectonics-level3">
                                                        <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                        <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                                        <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                                        <xsl:with-param name="level3" select="$level3"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:if>
                                            </xsl:for-each>
                                            <xsl:for-each select="$fragment/datafield[@tag='110']/subfield[@code='4']">
                                                <xsl:if test="matches(.,'^cre$|^cmp$')">
                                                    <xsl:variable name="level2">Körperschaften</xsl:variable>
                                                    <xsl:variable name="level3">
                                                        <xsl:call-template name="creator">
                                                            <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:call-template name="tectonics-level3">
                                                        <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                        <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                                        <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                                        <xsl:with-param name="level3" select="$level3"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:if>
                                            </xsl:for-each>
                                        </xsl:when>
                                        <!-- record id for Namen-Signaturen. Autogr, Abteilung Autogr: Autographensammlungen -->
                                        <xsl:when test="matches(./subfield[@code='w'], '\(HAN\)000172508DSV05|991170431072505501')">
                                            <xsl:variable name="level1">Autographen</xsl:variable>
                                            <xsl:variable name="level2">
                                                <xsl:call-template name="creator">
                                                    <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:call-template name="tectonics-level2">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:when>

                                    </xsl:choose>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^B583RO$')">
                            <!-- 2nd Level Rohrschach Archive :  -->
                                <xsl:variable name="level1">Archive von Personen und Organisationen</xsl:variable>
                                <xsl:variable name="level2">
                                    <xsl:call-template name="creator">
                                        <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level2">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^LUZHB$')">
                                <!-- 2nd Level : ZHB Luzern-->
                                <xsl:variable name="level1">Archive von Personen und Organisationen</xsl:variable>
                                <xsl:variable name="level2">
                                    <xsl:call-template name="creator">
                                        <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level2">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^SGARK$')">
                                <!-- 2nd Level : Trogen, KB AR-->
                                <xsl:variable name="level1">Archive von Personen und Organisationen</xsl:variable>
                                <xsl:variable name="level2">
                                    <xsl:call-template name="creator">
                                        <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level2">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^B415$')">
                                <!-- 2nd Level : SOB-->
                                <xsl:variable name="level1">Archive von Personen und Organisationen</xsl:variable>
                                <xsl:variable name="level2">
                                    <xsl:value-of select="$fragment/datafield[@tag='245']/subfield[@code='a']"/>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level2">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^B404$')">
                                <!-- 2nd Level : UB Bern Münstergasse-->
                                <xsl:variable name="level1">
                                    <xsl:choose>
                                        <xsl:when test="$fragment/myDocID = '991170538914605501'">
                                            <xsl:value-of select="$fragment/datafield[@tag='245']/subfield[@code='a']"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="creator">
                                                <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level1">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^A150$')">
                                <!-- 2nd Level : ZB Solothurn-->
                                <xsl:variable name="level1">Archive von Personen und Organisationen</xsl:variable>
                                <xsl:variable name="level2">
                                    <xsl:call-template name="creator">
                                        <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level2">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^AKB$')">
                                <!-- 2nd Level : KB Aargau-->
                                <xsl:variable name="level1">Archive von Personen und Organisationen</xsl:variable>
                                <xsl:variable name="level2">
                                    <xsl:call-template name="creator">
                                        <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level2">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="matches($level0, '^SGKBV$')">
                                <!-- 2nd Level : Vadiana -->
                                <xsl:variable name="level1">Archive von Personen und Organisationen</xsl:variable>
                                <xsl:variable name="level2">
                                    <xsl:call-template name="creator">
                                        <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:call-template name="tectonics-level2">
                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
            <!-- for SWA we do it separately as this is quite different -->
            <xsl:for-each select="distinct-values($fragment/datafield[@tag='949']/subfield[@code='b'] |
                                              $fragment/datafield[@tag='852']/subfield[@code='b'] |
                                              $fragment/datafield[@tag='947']/subfield[@code='b'])">
                <xsl:variable name="level0" select="." />
                <xsl:if test="matches($level0, '^A125$')">
                    <!-- 2nd Level SWA :  -->
                    <xsl:for-each select="$fragment/datafield[@tag='990']/subfield[@code='f']">
                        <xsl:choose>
                            <xsl:when test="matches(., '^swapa1$')">
                                <xsl:variable name="level1" select="'Wirtschaftsarchive'"></xsl:variable>
                                <xsl:for-each select="$fragment/datafield[@tag='990']/subfield[@code='f']">
                                    <xsl:choose>
                                        <xsl:when test="matches(.,'^swapafirma$|^swapaverband$')">
                                            <xsl:variable name="level2" select="'Firmen- und Verbandsarchive'"></xsl:variable>
                                            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1], '^stw$', 'i')]/subfield[@code='a']">
                                                <xsl:variable name="level3" select="." />
                                                <xsl:variable name="level4">
                                                    <xsl:call-template name="creator">
                                                        <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:call-template name="tectonics-level4">
                                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                                    <xsl:with-param name="level3" select="$level3"></xsl:with-param>
                                                    <xsl:with-param name="level4" select="$level4"></xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:for-each>
                                        </xsl:when>
                                        <xsl:when test="matches(.,'^swapapnl$')">
                                            <xsl:variable name="level2" select="'Personenarchive und Nachlässe'"></xsl:variable>
                                            <xsl:variable name="level3">
                                                <xsl:call-template name="creator">
                                                    <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:call-template name="tectonics-level3">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                                <xsl:with-param name="level3" select="$level3"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>

                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>

                </xsl:if>
            </xsl:for-each>

            <!-- for ZB Zürich, only data from ZBcollections -->
            <xsl:if test="matches($fragment/datafield[@tag='351'][1]/subfield[@code='c'], '^Bestand') and matches($fragment/datafield[@tag='035'][1]/subfield[@code='a'], 'ZBcollections')">
                <xsl:variable name="level0">Z01</xsl:variable>
                <xsl:variable name="level1" select="$fragment/datafield[@tag='990'][1]/subfield[@code='a']"/>
                <xsl:variable name="level2" select="$fragment/datafield[@tag='245'][1]/subfield[@code='a']"/>

                <xsl:call-template name="tectonics-level2">
                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                </xsl:call-template>

            </xsl:if>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'tectonics_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- Wirtschaftsdokumentation hierarchy -->
    <xsl:template name="businessDocumentation">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">

            <xsl:for-each select="distinct-values($fragment/datafield[@tag='949']/subfield[@code='b'] |
                                              $fragment/datafield[@tag='852']/subfield[@code='b'] |
                                              $fragment/datafield[@tag='947']/subfield[@code='b'])">
                <xsl:variable name="currentBib" select="." />
                <xsl:if test="matches($currentBib, '^A125$')">
                    <!-- Only SWA -->
                    <xsl:for-each select="$fragment/datafield[@tag='990']/subfield[@code='f']">
                        <xsl:choose>
                            <xsl:when test="matches(., '^doksF$|^doksB$|^doksS$')">
                                <xsl:variable name="level0" select="'A125'"></xsl:variable>
                                <xsl:for-each select="$fragment/datafield[@tag='990']/subfield[@code='f']">
                                    <xsl:choose>
                                        <xsl:when test="matches(.,'^doksF$')">
                                            <xsl:variable name="level1" select="'Firmen und Organisationen'"></xsl:variable>
                                            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1], '^stw$', 'i')]/subfield[@code='a']">
                                                <xsl:variable name="level2" select="." />
                                                <xsl:call-template name="tectonics-level2">
                                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:for-each>
                                        </xsl:when>
                                        <xsl:when test="matches(.,'^doksS$')">
                                            <xsl:variable name="level1" select="'Sachthemen Wirtschaft'"></xsl:variable>
                                            <xsl:for-each select="$fragment/datafield[@tag='650'][@ind2='7'][matches(descendant::subfield[@code='2'][1], '^stw$', 'i')]/subfield[@code='a']">
                                                <xsl:variable name="level2" select="." />
                                                <xsl:call-template name="tectonics-level2">
                                                    <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                    <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                                    <xsl:with-param name="level2" select="$level2"></xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:for-each>
                                        </xsl:when>
                                        <xsl:when test="matches(.,'^doksB$')">
                                            <xsl:variable name="level1" select="'Personen Wirtschaft'"></xsl:variable>
                                            <xsl:call-template name="tectonics-level1">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'business_doc_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <!-- Institutionsübergreifende Tektonik nach inhaltlichen Kategorien -->
    <xsl:template name="topicTectonic">
        <xsl:param name="fragment"/>
        <xsl:variable name="forDeduplication">
            <!-- we only build tectonics when this is a "Bestand" -->
            <xsl:if test="matches($fragment/datafield[@tag='351'][1]/subfield[@code='c'], '^Bestand')">
                <!-- we only build tectonics when there is an "Aktenbildner" -->
                <xsl:for-each
                        select="$fragment/datafield[matches(@tag, '100|110|111|700|710|711')]/subfield[@code='4']">
                    <xsl:if test="matches(., '^cre$|^cmp$')">
                        <!-- first level: Topic -->
                        <xsl:for-each
                                select="$fragment/datafield[@tag='690'][matches(descendant::subfield[@code='2'][1], '^han-A6$', 'i')][matches(descendant::subfield[@code='a'][1], '^Bildende Kunst$|^Diverse$|^Literatur$|^Musik$|^Politik und Verwaltung$|^Publizistik$|^Religion$|^Wirtschaft$|^Wissenschaft$')]/subfield[@code='a'][1]">
                            <xsl:variable name="level0" select="."/>
                            <xsl:variable name="level1">
                                <xsl:call-template name="creator">
                                    <xsl:with-param name="fragment" select="$fragment"></xsl:with-param>
                                </xsl:call-template>
                            </xsl:variable>
                            <xsl:call-template name="tectonics-level1">
                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                <xsl:with-param name="level1" select="$level1"></xsl:with-param>
                            </xsl:call-template>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'topic_tectonic_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
    </xsl:template>

    <!-- Aktenbildner / Composer -->
    <xsl:template name="creator">
        <xsl:param name="fragment"></xsl:param>
        <xsl:choose>
            <!-- if there is an aktenbildner -->
            <xsl:when test="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '100|110|111|700|710|711')]">
                <xsl:choose>
                    <xsl:when test="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '100|110')]">
                        <xsl:for-each select="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '100|110')]/subfield[@code='4']">
                            <xsl:if test="matches(., '^cre$|^cmp$')">
                                <xsl:value-of select="replace(preceding-sibling::subfield[@code='a'], '/|&lt;|&gt;', ' ')" />
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '700|710')]">
                            <xsl:for-each select="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '700|710')]/subfield[@code='4']">
                                <xsl:if test="matches(., '^cre$|^cmp$')">
                                    <xsl:value-of select="replace(preceding-sibling::subfield[@code='a'], '/|&lt;|&gt;', ' ')" />
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '111')]">
                        <xsl:for-each select="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '111')]">
                            <xsl:value-of select="replace(./subfield[@code='a'], '/|&lt;|&gt;', ' ')" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:for-each select="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='P'], '711')]">
                            <xsl:value-of select="replace(./subfield[@code='a'], '/|&lt;|&gt;', ' ')" />
                        </xsl:for-each>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <!-- no Aktenbildner, we output Diverse -->
            <xsl:otherwise>
                <xsl:text>Diverse</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="tectonics-level0">
        <xsl:param name="level0"></xsl:param>
        <xsl:text>0</xsl:text>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level0"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:text>##xx##</xsl:text>
    </xsl:template>
    <xsl:template name="tectonics-level1">
        <xsl:param name="level0"></xsl:param>
        <xsl:param name="level1"></xsl:param>
        <xsl:text>1</xsl:text>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level0"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level1"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:text>##xx##</xsl:text>
        <xsl:call-template name="tectonics-level0">
            <xsl:with-param name="level0" select="$level0"></xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template name="tectonics-level2">
        <xsl:param name="level0"></xsl:param>
        <xsl:param name="level1"></xsl:param>
        <xsl:param name="level2"></xsl:param>
        <xsl:text>2</xsl:text>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level0"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level1"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level2"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:text>##xx##</xsl:text>
        <xsl:call-template name="tectonics-level1">
            <xsl:with-param name="level0" select="$level0"></xsl:with-param>
            <xsl:with-param name="level1" select="$level1"></xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template name="tectonics-level3">
        <xsl:param name="level0"></xsl:param>
        <xsl:param name="level1"></xsl:param>
        <xsl:param name="level2"></xsl:param>
        <xsl:param name="level3"></xsl:param>
        <xsl:text>3</xsl:text>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level0"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level1"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level2"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level3"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:text>##xx##</xsl:text>
        <xsl:call-template name="tectonics-level2">
            <xsl:with-param name="level0" select="$level0"></xsl:with-param>
            <xsl:with-param name="level1" select="$level1"></xsl:with-param>
            <xsl:with-param name="level2" select="$level2"></xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template name="tectonics-level4">
        <xsl:param name="level0"></xsl:param>
        <xsl:param name="level1"></xsl:param>
        <xsl:param name="level2"></xsl:param>
        <xsl:param name="level3"></xsl:param>
        <xsl:param name="level4"></xsl:param>
        <xsl:text>4</xsl:text>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level0"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level1"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level2"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level3"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$level4"></xsl:value-of>
        <xsl:text>/</xsl:text>
        <xsl:text>##xx##</xsl:text>
        <xsl:call-template name="tectonics-level3">
            <xsl:with-param name="level0" select="$level0"></xsl:with-param>
            <xsl:with-param name="level1" select="$level1"></xsl:with-param>
            <xsl:with-param name="level2" select="$level2"></xsl:with-param>
            <xsl:with-param name="level3" select="$level3"></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="bibliographies">
        <!-- todo make this via recursive template -->
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="distinct-values($fragment/datafield[@tag='852']/subfield[@code='b'])">
                <xsl:if test="matches(., '^A115$')">
                    <!-- Basler Bibliographie -->
                    <xsl:for-each select="$fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1], 'ubs-FA', 'i')]/subfield[@code='e']">
                        <xsl:variable name="level0" select="'A100'"/>


                        <xsl:variable name="classification">
                            <xsl:choose>
                                <!-- 4 levels -->
                                <xsl:when test="starts-with(., '4.2.5.') or starts-with(., '3.5.4.')">
                                    <xsl:value-of select="substring(., 1, 9)"></xsl:value-of>
                                </xsl:when>
                                <!-- 2 levels -->
                                <xsl:when test="starts-with(., '1.') or starts-with(., '2.1.') or starts-with(., '2.2') or starts-with(., '2.3')">
                                    <xsl:value-of select="substring(., 1, 4)"></xsl:value-of>
                                </xsl:when>
                                <!-- 3 levels -->
                                <xsl:otherwise>
                                    <xsl:value-of select="substring(., 1, 6)"></xsl:value-of>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>

                        <xsl:variable name="digit1" select="substring-before($classification,'.')"/>
                        <!-- the first digit must be 1, 2, 3 or 4 -->
                        <xsl:if test="matches($digit1, '^[1234]$')">

                            <xsl:variable name="rest1" select="substring-after($classification,'.')"/>
                            <xsl:variable name="digit2">
                                <xsl:choose>
                                    <xsl:when test="contains($rest1, '.')">
                                        <xsl:value-of select="substring-before($rest1, '.')"></xsl:value-of>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$rest1"></xsl:value-of>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="rest2" select="substring-after($rest1,'.')"/>

                            <xsl:choose>
                                <xsl:when test="string-length($rest2)=0">
                                    <xsl:call-template name="tectonics-level2">
                                        <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                        <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                        <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:variable name="digit3">
                                        <xsl:choose>
                                            <xsl:when test="contains($rest2, '.')">
                                                <xsl:value-of select="substring-before($rest2, '.')"></xsl:value-of>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$rest2"></xsl:value-of>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <xsl:variable name="rest3" select="substring-after($rest2,'.')"/>

                                    <xsl:choose>
                                        <xsl:when test="string-length($rest3)=0">
                                            <xsl:call-template name="tectonics-level3">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                                <xsl:with-param name="level3" select="concat($digit1, '.', $digit2, '.', $digit3)"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:variable name="digit4">
                                                <xsl:choose>
                                                    <xsl:when test="contains($rest3, '.')">
                                                        <xsl:value-of select="substring-before($rest3, '.')"></xsl:value-of>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$rest3"></xsl:value-of>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable>
                                            <xsl:call-template name="tectonics-level4">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                                <xsl:with-param name="level3" select="concat($digit1, '.', $digit2, '.', $digit3)"></xsl:with-param>
                                                <xsl:with-param name="level4" select="concat($digit1, '.', $digit2, '.', $digit3, '.', $digit4)"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:if>

                <xsl:if test="matches(., '^B412$')">
                    <!-- Berner Bibliographie -->
                    <xsl:for-each select="$fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1], 'ube-GA', 'i')]/subfield[@code='e']">
                        <xsl:variable name="level0" select="'B400'"/>

                        <!-- removes the last digit at the end and everything before space -->
                        <xsl:variable name="classification">
                            <xsl:choose>
                                <xsl:when test="starts-with(., '1.5')">
                                    <xsl:text>1.5</xsl:text>
                                </xsl:when>
                                <xsl:when test="contains(., ' ')">
                                    <xsl:value-of select="substring(substring-before(.,' '), 1, string-length(substring-before(.,' '))-2)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="substring(., 1, string-length(.)-2)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="digit1" select="substring-before($classification,'.')"/>
                        <xsl:if test="matches($digit1, '^[12]$')">
                            <xsl:variable name="rest1" select="substring-after($classification,'.')"/>
                            <xsl:variable name="digit2">
                                <xsl:choose>
                                    <xsl:when test="contains($rest1, '.')">
                                        <xsl:value-of select="substring-before($rest1, '.')"></xsl:value-of>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$rest1"></xsl:value-of>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="rest2" select="substring-after($rest1,'.')"/>

                            <xsl:choose>
                                <xsl:when test="string-length($rest2)=0">
                                    <xsl:call-template name="tectonics-level2">
                                        <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                        <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                        <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:variable name="digit3">
                                        <xsl:choose>
                                            <xsl:when test="contains($rest2, '.')">
                                                <xsl:value-of select="substring-before($rest2, '.')"></xsl:value-of>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$rest2"></xsl:value-of>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <xsl:variable name="rest3" select="substring-after($rest2,'.')"/>

                                    <xsl:choose>
                                        <xsl:when test="string-length($rest3)=0">
                                            <xsl:call-template name="tectonics-level3">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                                <xsl:with-param name="level3" select="concat($digit1, '.', $digit2, '.', $digit3)"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:variable name="digit4">
                                                <xsl:choose>
                                                    <xsl:when test="contains($rest3, '.')">
                                                        <xsl:value-of select="substring-before($rest3, '.')"></xsl:value-of>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$rest3"></xsl:value-of>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable>
                                            <xsl:call-template name="tectonics-level4">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                                <xsl:with-param name="level3" select="concat($digit1, '.', $digit2, '.', $digit3)"></xsl:with-param>
                                                <xsl:with-param name="level4" select="concat($digit1, '.', $digit2, '.', $digit3, '.', $digit4)"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>
            <!-- Zürcher Bibliographie (no dedicated library code) -->
            <xsl:for-each select="$fragment/datafield[@tag='990'][matches(descendant::subfield[@code='a'][1], 'ZueBi', 'i')]/subfield[@code='a']">
            <xsl:variable name="level0" select="'Z01'"/>
                <xsl:if test="matches(., '^ZueBiSACH')">
                    <xsl:variable name="zh-classif">
                        <xsl:value-of select="."></xsl:value-of>
                    </xsl:variable>
                    <xsl:call-template name="tectonics-level1">
                        <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                        <xsl:with-param name="level1" select="$zh-classif"></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>

            <!-- Solothurner Bibliographie -->
            <xsl:for-each select="$fragment/datafield[@tag='990'][matches(descendant::subfield[@code='a'][1], 'sobib', 'i')]/subfield[@code='a']">
                <xsl:for-each select="$fragment/datafield[@tag='691'][matches(descendant::subfield[@code='2'][1], 'zbs-bib', 'i')]/subfield[@code='e']">
                <xsl:variable name="level0" select="'SO'"/>


                <xsl:variable name="classification">
                    <xsl:choose>
                        <xsl:when test="contains(., '.')"><xsl:value-of select="."/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="concat(., '.')"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                    <xsl:variable name="digit1">
                        <xsl:choose>
                            <xsl:when test="string-length(substring-before($classification, '.'))=1">
                                <xsl:value-of select="concat('0', substring-before($classification, '.'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="substring-before($classification, '.')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="rest1" select="substring-after($classification,'.')"/>
                    <xsl:choose>
                        <xsl:when test="string-length($rest1)=0">
                            <xsl:call-template name="tectonics-level1">
                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:variable name="digit2">
                                <xsl:choose>
                                    <xsl:when test="contains($rest1, '.')">
                                        <xsl:value-of select="substring-before($rest1, '.')"></xsl:value-of>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$rest1"></xsl:value-of>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="rest2" select="substring-after($rest1,'.')"/>

                            <xsl:choose>
                                <xsl:when test="string-length($rest2)=0">
                                    <xsl:call-template name="tectonics-level2">
                                        <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                        <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                        <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:variable name="digit3">
                                        <xsl:choose>
                                            <xsl:when test="contains($rest2, '.')">
                                                <xsl:value-of select="substring-before($rest2, '.')"></xsl:value-of>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$rest2"></xsl:value-of>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <xsl:variable name="rest3" select="substring-after($rest2,'.')"/>

                                    <xsl:choose>
                                        <xsl:when test="string-length($rest3)=0">
                                            <xsl:call-template name="tectonics-level3">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                                <xsl:with-param name="level3" select="concat($digit1, '.', $digit2, '.', $digit3)"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:variable name="digit4">
                                                <xsl:choose>
                                                    <xsl:when test="contains($rest3, '.')">
                                                        <xsl:value-of select="substring-before($rest3, '.')"></xsl:value-of>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$rest3"></xsl:value-of>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable>
                                            <xsl:call-template name="tectonics-level4">
                                                <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                                                <xsl:with-param name="level1" select="$digit1"></xsl:with-param>
                                                <xsl:with-param name="level2" select="concat($digit1, '.', $digit2)"></xsl:with-param>
                                                <xsl:with-param name="level3" select="concat($digit1, '.', $digit2, '.', $digit3)"></xsl:with-param>
                                                <xsl:with-param name="level4" select="concat($digit1, '.', $digit2, '.', $digit3, '.', $digit4)"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
            </xsl:for-each>
            </xsl:for-each>

            <!-- Aargauer Bibliographie (no dedicated library code) -->
            <xsl:for-each select="$fragment/datafield[@tag='990'][matches(descendant::subfield[@code='a'][1], '^AG', 'i')]/subfield[@code='a']">
                <xsl:variable name="level0" select="'AG'"/>
                <xsl:if test="matches(., '^AG[1-2][0-9][0-9][0-9] [0-1][0-9]')">
                    <xsl:variable name="ag-classif">
                        <xsl:value-of select="substring(., 8,2)"/>
                    </xsl:variable>
                    <xsl:call-template name="tectonics-level1">
                        <xsl:with-param name="level0" select="$level0"></xsl:with-param>
                        <xsl:with-param name="level1" select="$ag-classif"></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>

        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'bibliographies_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="bibliographies_special">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <!-- Gottfried-Keller Bibliographie (no dedicated library code) -->
            <xsl:for-each select="$fragment/datafield[@tag='900'][matches(descendant::subfield[@code='a'][1], 'IDSZ1GK', 'i')]">
                <xsl:call-template name="keller-gk"/>
            </xsl:for-each>

            <xsl:for-each select="$fragment/datafield[@tag='990'][matches(descendant::subfield[@code='a'][1], 'GK_Z01', 'i')]">
                <xsl:call-template name="keller-gk"/>
            </xsl:for-each>

            <xsl:for-each select="$fragment/datafield[@tag='852'][matches(descendant::subfield[@code='b'], '^Z0[1-8]')][matches(descendant::subfield[@code='j'][1], '^GK ')]">
                <xsl:call-template name="keller-gk"/>
            </xsl:for-each>

            <xsl:for-each select="$fragment/datafield[@tag='852'][matches(descendant::subfield[@code='b'], '^Z0[1-8]')][matches(descendant::subfield[@code='j'][1], '^42|^43')]">
                <xsl:call-template name="tectonics-level1">
                    <xsl:with-param name="level0" select="'GKZ01'"/>
                    <xsl:with-param name="level1" select="'library'"/>
                </xsl:call-template>
            </xsl:for-each>

            <xsl:for-each select="$fragment/datafield[@tag='852'][matches(descendant::subfield[@code='b'], '^Z0[1-8]')]">
                <xsl:if test="exists(../datafield[matches(@tag, '[17]00')][matches(child::subfield[@code='a'], 'Keller, Gottfried')])">
                    <xsl:if test="exists(../datafield[@tag='852'][matches(descendant::subfield[@code='j'][1], 'Ms GK ')]) or exists(../datafield[@tag='655'][matches(descendant::subfield[@code='a'][1], 'Handschrift')]) or exists(../datafield[@tag='300'][matches(descendant::subfield[@code='a'][1], 'Brief')])">
                        <xsl:call-template name="tectonics-level1">
                            <xsl:with-param name="level0" select="'GKZ01'"/>
                            <xsl:with-param name="level1" select="'manuscript'"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="matches($fragment/myDocID , 'ZBCfb30403c101148b5bb4924bcb45cd59d')">
                    <xsl:call-template name="tectonics-level1">
                        <xsl:with-param name="level0" select="'GKZ01'"/>
                        <xsl:with-param name="level1" select="'about'"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="matches($fragment/myDocID , 'ZBCe2963d75f42f4ff1878a235a5735fc96')">
                    <xsl:call-template name="tectonics-level1">
                        <xsl:with-param name="level0" select="'GKZ01'"/>
                        <xsl:with-param name="level1" select="'manuscript'"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="$fragment/datafield[@tag='900'][matches(descendant::subfield[@code='a'][1], 'IDSZ5gkn', 'i')]">
                <xsl:call-template name="tectonics-level1">
                    <xsl:with-param name="level0" select="'GKZ01'"/>
                    <xsl:with-param name="level1" select="'image'"/>
                </xsl:call-template>
            </xsl:for-each>

            <xsl:for-each select="$fragment/datafield[@tag='852'][matches(descendant::subfield[@code='b'], '^Z0[1-8]')][matches(descendant::subfield[@code='j'][1], '^GKN ')]">
                <xsl:call-template name="tectonics-level1">
                    <xsl:with-param name="level0" select="'GKZ01'"/>
                    <xsl:with-param name="level1" select="'image'"/>
                </xsl:call-template>
            </xsl:for-each>

            <!--Bernoulli Briefinventar A117 -->
            <xsl:for-each select="$fragment/datafield[@tag='852'][matches(descendant::subfield[@code='b'], 'A117')]">
                <xsl:for-each select="$fragment/datafield[matches(@tag, '100|700')][matches(descendant::subfield[@code='0'][1], '^\(DE-588\)118656503$|^\(DE-588\)135542146$|^\(DE-588\)118509950$|^\(DE-588\)120475030$|^\(DE-588\)118509969$|^\(DE-588\)117589136$|^\(DE-588\)119166895$|^\(DE-588\)117589144$|^\(DE-588\)119112450$')][matches(descendant::subfield[@code='4'][1], 'aut|rcp')]/subfield[@code='0'][1]">
                    <xsl:variable name="bernoulli_name">
                        <xsl:choose>
                            <xsl:when test=". = '(DE-588)118656503'">
                                <xsl:value-of select="'Bernoulli, Daniel I (1700-1782)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)135542146'">
                                <xsl:value-of select="'Bernoulli, Daniel II (1751-1834)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)118509950'">
                                <xsl:value-of select="'Bernoulli, Jacob I (1655-1705)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)120475030'">
                                <xsl:value-of select="'Bernoulli, Jacob II (1759-1789)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)118509969'">
                                <xsl:value-of select="'Bernoulli, Johann I (1667-1748)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)117589136'">
                                <xsl:value-of select="'Bernoulli, Johann II (1710-1790)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)119166895'">
                                <xsl:value-of select="'Bernoulli, Nicolaus I (1687-1759)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)117589144'">
                                <xsl:value-of select="'Bernoulli, Nicolaus II (1695-1726)'"/>
                            </xsl:when>
                            <xsl:when test=". = '(DE-588)119112450'">
                                <xsl:value-of select="'Hermann, Jacob (1678-1733)'"/>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:variable>

                    <xsl:variable name="bernoulli_gnd">
                        <xsl:value-of select="."/>
                    </xsl:variable>

                    <xsl:value-of select="concat('0/Bernoulli-Briefinventar (BIBB)/', '##xx##')"/>
                    <xsl:value-of select="concat('1/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/', '##xx##')"/>

                    <xsl:for-each select="$fragment/datafield[@tag='979'][matches(descendant::subfield[@code='4'][1], 'rcp|aut')]">
                        <xsl:choose>
                            <xsl:when test="descendant::subfield[@code='0'] != $bernoulli_gnd">
                                <xsl:choose>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)118656503'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Daniel I (1700-1782)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)135542146'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Daniel II (1751-1834)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)118509950'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Jacob I (1655-1705)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)120475030'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Jacob II (1759-1789)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)118509969'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Johann I (1667-1748)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)117589136'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Johann II (1710-1790)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)119166895'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Nicolaus I (1687-1759)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)117589144'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Bernoulli, Nicolaus II (1695-1726)/')"/>
                                    </xsl:when>
                                    <xsl:when test="descendant::subfield[@code='0'] = '(DE-588)119112450'">
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/Hermann, Jacob (1678-1733)/')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/', replace(descendant::subfield[@code='a'], '&lt;|&gt;', ''), '/')"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="not(descendant::subfield[@code='0'])">
                                <xsl:value-of select="concat('2/Bernoulli-Briefinventar (BIBB)/', $bernoulli_name, '/', replace(descendant::subfield[@code='a'], '&lt;|&gt;', ''), '/')"/>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:text>##xx##</xsl:text>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:for-each>

        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'biblio_special_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="keller-gk">
        <xsl:variable name="level0" select="'GKZ01'"/>
        <xsl:variable name="level1">
            <xsl:choose>
                <xsl:when test="exists(../datafield[@tag='100'][matches(child::subfield[@code='a'], 'Keller, Gottfried')])">
                    <xsl:choose>
                        <xsl:when test="matches(substring(../controlfield[@tag='008'], 36, 3), 'ger')">work</xsl:when>
                        <xsl:when test="not(matches(substring(../controlfield[@tag='008'], 36, 3), 'zxx'))">translation</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="exists(../datafield[@tag='600'][matches(child::subfield[@code='a'], 'Keller, Gottfried')])">
                    <xsl:choose>
                        <xsl:when test="not(matches(substring(../controlfield[@tag='008'], 36, 3), 'zxx'))">about</xsl:when>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="($level1 = '')">
                <xsl:call-template name="tectonics-level0">
                    <xsl:with-param name="level0" select="$level0"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="tectonics-level1">
                    <xsl:with-param name="level0" select="$level0"/>
                    <xsl:with-param name="level1" select="$level1"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>


    <xsl:template name="facet_bgs_media">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='347']">
                <xsl:value-of select="concat(descendant::subfield[@code='a'], descendant::subfield[@code='b'])"/>
            </xsl:for-each>

        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'navMedia_bgs_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'icon_code_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="facet_bgs_publplace">
        <xsl:param name="fragment" />
        <xsl:for-each select="$fragment/datafield[@tag='264'][@ind2='0']/subfield[@code='a'] | $fragment/datafield[@tag='651']/subfield[@code='a']">
            <xsl:variable name="forDeduplication">
                <xsl:if test=". != '-' and
                              . !='[S.l.]' and
                              . !='[s.l.]' and
                              . !='[S. l.]' and
                              . !='[s. l.]' and
                              . !='s. l.' and
                              . !='[S.l]' and
                              . !='[...]' and
                              . !='...' and
                              . !='[Var.loc.]' and
                              . !='[var.loc.]' and
                              . !='[Var.loc]' and
                              . !='[var.loc]' and
                              . !='[Var.loc. ]' and
                              . !='[var.loc. ]' and
                              . !='[O.O.]' and
                              . !='[o.O.]' and
                              . !='O.O.' and
                              . !='o.O.' and
                              . !='Div.' and
                              . !='div.' and
                              . !='[Div.]' and
                              . !='[div.]' and
                              . !='[Div. Erscheinungsorte]' and
                              . !='[Ohne Ort]' and
                              . !='Versch. Orte' and
                              . !='[Versch. Orte]' and
                              . !='[Erscheinungsort nicht ermittelbar]' and
                              . !='[Absendeort nicht ermittelbar]' and
                              . !='[Entstehungsort nicht ermittelbar]' and
                              . !='[Herstellungsort nicht ermittelbar]' and
                              . !='[Lieu de publication non identifié]'">
                    <xsl:value-of select="replace(replace(.,'ß', 'ss', 'i'), '\[|\]|\?| etc.| \[etc.\]', '', 'i')" />
                </xsl:if>
            </xsl:variable>


            <xsl:element name="createNavFieldCombined">
                <xsl:attribute name="fieldName" select="'navPublPlace_bgs_str_mv'"/>
                <xsl:attribute name="fieldValue" select="$forDeduplication"/>
            </xsl:element>

        </xsl:for-each>

    </xsl:template>

    <xsl:template name="facet_bgs_text_type">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655'][@ind2='7']/subfield[@code='a']">
                <xsl:if test="matches(following-sibling::subfield[@code='2'][1],'^bgs-genre', 'i')">
                    <xsl:value-of select="concat(., '##xx##')"/>
                </xsl:if>
            </xsl:for-each>

        </xsl:variable>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'navSubform_bgs_str_mv'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

    </xsl:template>

    <xsl:template name="facet_bgs_collection">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
        <xsl:for-each select="$fragment/datafield[@tag='859']/subfield[@code='a']">
            <xsl:variable name="level0">
                <xsl:value-of select="."/>
            </xsl:variable>
            <xsl:variable name="level1">
                <xsl:value-of select="following-sibling::subfield[@code='b']/text()"/>
            </xsl:variable>
            <xsl:variable name="level2">
                <xsl:value-of select="following-sibling::subfield[@code='c']/text()"/>
            </xsl:variable>

            <xsl:choose>
                <xsl:when test="string($level2) = ''">
                    <xsl:call-template name="tectonics-level1">
                        <xsl:with-param name="level0" select="$level0"/>
                        <xsl:with-param name="level1" select="$level1"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="tectonics-level2">
                        <xsl:with-param name="level0" select="$level0"/>
                        <xsl:with-param name="level1" select="$level1"/>
                        <xsl:with-param name="level2" select="$level2"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="matches($level1, 'era-stp|ep-stp|era-supsi|ep-supsi')">
                    <xsl:call-template name="tectonics-level1">
                        <xsl:with-param name="level0" select="substring-after($level1, '-')"/>
                        <xsl:with-param name="level1" select="$level1"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>

        </xsl:for-each>

        </xsl:variable>


        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'collection_bgs_hierarchy_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>


        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='859']/subfield[@code='a']">
                <xsl:value-of select="concat(., '##xx##', following-sibling::subfield[@code='c'], '##xx##')"/>
                <xsl:choose>
                    <xsl:when test="matches(following-sibling::subfield[@code='b'],'^ep-', 'i')"/>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(following-sibling::subfield[@code='b'], '##xx##')"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="matches(following-sibling::subfield[@code='b'],'^era-stp', 'i')">
                        <xsl:value-of select="'stp##xx##stp-era##xx##'"/>
                    </xsl:when>
                    <xsl:when test="matches(following-sibling::subfield[@code='b'],'^ep-stp', 'i')">
                        <xsl:value-of select="'stp##xx##stp-ep##xx##'"/>
                    </xsl:when>
                    <xsl:when test="matches(following-sibling::subfield[@code='b'],'^era-supsi', 'i')">
                        <xsl:value-of select="'supsi##xx##supsi-era##xx##'"/>
                    </xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:for-each>

        </xsl:variable>

        <xsl:element name="createNavFieldCombined">
            <xsl:attribute name="fieldName" select="'collection_bgs_str_mv'"/>
            <xsl:attribute name="fieldValue" select="$forDeduplication"/>
        </xsl:element>

    </xsl:template>

    <xsl:template name="filter_bgs">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='856'][matches(descendant::subfield[@code='3'], 'online|pdf')]">
                <xsl:text>ONL##xx##</xsl:text>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'filter_bgs_str_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="callno_bgs">
        <xsl:param name="fragment" />
        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='090']/subfield[@code='b']">
                <xsl:value-of select="."/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'callno_bgs_strl_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="format_additional_bgs">
        <xsl:param name="fragment"/>

        <xsl:variable name="forDeduplication">
            <xsl:for-each select="$fragment/datafield[@tag='655']/subfield[@code='a']">
                <xsl:choose>
                    <xsl:when test=". = 'Archivalien'">
                        <xsl:value-of select="'Archivalien##xx##Documents d''archive##xx##Fonti d''archivio##xx##Archive material##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Artikel (Lexikon)'">
                        <xsl:value-of select="'Artikel (Lexikon)##xx##Article (enciclopédie ou dictionnaire)##xx##Articolo (enciclopedia)##xx##Article (encyclopaedia)'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Artikel (Zeitschrift/Periodikum)'">
                        <xsl:value-of select="'Artikel (Zeitschrift/Periodikum)##xx##Article (revue ou journal)##xx##Articolo (rivista o giornale)##xx##Article (magazine/periodical)'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Buch/Heft'">
                        <xsl:value-of select="'Buch/Heft##xx##Livre/cahier##xx##Libro/quaderno##xx##Book/issue##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Forschungsdaten'">
                        <xsl:value-of select="'Forschungsdaten##xx##Données de recherche##xx##Dati di ricerca##xx##Research data##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Glasdia'">
                        <xsl:value-of select="'Glasdia##xx##Diapositive en verre##xx##Diapositiva in vetro##xx##Glass slide##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Schulwandbild'">
                        <xsl:value-of select="'Schulwandbild##xx##Tableau mural##xx##Cartellone scolastico##xx##School mural##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Zeichnung'">
                        <xsl:value-of select="'Zeichnung##xx##Dessin##xx##Disegno##xx##Drawing##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Zeitschrift/Heft'">
                        <xsl:value-of select="'Zeitschrift/Heft##xx##Revue/cahier##xx##Rivista/quaderno##xx##Magazine/issue##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Schulbuch'">
                        <xsl:value-of select="'Schulbuch##xx##Libre Scolaire##xx##Libro Di Testo##xx##Textbook##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Lesebuch'">
                        <xsl:value-of select="'Lesebuch##xx##Livre De Lectures##xx##Libro Di Letture##xx##Reader##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Konferenzschrift'">
                        <xsl:value-of select="'Konferenzschrift##xx##Actes De Colloque##xx##Atti Di Convegno##xx##Proceedings##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Lehrmittel'">
                        <xsl:value-of select="'Lehrmittel##xx##Moyen D''Enseignement##xx##Sussidio Didattico##xx##Teaching material##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Festschrift'">
                        <xsl:value-of select="'Festschrift##xx##Hommages Offerts À##xx##Omaggi Offerti A##xx##Festschrift##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Praktikum'">
                        <xsl:value-of select="'Praktikum##xx##Stage Pratique##xx##Tirocinio##xx##Internship##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Anthologie'">
                        <xsl:value-of select="'Anthologie##xx##Antologia##xx##Anthology##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Atlas'">
                        <xsl:value-of select="'Atlas##xx##Atlante##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Ausstellungskatalog'">
                        <xsl:value-of select="'Ausstellungskatalog##xx##Catalogue D''Exposition##xx##Catalogo Di Mostra##xx##Exhibition catalogue##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Beispielsammlung'">
                        <xsl:value-of select="'Beispielsammlung##xx##Collection D''Exemples##xx##Collana Di Esempi##xx##Example collection##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Bestimmungsbuch'">
                        <xsl:value-of select="'Bestimmungsbuch##xx##Livret Pour L''Usager##xx##Manuale D''Uso##xx##Field guide##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Hochschulschrift'">
                        <xsl:value-of select="'Hochschulschrift##xx##Thèse Universitaires##xx##Tesi##xx##University thesis##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Kinderbuch'">
                        <xsl:value-of select="'Kinderbuch##xx##Livre Pour Enfants##xx##Libro Per Ragazzi##xx##Children''s book##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Kindersachbuch'">
                        <xsl:value-of select="'Kindersachbuch##xx##Livre De Divulgation Pour Enfants##xx##Testo Divulgativo Per Ragazzi##xx##Children''s non-fiction book##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Quelle'">
                        <xsl:value-of select="'Quelle##xx##Source Historique##xx##Fonte##xx##Source##xx##'"/>
                    </xsl:when>
                    <xsl:when test=". = 'Statistik'">
                        <xsl:value-of select="'Statistik##xx##Statistique##xx##Statistuica##xx##Statistics##xx##'"/>
                    </xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>

        <xsl:call-template name="prepareDedup">
            <xsl:with-param name="fieldname" select="'format_additional_bgs_txt_mv'" />
            <xsl:with-param name="fieldValues" select ="$forDeduplication" />
        </xsl:call-template>

    </xsl:template>

</xsl:stylesheet>
