<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                version="2.0">

    <xsl:output
            method="xml"
            encoding="UTF-8"
            indent="no"
            omit-xml-declaration="yes"
    />

    <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
        <desc>
            Dieses Skript ist die erste Stufe (Step1) der Verarbeitung im Document-Processing zur Aufbereitung
            der Daten vor der eigentlich Indexierung.

            Stages
            ------
            1) Kopie der kompletten urspruenglichen Struktur in ein eigenes Feld
            2) Erweiterung der urspruenglichen MARC-Struktur mit Feldern und Daten, die fuer das Indexieren
            benoetigt werden
            3) Mappen der erstellten und erweiterten Struktur auf die Indexfelder (primaer in Step2)

            Die Anreicherungen mit XSLT-Verarbeitungen genuegen nicht. Deshalb werden in Java geschriebene
            XSLT-Extensions eingesetzt (alle eingebunden in Step2)
            * Dedublierung Inhalte
            * Anreicherung von TOC / Volltexte (Apache TIKA)
            * Anreicherung GND-Nebenvarianten
            * Anreicherung VIAF-Nebenvarianten

            ************************
            2020 UB Basel
            ************************

        </desc>
    </doc>

    <xsl:template match="*|@*|comment()|processing-instruction()|text()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|comment()|processing-instruction()|text()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="originalMarc" >
            <xsl:copy-of select="/"/>
        </xsl:variable>
        <record>
            <xsl:call-template name="orgRecord" >
                <xsl:with-param name="original" select="$originalMarc"/>
            </xsl:call-template>
            <xsl:apply-templates />
        </record>
    </xsl:template>


    <xsl:template name="orgRecord">
        <xsl:param name="original"/>
        <fsource>
            <xsl:copy-of select="$original/record"/>
        </fsource>
    </xsl:template>

    <xsl:template match="record">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- =====
         DocID
         sortfields
         URIs for TIKA
         ==================================
    -->

    <xsl:template match="controlfield[@tag='001']">
        <myDocID>
            <xsl:value-of select="."/>
        </myDocID>
        <sortauthor>
            <xsl:choose>
                <xsl:when test="following-sibling::datafield[@tag='100']/subfield[@code='a']">
                    <xsl:value-of select="following-sibling::datafield[@tag='100']/subfield[@code='a']"/>
                </xsl:when>
                <xsl:when test="following-sibling::datafield[@tag='700']/subfield[@code='a']">
                    <xsl:value-of select="following-sibling::datafield[@tag='700']/subfield[@code='a']"/>
                </xsl:when>
                <xsl:when test="following-sibling::datafield[@tag='110']/subfield[@code='a']">
                    <xsl:value-of select="following-sibling::datafield[@tag='110']/subfield[@code='a']"/>
                    <xsl:if test="following-sibling::datafield[@tag='110']/subfield[@code='b'] and 
                        following-sibling::datafield[@tag='110']/subfield[@code='b']/text() != ''">
                        <xsl:value-of select="following-sibling::datafield[@tag='110']/subfield[@code='b']"/>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="following-sibling::datafield[@tag='710']/subfield[@code='a']">
                    <xsl:value-of select="following-sibling::datafield[@tag='710']/subfield[@code='a']"/>
                    <xsl:if test="following-sibling::datafield[@tag='710']/subfield[@code='b'] and 
                        following-sibling::datafield[@tag='710']/subfield[@code='b']/text() != ''">
                        <xsl:value-of select="following-sibling::datafield[@tag='710']/subfield[@code='b']"/>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="following-sibling::datafield[@tag='111']/subfield[@code='a']">
                    <xsl:value-of select="following-sibling::datafield[@tag='111']/subfield[@code='a']"/>
                </xsl:when>
                <xsl:when test="following-sibling::datafield[@tag='711']/subfield[@code='a']">
                    <xsl:value-of select="following-sibling::datafield[@tag='711']/subfield[@code='a']"/>
                </xsl:when>
            </xsl:choose>
        </sortauthor>

        <sorttitle>
            <xsl:choose>
                <xsl:when test="following-sibling::datafield[@tag='245']/subfield[@code='a'] != '@'">
                    <xsl:variable name="sorttitle">
                        <xsl:value-of select="following-sibling::datafield[@tag='245']/subfield[@code='a']"/>
                    </xsl:variable>


                    <xsl:choose>
                        <xsl:when test="string-length($sorttitle) &gt; 0 and following-sibling::datafield[@tag='245'][1]/@ind2 !=' '">
                            <xsl:variable name="indicator">
                                <xsl:value-of select="following-sibling::datafield[@tag='245'][1]/@ind2"/>
                            </xsl:variable>
                            <xsl:variable name="sortoutput">
                                <xsl:value-of select="substring($sorttitle, $indicator+1)"/>
                            </xsl:variable>
                            <xsl:value-of select="substring($sortoutput,1,30)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$sorttitle"></xsl:value-of>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="following-sibling::datafield[@tag='490'][1]/subfield[@code='a'][1]">
                    <xsl:value-of select="substring(following-sibling::datafield[@tag='490'][1]/subfield[@code='a'][1],1,30)"/>
                </xsl:when>
                <xsl:when test="following-sibling::datafield[@tag='773'][1]/subfield[@code='t'][1]">
                    <xsl:value-of select="substring(following-sibling::datafield[@tag='773'][1]/subfield[@code='t'][1],1,30)"/>
                </xsl:when>
            </xsl:choose>
        </sorttitle>

        <!-- Preparing fields for TIKA -->
        <xsl:for-each select="following-sibling::datafield[@tag='856']">
            <uri856>
                <xsl:value-of select="child::subfield[@code='u'][1]"/>
            </uri856>
            <xsl:copy-of select="current()"/>
        </xsl:for-each>
    </xsl:template>

    <!-- Dates / Years -->
    <xsl:template match="controlfield[@tag='008']">
        <xsl:variable name="datetype" select="substring(text(),7,1)"/>
        <xsl:variable name="fullDate" select="replace(following-sibling::datafield[@tag='046'][1]/subfield[@code='c'][1], '\.|\[|\]', '')"/>
        <xsl:variable name="year1" select="substring(text()[1],8,4)" />
        <xsl:variable name="year2" >
            <xsl:choose>
                <xsl:when test="matches($datetype, '[mr]')">
                    <xsl:value-of select="replace(substring(text()[1],12,4), '9999', '')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="replace(substring(text()[1],12,4), '9999', string(year-from-date(current-date())))"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($datetype, '[espt]') and matches($year1, '[\d]{4}')">
                <year>
                    <xsl:value-of select="$year1" />
                </year>
                <freshness>
                    <xsl:value-of select="concat($year1, '-01-01T00:00:00Z')" />
                </freshness>
                <sortyear>
                    <xsl:call-template name="sortyear">
                        <xsl:with-param name="fullDate" select="$fullDate"/>
                        <xsl:with-param name="year1" select="$year1"/>
                    </xsl:call-template>
                </sortyear>
            </xsl:when>
            <xsl:when test="matches($datetype, '[mr]') and (matches($year1, '[\d]{4}') or matches($year2, '[\d]{4}'))">
                <xsl:if test="matches($year1, '[\d]{4}')">
                    <year>
                        <xsl:value-of select="$year1" />
                    </year>
                </xsl:if>
                <xsl:if test="matches($year2, '[012][\d]{3}')">
                    <year>
                        <xsl:value-of select="$year2" />
                    </year>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="matches($year1, '[\d]{4}')">
                        <freshness>
                            <xsl:value-of select="concat($year1, '-01-01T00:00:00Z')" />
                        </freshness>
                        <sortyear>
                            <xsl:call-template name="sortyear">
                                <xsl:with-param name="fullDate" select="$fullDate"/>
                                <xsl:with-param name="year1" select="$year1"/>
                            </xsl:call-template>
                        </sortyear>
                    </xsl:when>
                    <xsl:otherwise />
                </xsl:choose>
            </xsl:when>
            <xsl:when test="matches($datetype, '[cdiku]')">
                <xsl:choose>
                    <xsl:when test="matches($year1, '[\d]{4}') and matches($year2, '[012][\d]{3}')">
                        <xsl:if test="$year1 &gt; $year2">
                            <freshness>
                                <xsl:value-of select="concat($year1, '-01-01T00:00:00Z')" />
                            </freshness>
                        </xsl:if>
                        <xsl:if test="$year2 &gt; $year1">
                            <freshness>
                                <xsl:value-of select="concat($year2, '-01-01T00:00:00Z')" />
                            </freshness>
                        </xsl:if>
                        <sortyear>
                            <xsl:call-template name="sortyear">
                                <xsl:with-param name="fullDate" select="$fullDate"/>
                                <xsl:with-param name="year1" select="$year1"/>
                            </xsl:call-template>
                        </sortyear>
                        <xsl:call-template name="yearranges">
                            <xsl:with-param name="year1" as="xs:integer" select="xs:integer($year1)" />
                            <xsl:with-param name="year2" as="xs:integer" select="xs:integer($year2)" />
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="matches($year1, '[\d]{4}') and not(matches($year2, '[\d]{4}'))">
                        <year>
                            <xsl:value-of select="$year1" />
                        </year>
                        <freshness>
                            <xsl:value-of select="concat($year1, '-01-01T00:00:00Z')" />
                        </freshness>
                        <sortyear>
                            <xsl:call-template name="sortyear">
                                <xsl:with-param name="fullDate" select="$fullDate"/>
                                <xsl:with-param name="year1" select="$year1"/>
                            </xsl:call-template>
                        </sortyear>
                    </xsl:when>
                    <xsl:when test="matches($year2, '[\d]{4}') and not(matches($year1, '[\d]{4}'))">
                        <year>
                            <xsl:value-of select="$year2" />
                        </year>
                        <freshness>
                            <xsl:value-of select="concat($year2, '-01-01T00:00:00Z')" />
                        </freshness>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="matches($datetype, 'q')">
                <xsl:choose>
                    <xsl:when test="matches($year1, '[\d]{4}') and not(matches($year2, '2015')) and matches($year2, '[012][\d]{3}')">
                        <xsl:if test="$year1 &gt; $year2">
                            <freshness>
                                <xsl:value-of select="concat($year1, '-01-01T00:00:00Z')" />
                            </freshness>
                        </xsl:if>
                        <xsl:if test="$year2 &gt; $year1">
                            <freshness>
                                <xsl:value-of select="concat($year2, '-01-01T00:00:00Z')" />
                            </freshness>
                        </xsl:if>
                        <sortyear>
                            <xsl:call-template name="sortyear">
                                <xsl:with-param name="fullDate" select="$fullDate"/>
                                <xsl:with-param name="year1" select="$year1"/>
                            </xsl:call-template>
                        </sortyear>
                        <xsl:call-template name="yearranges">
                            <xsl:with-param name="year1" as="xs:integer" select="xs:integer($year1)" />
                            <xsl:with-param name="year2" as="xs:integer" select="xs:integer($year2)" />
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="matches($year1, '[\d]{4}') and (matches($year2, '2015') or not(matches($year2, '[012][\d]{3}')))">
                        <year>
                            <xsl:value-of select="$year1" />
                        </year>
                        <freshness>
                            <xsl:value-of select="concat($year1, '-01-01T00:00:00Z')" />
                        </freshness>
                        <sortyear>
                            <xsl:call-template name="sortyear">
                                <xsl:with-param name="fullDate" select="$fullDate"/>
                                <xsl:with-param name="year1" select="$year1"/>
                            </xsl:call-template>
                        </sortyear>
                    </xsl:when>
                    <xsl:when test="matches($year2, '[\d]{4}') and not(matches($year1, '[\d]{4}'))">
                        <year>
                            <xsl:value-of select="$year2" />
                        </year>
                        <freshness>
                            <xsl:value-of select="concat($year2, '-01-01T00:00:00Z')" />
                        </freshness>
                    </xsl:when>
                    <xsl:otherwise />
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise />
        </xsl:choose>
        <year1><xsl:value-of select="$year1" /></year1>
        <year2><xsl:value-of select="$year2" /></year2>
        <xsl:copy-of select="current()"> </xsl:copy-of>
    </xsl:template>

    <xsl:template name="yearranges">
        <xsl:param name="year1" />
        <xsl:param name="year2" />
        <xsl:for-each select="$year1 to $year2">
            <year>
                <xsl:sequence select="."  />
            </year>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="sortyear">
        <xsl:param name="year1"/>
        <xsl:param name="fullDate"/>
        <xsl:choose>
            <xsl:when test="matches($fullDate, '[\d]{8}')">
                <xsl:value-of select="$fullDate"/>
            </xsl:when>
            <xsl:when test="matches($fullDate, '[\d]{6}')">
                <xsl:value-of select="concat($fullDate, '01')"/>
            </xsl:when>
            <xsl:when test="matches($fullDate, '[\d]{4}')">
                <xsl:value-of select="concat($fullDate, '0101')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($year1, '0101')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ===========================
         Autorenfelder (1xx/7xx/8xx) : Aufbereitung der Facetten
         ===========================
    -->
    <xsl:template match="datafield[@tag='100'] |
                         datafield[@tag='600'] |
                         datafield[@tag='700']">
        <xsl:call-template name="pers_facet" />
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:call-template name="entry_979"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='110'] |
                         datafield[@tag='610'] |
                         datafield[@tag='710']">
        <xsl:call-template name="corp_facet" />
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:call-template name="entry_979"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='111'] |
                         datafield[@tag='611'] |
                         datafield[@tag='711']">
        <xsl:call-template name="conf_facet" />
        <xsl:call-template name="entry_979"/>
    </xsl:template>

    <xsl:template name="pers_facet">
        <xsl:copy-of select="." />
        <!-- Autorenfacette generisch
             ************************
             nur Namensansetzung
             ergaenzt um Lebensdaten, Titel sowie Zaehlung fuer Adel und Geistlichkeit (osc/13.08.2013)
        -->
        <navAuthor>
            <xsl:value-of select="child::subfield[@code='a']"/>
            <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
                <xsl:value-of select="concat(' ', replace(child::subfield[@code='b'][1], '[,.]', ''), '.')" />
            </xsl:if>
            <xsl:if test="child::subfield[@code='c'] and child::subfield[@code='c']/text() != ''">
                <xsl:value-of select="concat(', ', child::subfield[@code='c'][1])" />
            </xsl:if>
            <xsl:if test="child::subfield[@code='d'] and child::subfield[@code='d']/text() != ''">
                <xsl:value-of select="concat(' (', child::subfield[@code='d'][1], ')')" />
            </xsl:if>
        </navAuthor>
    </xsl:template>

    <xsl:template name="corp_facet">
        <xsl:copy-of select="." />
        <!-- Autorenfacette generisch
             ************************
             Koerperschaften, komma-separierte Unterfelder
        -->
        <navAuthor>
            <xsl:value-of select="child::subfield[@code='a']"/>
            <xsl:if test="exists(child::subfield[@code='b']/text())">
                <xsl:for-each select="child::subfield[@code='b']">
                    <xsl:value-of select="concat(', ', .)"/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="exists(child::subfield[@code='g']/text())">
                <xsl:value-of select="concat(' (', child::subfield[@code='g'][1], ')')"/>
            </xsl:if>
        </navAuthor>
    </xsl:template>

    <xsl:template name="conf_facet">
        <xsl:copy-of select="."/>
        <navAuthor>
            <xsl:value-of select="child::subfield[@code='a']"/>
            <xsl:if test="exists(child::subfield[@code='n']/text())">
                <xsl:for-each select="child::subfield[@code='n']">
                    <xsl:value-of select="concat(' ', .)"/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="exists(child::subfield[@code='c']/text())">
                <xsl:for-each select="child::subfield[@code='c']">
                    <xsl:value-of select="concat(', ', .)"/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="exists(child::subfield[@code='e']/text())">
                <xsl:for-each select="child::subfield[@code='e']">
                    <xsl:value-of select="concat(' ', .)"/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="exists(child::subfield[@code='g']/text())">
                <xsl:for-each select="child::subfield[@code='g']">
                    <xsl:value-of select="concat(' ', .)"/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="exists(child::subfield[@code='d']/text())">
                <xsl:for-each select="child::subfield[@code='d']">
                    <xsl:value-of select="concat(' (', ., ')')"/>
                </xsl:for-each>
            </xsl:if>
        </navAuthor>
    </xsl:template>

    <!-- Autorenfacette source-spezifisch
         ********************************
         Namensansetzungen, inkl. Lebensdaten, Titel, Zaehlung
    -->
    <xsl:template name="entry_979">
        <!-- Beispiele fuer Ausschlusskriterien von Relatoren (spezifische Liste, oder alle) (15.05.2014 / osc)
        <xsl:template match="datafield[@tag='950'][matches(child::subfield[@code='P'], '100|110|111|700|710|711')][not(matches(child::subfield[@code='4'], 'fmo|ths'))]">
        <xsl:template match="datafield[@tag='950'][matches(child::subfield[@code='P'], '100|110|111|700|710|711')][not(exists(child::subfield[@code='4']))]">
        -->
        <!-- this is a random MARC tag for transport use only (13.08.2013/osc) -->
        <datafield tag="979">
            <subfield code="a">
                <xsl:choose>
                    <!-- persons -->
                    <xsl:when test="matches(@tag, '100|600|700')">
                        <xsl:value-of select="child::subfield[@code='a']" />
                        <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
                            <xsl:value-of select="concat(' ', replace(child::subfield[@code='b'][1], '[,.]', ''), '.')" />
                        </xsl:if>
                        <xsl:if test="child::subfield[@code='c'] and child::subfield[@code='c']/text() != ''">
                            <xsl:value-of select="concat(', ', child::subfield[@code='c'][1])" />
                        </xsl:if>
                        <xsl:if test="child::subfield[@code='d'] and child::subfield[@code='d']/text() != ''">
                            <xsl:value-of select="concat(' (', child::subfield[@code='d'][1], ')')" />
                        </xsl:if>
                    </xsl:when>
                    <!-- corporations -->
                    <xsl:when test="matches(@tag, '110|610|710')">
                        <xsl:value-of select="child::subfield[@code='a']"/>
                        <xsl:if test="exists(child::subfield[@code='b']/text())">
                            <xsl:for-each select="child::subfield[@code='b']">
                                <xsl:value-of select="concat(', ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='g']/text())">
                            <xsl:value-of select="concat(' (', child::subfield[@code='g'][1], ')')"/>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="matches(@tag, '111|611|711')">
                        <xsl:value-of select="child::subfield[@code='a']"/>
                        <xsl:if test="exists(child::subfield[@code='n']/text())">
                            <xsl:for-each select="child::subfield[@code='n']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='c']/text())">
                            <xsl:for-each select="child::subfield[@code='c']">
                                <xsl:value-of select="concat(', ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='e']/text())">
                            <xsl:for-each select="child::subfield[@code='e']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='g']/text())">
                            <xsl:for-each select="child::subfield[@code='g']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(child::subfield[@code='d']/text())">
                            <xsl:for-each select="child::subfield[@code='d']">
                                <xsl:value-of select="concat(' (', ., ')')"/>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise />
                </xsl:choose>
            </subfield>
            <xsl:if test="exists(child::subfield[@code='4']/text())">
                <xsl:for-each select="child::subfield[@code='4']">
                    <subfield code="4">
                        <xsl:value-of select="." />
                    </subfield>
                </xsl:for-each>
            </xsl:if>
            <subfield code="P">
                <xsl:value-of select="@tag"/>
            </subfield>
            <xsl:if test="exists(child::subfield[@code='0']/text())">
                <xsl:for-each select="child::subfield[@code='0']">
                    <subfield code="0">
                        <xsl:value-of select="." />
                    </subfield>
                </xsl:for-each>
            </xsl:if>
        </datafield>
    </xsl:template>

    <xsl:template match="leader">
        <xsl:variable name="LDR_06" select="substring(./text(), 7, 1)"/>
        <xsl:variable name="LDR_07" select="substring(./text(), 8, 1)"/>
        <xsl:variable name="p007_00">
            <xsl:for-each select="following-sibling::controlfield[@tag=007]">
                <xsl:value-of select="substring(./text(), 1, 1)"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="p007_01">
            <xsl:for-each select="following-sibling::controlfield[@tag=007]">
                <xsl:value-of select="substring(./text(), 2, 1)"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="rec007_03">
            <xsl:for-each select="following-sibling::controlfield[@tag=007][matches(substring(., 1, 2), 'sd', 'i')]">
                <xsl:value-of select="substring(./text(), 4, 1)"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="rec007_06">
            <xsl:for-each select="following-sibling::controlfield[@tag=007][matches(substring(., 1, 2), 'sd', 'i')]">
                <xsl:value-of select="substring(./text(), 7, 1)"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="p008_20" select="substring(following-sibling::controlfield[@tag='008']/text(), 21, 1)"/>
        <xsl:variable name="p008_21" select="substring(following-sibling::controlfield[@tag='008']/text(), 22, 1)"/>
        <xsl:variable name="p008_33" select="substring(following-sibling::controlfield[@tag='008']/text(), 34, 1)"/>
        <xsl:variable name="worktitle" select="following-sibling::datafield[@tag='240']/subfield[@code='a']"/>
        <xsl:variable name="physicalDescription">
            <xsl:for-each select="following-sibling::datafield[@tag='300']">
                <xsl:for-each select="subfield[@code='b']">
                    <xsl:value-of select="concat(., '###')"/>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="medium" select="following-sibling::datafield[@tag='340']/subfield[@code='a']"/>
        <xsl:variable name="level" select="following-sibling::datafield[@tag='351']/subfield[@code='c']"/>
        <xsl:variable name="carrier" select="following-sibling::datafield[@tag='338']/subfield[@code='b']"/>
        <xsl:variable name="gnd-music" select="following-sibling::datafield[@tag='348'][matches(child::subfield[@code='2'], '^gnd-music', 'i')
        ]/subfield[@code='a']"/>
        <xsl:variable name="gnd-carrier" select="following-sibling::datafield[@tag='655'][matches(child::subfield[@code='2'][1], '^gnd-carrier', 'i')
        ]/subfield[@code='a']"/>
        <xsl:variable name="gnd-content" select="following-sibling::datafield[@tag='655'][matches(child::subfield[@code='2'][1], '^gnd-content', 'i')
        ]/subfield[@code='a']"/>
        <xsl:variable name="other-content" select="following-sibling::datafield[@tag='655'][@ind2='4']/subfield[@code='a']"/>
        <xsl:variable name="uzbZK_965" select="following-sibling::datafield[@tag='965'][matches(child::subfield[@code='2'], '^uzb-ZK', 'i')]/subfield[@code='a']"/>
        <xsl:variable name="uzbZG_965" select="following-sibling::datafield[@tag='965'][matches(child::subfield[@code='2'], '^uzb-ZG', 'i')]/subfield[@code='a']"/>
        <xsl:variable name="local_990bibliography_a"  select="following-sibling::datafield[@tag='990'][matches(child::subfield[@code='a'][1], '^zuebi|sobib|^AG', 'i')]/subfield[@code='a']"/>
        <xsl:variable name="local_990bibliography_b"  select="following-sibling::datafield[@tag='990'][matches(child::subfield[@code='b'], '^basb|^bbg')]/subfield[@code='b']"/>
        <xsl:variable name="local_990f"  select="following-sibling::datafield[@tag='990']/subfield[@code='f']"/>


        <xsl:element name="formatParent">
            <xsl:choose>
                <xsl:when test="$local_990f='doksB' or $local_990f='doksF' or $local_990f='doksS'">
                    <xsl:value-of select="'DS'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'p')">
                    <xsl:value-of select="'AR'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'c|d')">
                    <xsl:value-of select="'SM'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'j')">
                    <xsl:value-of select="'MR'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'i')">
                    <xsl:value-of select="'TR'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'e|f')">
                    <xsl:value-of select="'MP'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 't')">
                    <xsl:value-of select="'MS'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'k|r')">
                    <xsl:value-of select="'IO'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'g') and matches($p008_33, 's|f|i')">
                    <xsl:value-of select="'IO'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'g')">
                    <xsl:value-of select="'VM'"/>
                </xsl:when>
                <xsl:when test="matches($LDR_06, 'a')">
                    <xsl:choose>
                        <xsl:when test="exists($local_990bibliography_a) or exists($local_990bibliography_b)">
                            <xsl:value-of select="'BE'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'PR'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'XY'"/>
                </xsl:otherwise>

            </xsl:choose>
        </xsl:element>

        <xsl:if test="matches($LDR_06, 'a|k|r|g') and ($local_990f='einblatt-bs' or $gnd-content='Einblattdruck' or $uzbZG_965='Einblattdruck [Formschlagwort]')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'einblatt'"/>
            </xsl:element>
        </xsl:if>


        <!-- Sheet music -->
        <xsl:if test="matches($LDR_06, 'c|d') and (matches($p008_20, 'a|l') or $gnd-music='Partitur')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'score'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and (matches($p008_20, 'b') or $gnd-music='Studienpartitur')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'study'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and (matches($p008_20, 'c') or $gnd-music='Klavierauszug')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'reduction'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and $gnd-music='Klavierbearbeitung'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'piano'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and (matches($p008_20, 'e') or $gnd-music='Particell')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'condensed'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and (matches($p008_20, 'h') or $gnd-music='Chorpartitur')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'chorus'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and (matches($p008_21, 'd|e|f') or $gnd-music='Stimmen')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'parts'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and (matches($p008_20, 'j') or $gnd-music='Klavier-Direktionsstimme' or $gnd-music='Violin-Direktionsstimme')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'conductor'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'c|d') and $gnd-music='Aufführungsmaterial'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'performance'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'd')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'musicms'"/>
            </xsl:element>
        </xsl:if>

        <!-- Music recording -->
        <xsl:if test="matches($LDR_06, 'j') and (matches($rec007_03, 'f') or matches($rec007_06, 'g') or $gnd-carrier='CD')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'cd'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'j') and (matches($rec007_03, 'a|b|c|d|e') or matches($rec007_06, 'a|b|c|d|e|f') or $gnd-carrier='Schallplatte')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'record'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'j') and (matches($p007_01, 's') or $carrier ='ss')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'cassette'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'j') and (matches($p007_01, 't') or $carrier ='st')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'reel'"/>
            </xsl:element>
        </xsl:if>


        <!-- Maps -->
        <xsl:if test="matches($LDR_06, 'e|f') and $gnd-content='Altkarte'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'oldmap'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and (matches($p007_01, 'd') or $gnd-content='Atlas')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'atlas'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and (matches($p007_00, 'd') or $gnd-content='Globus')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'globus'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and $gnd-content='Karte'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'map'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and $gnd-content='Plan'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'plan'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and $gnd-content='Weltkarte'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'world'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and $gnd-content='Stadtplan'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'city'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and (matches($p007_01, 'k'))">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'profil'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and $gnd-content='Kartenwerk'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'mapwork'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and $gnd-content='Wandkarte'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'wall'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and (matches($p007_01, 'y') or $uzbZK_965='Panorama [Formschlagwort Sondersammlungen]' or $uzbZK_965='Vogelschaukarte [Formschlagwort Sondersammlungen]' )">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'panorama'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'e|f') and $uzbZK_965='Relief [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'relief'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'e') and (matches($p007_01, 'j')) and not ($local_990f='doksB' or $local_990f='doksF' or $local_990f='doksS'))">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'mapprint'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'f') or $uzbZK_965='Manuskriptkarte \[Formschlagwort Sondersammlungen\]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'mapms'"/>
            </xsl:element>
        </xsl:if>

        <!-- Dokumentensammlung -->
        <xsl:if test="$local_990f='doksB'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'person'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="$local_990f='doksF'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'corporate'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="$local_990f='doksS'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'subject'"/>
            </xsl:element>
        </xsl:if>

        <!-- Text -->
        <xsl:if test="(not($local_990bibliography_a) and not($local_990bibliography_b)) and (matches($LDR_06, 'a') and ((matches($LDR_07, 's') and matches($p008_21, 'q|r')) or $gnd-content = 'Zeitschrift' or $gnd-content ='Zeitung'))">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'journal'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(not($local_990bibliography_a) and not($local_990bibliography_b))  and (matches($LDR_06, 'a') and $worktitle='Werke')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'work'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 't') and $gnd-content='Handschrift' and not ($medium='Papyrus' or $gnd-content='Papyrus' or matches($p008_33, 'i') or $gnd-content='Briefsammlung' or $gnd-content='Fragment' or matches($physicalDescription, 'Typoskript') or $other-content = 'Typoskript')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'textms'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 't') and ($medium='Papyrus' or $gnd-content='Papyrus')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'papyrus'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 't') and $gnd-content='Fragment'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'fragment'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 't') and (matches($p008_33, 'i') or $gnd-content='Briefsammlung')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'letter'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 't') and $gnd-content='Autograf'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'autograph'"/>
            </xsl:element>
        </xsl:if>

        <!-- additional formats for special cases which have to placed in the hierarchy depending on value -->

        <xsl:if test="matches($LDR_06, 'd|f') and $gnd-content='Autograf'">
            <xsl:element name="formatAdditional">
                <xsl:value-of select="'autograph'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($physicalDescription, 'Typoskript') or $other-content='Typoskript'">
            <xsl:element name="formatAdditional">
                <xsl:value-of select="'typoskript'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="$gnd-content='Gegenstand'">
            <xsl:element name="formatAdditional">
                <xsl:value-of select="'objekt'"/>
            </xsl:element>
        </xsl:if>


        <!-- Images and Objects -->
        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Aquarell [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'aquarell'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Aquatinta [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'aquatinta'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Autotypie [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'autotypie'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Collage [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'collage'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Cyanotypie [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'cyanotypie'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Daguerreotypie [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'daguerreotypie'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Digitaler Ausdruck [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'digitalerAusdruck'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Druckplatte [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'druckplatte'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Druckstock [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'druckstock'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Fotokopie [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'fotokopie'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Glasmalerei [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'glasmalerei'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Prägedruck [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'praegedruck'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Gouache [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'gouache'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Heliogravüre [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'heliogravuere'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Holzschnitt [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'holzschnitt'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Holzstich [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'holzstich'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Kontaktkopie [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'kontaktkopie'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Kupferstich [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'kupferstich'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Laserprint [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'laserprint'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Lichtdruck (Druckverfahren) [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'lichtdruck'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Linolschnitt [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'linolschnitt'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Lithographie [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'lithographie'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Offsetdruck [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'offsetdruck'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Pastellzeichnung [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'pastellzeichnung'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Photochrom [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'photochrom'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Radierung [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'radierung'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Rakeltiefdruck [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'rakeltiefdruck'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Rasterdruck [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'rasterdruck'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Schabkunst [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'schabkunst'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Stahlstich [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'stahlstich'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Strichklischee [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'strichklischee'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Umrissradierung [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'umrissradierung'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Zinkdruck [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'zinkdruck'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $uzbZG_965='Gemälde [Formschlagwort Sondersammlungen]'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'gemaelde'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Bildnis'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'bildnis'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') and $gnd-content='Fotografie') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'fotografie'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Grafik'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'grafik'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Kalender'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'kalender'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Karikatur'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'karikatur'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Modell'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'modell'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Plakat'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'plakat'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Postkarte'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'postkarte'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="(matches($LDR_06, 'k|r') or (matches($LDR_06, 'g') and matches($p008_33, 's|f|i'))) and $gnd-content='Zeichnung'">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'zeichnung'"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="matches($LDR_06, 'r')">
            <xsl:element name="formatSpecific">
                <xsl:value-of select="'objekt'"/>
            </xsl:element>
        </xsl:if>

        <!-- bibliographies -->

        <xsl:if test="exists($local_990bibliography_a) or exists($local_990bibliography_b)">
            <xsl:element name="formatParent_bibliography">
                <xsl:value-of select="'BE'"/>
            </xsl:element>
        </xsl:if>

        <xsl:for-each select="$local_990bibliography_a">
            <xsl:if test="matches(.,'^zuebi', 'i')">
                <xsl:element name="formatSpecific_bibliography">
                    <xsl:value-of select="'zurich'"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="matches(.,'^sobib', 'i')">
                <xsl:element name="formatSpecific_bibliography">
                    <xsl:value-of select="'solothurn'"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="matches(.,'^AG', 'i')">
                <xsl:element name="formatSpecific_bibliography">
                    <xsl:value-of select="'aargau'"/>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>

        <xsl:for-each select="$local_990bibliography_b">
            <xsl:if test="matches(.,'^basb')">
                <xsl:element name="formatSpecific_bibliography">
                    <xsl:value-of select="'basel'"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="matches(.,'^bbg')">
                <xsl:element name="formatSpecific_bibliography">
                    <xsl:value-of select="'bern'"/>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>

        <!-- Archival material -->
        <xsl:if test="exists($level)">
            <xsl:element name="formatParent_archival">
                <xsl:value-of select="'AR'"/>
            </xsl:element>
        </xsl:if>

        <xsl:for-each select="$level">
            <xsl:if test="matches(., '^Bestand|^Teilbestand')">
                <xsl:element name="formatSpecific_archival">
                    <xsl:value-of select="'fonds'"/>
                </xsl:element>
            </xsl:if>

            <xsl:if test="matches(., '^Serie|^Teilserie')">
                <xsl:element name="formatSpecific_archival">
                    <xsl:value-of select="'series'"/>
                </xsl:element>
            </xsl:if>

            <xsl:if test="matches(., '^Dossier|^Teildossier')">
                <xsl:element name="formatSpecific_archival">
                    <xsl:value-of select="'file'"/>
                </xsl:element>
            </xsl:if>

            <xsl:if test="matches(., '^Dokument')">
                <xsl:element name="formatSpecific_archival">
                    <xsl:value-of select="'item'"/>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>


    </xsl:template>


</xsl:stylesheet>
