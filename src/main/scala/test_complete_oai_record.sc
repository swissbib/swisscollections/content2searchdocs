
import ch.swisscollections.searchdocs.utilities.XmlHelpers.usePostTransformations
import ch.swisscollections.searchdocs.utilities.{AppProperties, XmlHelpers}
import scala.xml.{Node, XML}

//Initialisieren von allen Plugins die konfiguriert worden sind
//damit wird auch die komplette Konfiguration gelesen und bereitgestellt
AppProperties.initTransformation()


val record = """<record><header><identifier>oai:alma.41SLSP_UBS:99479390105504</identifier><datestamp>2020-08-21T05:57:47Z</datestamp><setSpec>ubs_full_set</setSpec></header><metadata><marc:record xmlns:marc="http://www.loc.gov/MARC21/slim"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"><leader>00000nam a2200000uc 4500</leader><controlfield tag="001">99479390105504</controlfield><controlfield tag="005">20200119192743.0</controlfield><controlfield tag="008">840509s1981    gw            00| | ger d</controlfield><datafield tag="020" ind1=" " ind2=" "><subfield code="a">3923098030</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(swissbib)076405559-41slsp_network</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">076405559</subfield><subfield code="9">ExL</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(IDSBB)000047939DSV01</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(EXLNZ-41SLSP_NETWORK)991079447399705501</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">SzZuIDS BS/BE A100</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield><subfield code="d">CH-ZuSLS</subfield></datafield><datafield tag="072" ind1=" " ind2="7"><subfield code="a">rw</subfield><subfield code="2">SzZuIDS BS/BE</subfield></datafield><datafield tag="082" ind1="1" ind2="4"><subfield code="a">340</subfield><subfield code="2">15</subfield></datafield><datafield tag="084" ind1=" " ind2=" "><subfield code="a">PQ 750</subfield><subfield code="2">rvk</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Hofmann, Albert</subfield><subfield code="c">Sozialwissenschaftler</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Sozialhilfe für politische Betatigung?</subfield><subfield code="b">muss die Sozialhilfe Kosten für die Teilnahme an einer Demonstration übernehmen? : Rechtsgutachten, Gerichtsentscheidungen</subfield><subfield code="c">Albert Hofmann, Utz Krahmer, Günther Stahlmann</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">Frankfurt am Main</subfield><subfield code="b">Fachhochschule</subfield><subfield code="c">1981</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">75 S.</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="b">txt</subfield><subfield code="2">rdacontent</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="b">n</subfield><subfield code="2">rdamedia</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="b">nc</subfield><subfield code="2">rdacarrier</subfield></datafield><datafield tag="490" ind1="1" ind2=" "><subfield code="a">Materialien zur Sozialarbeit und Sozialpolitik</subfield><subfield code="v">Bd. 4</subfield></datafield><datafield tag="650" ind1=" " ind2="7"><subfield code="a">Sozialhilfe</subfield><subfield code="z">Deutschland (BRD)</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="650" ind1=" " ind2="7"><subfield code="a">Demonstrationsfreiheit</subfield><subfield code="z">Deutschland (BRD)</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="700" ind1="1" ind2=" "><subfield code="a">Krahmer, Utz</subfield></datafield><datafield tag="700" ind1="1" ind2=" "><subfield code="a">Stahlmann, Günther</subfield></datafield><datafield tag="830" ind1="_" ind2="0"><subfield code="a">Materialien zur Sozialarbeit und Sozialpolitik</subfield><subfield code="v">4</subfield><subfield code="w">(IDSBB)000052785DSV01</subfield></datafield><datafield tag="852" ind1="4" ind2=" "><subfield code="b">A100</subfield><subfield code="c">MAG</subfield><subfield code="j">UBH Bro 43429</subfield><subfield code="8">22128104850005504</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="3">22128104850005504</subfield><subfield code="o">BOOK</subfield><subfield code="x">Akz: 82,8061 JÃ¨aggi</subfield><subfield code="2">0UBU0937167</subfield><subfield code="c">MAG</subfield><subfield code="1">23128104840005504</subfield><subfield code="p">01</subfield><subfield code="j">UBH Bro 43429</subfield><subfield code="b">A100</subfield></datafield></marc:record></metadata></record>"""
//println(record)
val xml = XML.loadString(record)
val recordToTransform = (xml \ "metadata" \ "record").toString()

val rawRecordNoNs = XmlHelpers.transformNS(recordToTransform)
val completeHoldings = XmlHelpers.transformCollectAllHoldings(rawRecordNoNs)

val weededRecord = XmlHelpers.transformWeedHoldings(rawRecordNoNs)
val preparationStepGreen = XmlHelpers.transformSpecialGreen(weededRecord)
val step1 = XmlHelpers.transformStep1(preparationStepGreen)
val finalTransformation = XmlHelpers.transformStep2 (step1) {
  completeHoldings
}

val parsedRecord = XmlHelpers.parseRecord(finalTransformation)
val completeSolrDocWithoutWrappers: Node = usePostTransformations(parsedRecord).asInstanceOf[Node]

println(XmlHelpers.makeXmlOneLiner(completeSolrDocWithoutWrappers))
