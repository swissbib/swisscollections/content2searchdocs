
import ch.swisscollections.searchdocs.helper.{TemplateCreator, TemplateTransformer, XSLTDataObject}

import ch.swisscollections.searchdocs.extensions.IDocProcPlugin
import ch.swisscollections.searchdocs.utilities.XmlHelpers.usePostTransformations
import ch.swisscollections.searchdocs.utilities.{AppProperties, XmlHelpers}

import scala.collection.convert.ImplicitConversions.`collection asJava`
import scala.xml.Node

//Initialisieren von allen Plugins die konfiguriert worden sind
//damit wird auch die komplette Konfiguration bereitgestellt
AppProperties.plugins.use.forEach { ext =>
  val zClass = Class.forName(ext)
  //todo: how to make this appropriate in the scala environment?
  val instance = zClass.getDeclaredConstructor().newInstance().asInstanceOf[IDocProcPlugin];
  //val instance = zClass.newInstance().asInstanceOf[IDocProcPlugin]
  instance.initPluginAppConfig()
}


val singleRecord = """<record><leader>     cam a22     4  4500</leader><controlfield tag="001">005784956</controlfield><controlfield tag="003">CHVBK</controlfield><controlfield tag="005">20191119115632.0</controlfield><controlfield tag="008">110121s2010    gw            00    ger  </controlfield><datafield tag="020" ind1=" " ind2=" "><subfield code="a">978-3-89794-168-7</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(OCoLC)887465597</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(ABN)000557080</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">CH-AaAKB</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Plešnik</subfield><subfield code="D">Marko</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Bosnien und Herzegowina</subfield><subfield code="b">unterwegs zwischen Adria und Save</subfield><subfield code="c">Marko Plešnik</subfield></datafield><datafield tag="250" ind1=" " ind2=" "><subfield code="a">3., aktual. und erw. Aufl.</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">Berlin</subfield><subfield code="b">Trescher</subfield><subfield code="c">2010</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">1 Band</subfield><subfield code="b">Ill.</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="a">Text</subfield><subfield code="b">txt</subfield><subfield code="2">rdacontent/ger</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="a">ohne Hilfsmittel zu benutzen</subfield><subfield code="b">n</subfield><subfield code="2">rdamedia/ger</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="a">Band</subfield><subfield code="b">nc</subfield><subfield code="2">rdacarrier/ger</subfield></datafield><datafield tag="651" ind1=" " ind2="7"><subfield code="a">Bosnien</subfield><subfield code="0">(DE-588)4007826-7</subfield><subfield code="2">gnd</subfield></datafield><datafield tag="651" ind1=" " ind2="7"><subfield code="a">Herzegowina</subfield><subfield code="0">(DE-588)4024643-7</subfield><subfield code="2">gnd</subfield></datafield><datafield tag="655" ind1=" " ind2="7"><subfield code="a">Führer</subfield><subfield code="2">gnd</subfield></datafield><datafield tag="691" ind1=" " ind2="7"><subfield code="B">u</subfield><subfield code="u">914.971</subfield><subfield code="2">abn YN</subfield></datafield><datafield tag="912" ind1=" " ind2="7"><subfield code="a">900</subfield><subfield code="2">CH-AaABN</subfield></datafield><datafield tag="950" ind1=" " ind2=" "><subfield code="B">ABN</subfield><subfield code="P">100</subfield><subfield code="E">1-</subfield><subfield code="a">Plešnik</subfield><subfield code="D">Marko</subfield></datafield><datafield tag="898" ind1=" " ind2=" "><subfield code="a">BK020000</subfield><subfield code="b">XK020000</subfield><subfield code="c">XK020000</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="B">ABN</subfield><subfield code="C">ABN51</subfield><subfield code="D">ABN01</subfield><subfield code="E">000557080</subfield><subfield code="F">KSZ</subfield><subfield code="b">KSZ</subfield><subfield code="c">KSZFH</subfield><subfield code="j">KSZ 914.971</subfield><subfield code="p">BZZ5509631</subfield><subfield code="q">000557080</subfield><subfield code="r">000010</subfield><subfield code="0">Kantonsschule Zofingen</subfield><subfield code="1">Freihand</subfield><subfield code="3">BOOK</subfield><subfield code="4">11</subfield><subfield code="5">1 Monat</subfield></datafield></record>"""
val sR = """<marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"><leader>00000nam a2200000uc 4500</leader><controlfield tag="001">99537220105504</controlfield><controlfield tag="005">20200119195152.0</controlfield><controlfield tag="008">840509s1979    gw       m    00| | ger d</controlfield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(swissbib)083003428-41slsp_network</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">083003428</subfield><subfield code="9">ExL</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(IDSBB)000053722DSV01</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(EXLNZ-41SLSP_NETWORK)991029235339705501</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">SzZuIDS BS/BE</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield><subfield code="d">CH-ZuSLS</subfield></datafield><datafield tag="072" ind1=" " ind2="7"><subfield code="a">mg</subfield><subfield code="2">SzZuIDS BS/BE</subfield></datafield><datafield tag="082" ind1="1" ind2="4"><subfield code="a">610</subfield><subfield code="2">15</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Hentschel, Ralph-Christian</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Franz Schönenberger (1865-1933)</subfield><subfield code="b">Biobibliographie eines ärztlichen Vertreters der Naturheilkunde</subfield><subfield code="c">von Ralph-Christian Hentschel</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">München</subfield><subfield code="c">1979</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">186 S., Portr.</subfield><subfield code="c">22 cm</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="b">txt</subfield><subfield code="2">rdacontent</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="b">n</subfield><subfield code="2">rdamedia</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="b">nc</subfield><subfield code="2">rdacarrier</subfield></datafield><datafield tag="502" ind1=" " ind2=" "><subfield code="a">Diss. med. München</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Naturheilkunde / Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="600" ind1="1" ind2="7"><subfield code="a">Schönenberger, Franz</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="650" ind1=" " ind2="7"><subfield code="a">Medizin</subfield><subfield code="z">Deutschland</subfield><subfield code="y">Geschichte 19. Jh</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="655" ind1=" " ind2="7"><subfield code="a">Hochschulschrift</subfield><subfield code="2">gnd-content</subfield></datafield><datafield tag="852" ind1="4" ind2=" "><subfield code="b">A100</subfield><subfield code="c">MAG</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="8">2245498210005504</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="3">2245498210005504</subfield><subfield code="o">BOOK</subfield><subfield code="x">Akz: 81 T</subfield><subfield code="2">A1001274783</subfield><subfield code="c">MAG</subfield><subfield code="1">2345498200005504</subfield><subfield code="p">01</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="b">A100</subfield></datafield></marc:record></metadata></record>"""

//compile the XML Templates

lazy val removeNs = {
  new TemplateCreator(AppProperties.templates.transfomerImpl,
    AppProperties.templates.removeAllNs).createTransformerFromResource
}

lazy val startTransformer = {
  new TemplateCreator(AppProperties.templates.transfomerImpl,
    AppProperties.templates.holdingsStructure).createTransformerFromResource
}

lazy val weedTransformer = {
  new TemplateCreator(AppProperties.templates.transfomerImpl,
    AppProperties.templates.weedholdings).createTransformerFromResource
}

lazy val step1Transformer = {
  new TemplateCreator(AppProperties.templates.transfomerImpl,
    AppProperties.templates.step1).createTransformerFromResource
}

lazy val specialGreenTransformer = {
  new TemplateCreator(AppProperties.templates.transfomerImpl,
    AppProperties.templates.specialGreen).createTransformerFromResource
}

lazy val step2Transformer = {
  new TemplateCreator(AppProperties.templates.transfomerImpl,
    AppProperties.templates.step2).createTransformerFromResource
}


val pureXml = new TemplateTransformer(sR).transform(removeNs)
println(pureXml)

val holdings = new TemplateTransformer(pureXml).transform(startTransformer)

val dataObject = new XSLTDataObject
dataObject.additions.put("holdingsStructure", holdings)
dataObject.record = pureXml

val weedHoldingsTransformation = new TemplateTransformer(dataObject.record).transform(weedTransformer)
dataObject.record = weedHoldingsTransformation

val step1Transformation = new TemplateTransformer(dataObject.record).transform(step1Transformer)
dataObject.record = step1Transformation
val specialGreenTransformation = new TemplateTransformer(dataObject.record).transform(specialGreenTransformer)

dataObject.record = specialGreenTransformation

step2Transformer.setParameter("holdingsStructure", dataObject.additions.get("holdingsStructure"))
val step2Transformation = new TemplateTransformer(dataObject.record).transform(step2Transformer)

println(step2Transformation)

val parsedRecord = XmlHelpers.parseRecord(step2Transformation)
val completeSolrDocWithoutWrappers = usePostTransformations(parsedRecord).asInstanceOf[Node]

println(completeSolrDocWithoutWrappers)


