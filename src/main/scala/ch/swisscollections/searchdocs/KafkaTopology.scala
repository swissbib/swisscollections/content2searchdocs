/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.searchdocs

import ch.swisscollections.searchdocs.stream.functions.Marc2SearchDocTransform
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.kstream.KStream
import org.apache.kafka.streams.scala.{StreamsBuilder, _}
import org.apache.logging.log4j.scala.Logging


class KafkaTopology extends Logging {

  import Serdes._


  def build(): Topology = {
    val builder = new StreamsBuilder
    val source = builder.stream[String, String](SettingsFromFile.getKafkaInputTopic)
    source.mapValues(Marc2SearchDocTransform.apply _)
      .filter(
        //in case there happens a failure in transformation the document isn't sent into the topic
        //we have to decide if it would be better to create a second branch in the DAG for these
        //kind of exceptions and put the record into it - for later
      (_,transformedDoc) => !transformedDoc.isEmpty)
      //filter is needed to avoid messages larger than the default message
      // length of Kafka todo: adjust this on the Kafka cluster
      .filter((_,v) => v.getBytes.length <= 990000)
      .to(SettingsFromFile.getKafkaOutputTopic)


    builder.build()
  }


}
