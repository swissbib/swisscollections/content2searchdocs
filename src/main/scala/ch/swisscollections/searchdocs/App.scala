/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs

import java.time.Duration
import ch.swisscollections.searchdocs.utilities.AppProperties
import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}
import java.util.UUID

object App extends App with Logging {

  val topology = new KafkaTopology


  //AppProperties.MONGO_USER = SettingsFromFile.getMongoDBuser
  //AppProperties.MONGO_PASSWD = SettingsFromFile.getMongoDBpasswd
  //AppProperties.TOC_USER = SettingsFromFile.getTocDBUser
  //AppProperties.TOC_PASSWD = SettingsFromFile.getTocDBPassword

  AppProperties.initTransformation()
  //val fullConf = AppProperties.plugins.fulltextConf
  //val gndConf = AppProperties.plugins.gndConf

  val props = SettingsFromFile.getKafkaStreamsSettings
  //println(props)
  //props.put("application.id",UUID.randomUUID().toString)

  val streams = new KafkaStreams(topology.build(), props)
  //val streams = new KafkaStreams(topology.build(), SettingsFromFile.getKafkaStreamsSettings)

  val shutdownGracePeriodMs = 10000

  logger.trace("Starting stream processing")
  Try(
    streams.start()
  ) match {
    case Success(_) =>
      logger.info("Workflow successful. Finishing...")
    case Failure(f) =>
      logger.error(s"Aborting due to errors: ${f.getMessage}")
      sys.exit(1)
  }

  sys.ShutdownHookThread {
    streams.close(Duration.ofMillis(shutdownGracePeriodMs))
  }


}
