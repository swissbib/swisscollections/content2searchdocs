/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.searchdocs

import java.util.Properties

import ch.memobase.settings.SettingsLoader

import scala.collection.JavaConverters._

trait Settings {

  def getKafkaStreamsSettings: Properties

  def getKafkaInputTopic: String

  def getKafkaOutputTopic: String

  def getKafkaReportTopic: String

  def getId: String

}


/**
 * Acts as a single point of fetching for the SettingsLoader
 */
object SettingsFromFile extends Settings {
  private val settings = new SettingsLoader(List(
    "tocDBuser",
    "tocDBpassword",
    "connectionstring",
    "collection",
    "mongodb"
  )
    .asJava,
    "app.yml",
    false,
    true,
    false,
    false)


  def getApplicationId: String = sys.env.getOrElse("APPLICATION_ID", "")

  def getMongoConnection:String = settings.getAppSettings.getProperty("connectionstring")

  def getMongoCollection:String = settings.getAppSettings.getProperty("collection")

  def getMongoDB:String = settings.getAppSettings.getProperty("mongodb")


  def getKafkaStreamsSettings: Properties = settings.getKafkaStreamsSettings

  def getKafkaInputTopic: String = settings.getInputTopic

  def getKafkaOutputTopic: String = settings.getOutputTopic

  def getKafkaReportTopic: String = settings.getProcessReportTopic

  def getId: String = "app.content2searchdocs"


}
