/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.stream.functions

import ch.swisscollections.searchdocs.utilities.XmlHelpers.usePostTransformations
import ch.swisscollections.searchdocs.utilities.{AppProperties, XmlHelpers}
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}
import scala.xml.{Node, XML}

object Marc2SearchDocTransform extends Function2[String,String,String] with Logging {

  def initTransformations(): Unit = AppProperties.initTransformation()

  override def apply(v1: String, v2: String): String = {

    //logger.info(s"transformation of $v2")

    Try[String] {
      val xml = XML.loadString(v2)

      val recordToTransform = (xml \ "metadata" \ "record").toString()

      val rawRecordNoNs = XmlHelpers.transformNS(recordToTransform)
      val completeHoldings = XmlHelpers.transformCollectAllHoldings(rawRecordNoNs)

      val weededRecord = XmlHelpers.transformWeedHoldings(rawRecordNoNs)
      val step1 = XmlHelpers.transformStep1(weededRecord)
      val specialStepGreen = XmlHelpers.transformSpecialGreen(step1)
      val resultFinalTransformation = XmlHelpers.transformStep2(specialStepGreen) {
        completeHoldings
      }

      val parsedRecordResult = XmlHelpers.parseRecord(resultFinalTransformation)

      val stringAfterTransformation = usePostTransformations(parsedRecordResult).asInstanceOf[Node].toString()
      XmlHelpers.useOnlineStripSpace(stringAfterTransformation)


    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(s"Problem with record $v2")
        logger.error(exception.getStackTrace)
        "" //handover en empty String which is filtered out in the next operator

    }
  }
}
