/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs

import java.io.{File, FileInputStream, InputStream}
import java.nio.file.{Files, Paths}
import java.util.stream.Collectors
import java.util.zip.GZIPInputStream

import ch.swisscollections.searchdocs.extensions.IDocProcPlugin
import ch.swisscollections.searchdocs.utilities.{AppProperties, CreateDSV11, CreateIsbn, CreateNavFieldCombined, CreateNavFieldCombinedSingleValue, CreateNavFieldForm, EnrichFulltext, FilterDuplicates, GndReferenceConcatenated, GndReferences5xxConcatenated, LibraryFacets, OptionsParser}
import ch.swisscollections.searchdocs.utilities.OptionsParser.OptionMap
import ch.swisscollections.searchdocs.utilities.XmlHelpers.{parseRecord, usePostTransformations}
import helper.{TemplateCreator, TemplateTransformer, XSLTDataObject}

import scala.collection.mutable
import scala.io.Source
import scala.xml.parsing.ConstructingParser
import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node, Text}
import scala.collection.convert.ImplicitConversions.`seq AsJavaList`

//to make it easy
//object Main extends App {
object Main {

  //val options: OptionMap = OptionsParser.nextOption(Map(), args.toList)
  //only to reduce number of App types
  val options: OptionMap = OptionsParser.nextOption(Map(), List.empty)
  assert(options.contains(Symbol("indir")))

  private object FileList {

    def getFileList: java.util.List[String] = {
      Files.walk(Paths.get(options(Symbol("indir")).toString)).
        filter(Files.isRegularFile(_)).map[String](_.toString).collect(Collectors.toList[String])

    }

    def getStream(fileName: String): InputStream = {
      val infile = new File(fileName)

      val nameInFile: String = infile.getAbsoluteFile.getName
      val zipped = if (nameInFile.matches(""".*?.gz$""")) true else false
      val source: InputStream = if (zipped) {
        new GZIPInputStream(new FileInputStream(infile))
      } else {
        new FileInputStream(infile)
      }
      source
    }

  }

  FileList.getFileList.forEach(f => {

    val source = FileList.getStream(f)
    XMLTransformation.transformSource(source)
  })


  object XMLTransformation {

    private def isRecord(line: String) = line.startsWith("<record>")

    /*
    private val TRANSFORMERIMPL = "net.sf.saxon.TransformerFactoryImpl"
    private val holdingsStructure = "collect.holdings.xsl"
    private val weedholdings = "weedholdings.xsl"
    private val solrstep1 = "swissbib.solr.step1.xsl"
    private val specialgreen = "vufind.special.green.xsl"
    private val solrstep2 = "swissbib.solr.step2.xsl"
    */

    private lazy val removeNs = {
      new TemplateCreator(AppProperties.templates.transfomerImpl,
        AppProperties.templates.removeAllNs).createTransformerFromResource
    }

    private lazy val startTransformer = {
      new TemplateCreator(AppProperties.templates.transfomerImpl,
        AppProperties.templates.holdingsStructure).createTransformerFromResource
    }

    private lazy val weedTransformer = {
      new TemplateCreator(AppProperties.templates.transfomerImpl,
        AppProperties.templates.weedholdings).createTransformerFromResource
    }

    private lazy val step1Transformer = {
      new TemplateCreator(AppProperties.templates.transfomerImpl,
        AppProperties.templates.step1).createTransformerFromResource
    }

    private lazy val specialGreenTransformer = {
      new TemplateCreator(AppProperties.templates.transfomerImpl,
        AppProperties.templates.specialGreen).createTransformerFromResource
    }

    private lazy val step2Transformer = {
      new TemplateCreator(AppProperties.templates.transfomerImpl,
        AppProperties.templates.step2).createTransformerFromResource
    }


    def transformSource(stream: InputStream): Unit = {

      //initialize configured plugins
      AppProperties.plugins.use.forEach { ext =>
        val zClass = Class.forName(ext)
        //todo: how to make this appropriate in the scala environment?
        val instance = zClass.getDeclaredConstructor().newInstance().asInstanceOf[IDocProcPlugin];
        //val instance = zClass.newInstance().asInstanceOf[IDocProcPlugin]
        instance.initPluginAppConfig()
      }


      var count = 0
      val it = Source.fromInputStream(stream).getLines()
      var zahler = 0
      for (line <- it if isRecord(line)) {

        //val sR = """<marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"><leader>00000nam a2200000uc 4500</leader><controlfield tag="001">99537220105504</controlfield><controlfield tag="005">20200119195152.0</controlfield><controlfield tag="008">840509s1979    gw       m    00| | ger d</controlfield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(swissbib)083003428-41slsp_network</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">083003428</subfield><subfield code="9">ExL</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(IDSBB)000053722DSV01</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(EXLNZ-41SLSP_NETWORK)991029235339705501</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">SzZuIDS BS/BE</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield><subfield code="d">CH-ZuSLS</subfield></datafield><datafield tag="072" ind1=" " ind2="7"><subfield code="a">mg</subfield><subfield code="2">SzZuIDS BS/BE</subfield></datafield><datafield tag="082" ind1="1" ind2="4"><subfield code="a">610</subfield><subfield code="2">15</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Hentschel, Ralph-Christian</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Franz Schönenberger (1865-1933)</subfield><subfield code="b">Biobibliographie eines ärztlichen Vertreters der Naturheilkunde</subfield><subfield code="c">von Ralph-Christian Hentschel</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">München</subfield><subfield code="c">1979</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">186 S., Portr.</subfield><subfield code="c">22 cm</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="b">txt</subfield><subfield code="2">rdacontent</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="b">n</subfield><subfield code="2">rdamedia</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="b">nc</subfield><subfield code="2">rdacarrier</subfield></datafield><datafield tag="502" ind1=" " ind2=" "><subfield code="a">Diss. med. München</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Naturheilkunde / Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="600" ind1="1" ind2="7"><subfield code="a">Schönenberger, Franz</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="650" ind1=" " ind2="7"><subfield code="a">Medizin</subfield><subfield code="z">Deutschland</subfield><subfield code="y">Geschichte 19. Jh</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="655" ind1=" " ind2="7"><subfield code="a">Hochschulschrift</subfield><subfield code="2">gnd-content</subfield></datafield><datafield tag="852" ind1="4" ind2=" "><subfield code="b">A100</subfield><subfield code="c">MAG</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="8">2245498210005504</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="3">2245498210005504</subfield><subfield code="o">BOOK</subfield><subfield code="x">Akz: 81 T</subfield><subfield code="2">A1001274783</subfield><subfield code="c">MAG</subfield><subfield code="1">2345498200005504</subfield><subfield code="p">01</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="b">A100</subfield></datafield></marc:record></metadata></record>"""
        val sR = """<marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"><leader>00000nam a2200000uc 4500</leader><controlfield tag="001">99537220105504</controlfield><controlfield tag="005">20200119195152.0</controlfield><controlfield tag="008">840509s1979    gw       m    00| | ger d</controlfield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(swissbib)083003428-41slsp_network</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">083003428</subfield><subfield code="9">ExL</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(IDSBB)000053722DSV01</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(EXLNZ-41SLSP_NETWORK)991029235339705501</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">SzZuIDS BS/BE</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield><subfield code="d">CH-ZuSLS</subfield></datafield><datafield tag="072" ind1=" " ind2="7"><subfield code="a">mg</subfield><subfield code="2">SzZuIDS BS/BE</subfield></datafield><datafield tag="082" ind1="1" ind2="4"><subfield code="a">610</subfield><subfield code="2">15</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Hentschel, Ralph-Christian</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Franz Schönenberger (1865-1933)</subfield><subfield code="b">Biobibliographie eines ärztlichen Vertreters der Naturheilkunde</subfield><subfield code="c">von Ralph-Christian Hentschel</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">München</subfield><subfield code="c">1979</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">186 S., Portr.</subfield><subfield code="c">22 cm</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="b">txt</subfield><subfield code="2">rdacontent</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="b">n</subfield><subfield code="2">rdamedia</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="b">nc</subfield><subfield code="2">rdacarrier</subfield></datafield><datafield tag="502" ind1=" " ind2=" "><subfield code="a">Diss. med. München</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Naturheilkunde / Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="600" ind1="1" ind2="7"><subfield code="a">Schönenberger, Franz</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="650" ind1=" " ind2="7"><subfield code="a">Medizin</subfield><subfield code="z">Deutschland</subfield><subfield code="y">Geschichte 19. Jh</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="655" ind1=" " ind2="7"><subfield code="a">Hochschulschrift</subfield><subfield code="2">gnd-content</subfield></datafield><datafield tag="852" ind1="4" ind2=" "><subfield code="b">A100</subfield><subfield code="c">MAG</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="8">2245498210005504</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="3">2245498210005504</subfield><subfield code="o">BOOK</subfield><subfield code="x">Akz: 81 T</subfield><subfield code="2">A1001274783</subfield><subfield code="c">MAG</subfield><subfield code="1">2345498200005504</subfield><subfield code="p">01</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="b">A100</subfield></datafield></marc:record>"""
        var pureXml = new TemplateTransformer(sR).transform(removeNs)
        //println(pureXml)

        val e = parseRecord(pureXml)
        //println(elem)

        //val l = usePostTransformations(e).asInstanceOf[Node]
        //makes it probably slow(er) but nice output
        val pp = new scala.xml.PrettyPrinter(0, 0, true)
        val sBB = new mutable.StringBuilder
        pp.format(e, sBB)

        //print(sBB)
        //https://stackoverflow.com/questions/11283670/remove-xsitype-using-xslt
        val neu = """<record.*?>""".r.replaceAllIn(sBB,"<record>")
        //print(neu)
        pureXml = """<marc:record xmlns:marc="http://www.loc.gov/MARC21/slim"><leader>00000nam a2200000uc 4500</leader><controlfield tag="001">99537220105504</controlfield><controlfield tag="005">20200119195152.0</controlfield><controlfield tag="008">840509s1979    gw       m    00| | ger d</controlfield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(swissbib)083003428-41slsp_network</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">083003428</subfield><subfield code="9">ExL</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(IDSBB)000053722DSV01</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(EXLNZ-41SLSP_NETWORK)991029235339705501</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">SzZuIDS BS/BE</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield><subfield code="d">CH-ZuSLS</subfield></datafield><datafield tag="072" ind1=" " ind2="7"><subfield code="a">mg</subfield><subfield code="2">SzZuIDS BS/BE</subfield></datafield><datafield tag="082" ind1="1" ind2="4"><subfield code="a">610</subfield><subfield code="2">15</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Hentschel, Ralph-Christian</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Franz Schönenberger (1865-1933)</subfield><subfield code="b">Biobibliographie eines ärztlichen Vertreters der Naturheilkunde</subfield><subfield code="c">von Ralph-Christian Hentschel</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">München</subfield><subfield code="c">1979</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">186 S., Portr.</subfield><subfield code="c">22 cm</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="b">txt</subfield><subfield code="2">rdacontent</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="b">n</subfield><subfield code="2">rdamedia</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="b">nc</subfield><subfield code="2">rdacarrier</subfield></datafield><datafield tag="502" ind1=" " ind2=" "><subfield code="a">Diss. med. München</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Naturheilkunde / Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="600" ind1="1" ind2="7"><subfield code="a">Schönenberger, Franz</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="650" ind1=" " ind2="7"><subfield code="a">Medizin</subfield><subfield code="z">Deutschland</subfield><subfield code="y">Geschichte 19. Jh</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="655" ind1=" " ind2="7"><subfield code="a">Hochschulschrift</subfield><subfield code="2">gnd-content</subfield></datafield><datafield tag="852" ind1="4" ind2=" "><subfield code="b">A100</subfield><subfield code="c">MAG</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="8">2245498210005504</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="3">2245498210005504</subfield><subfield code="o">BOOK</subfield><subfield code="x">Akz: 81 T</subfield><subfield code="2">A1001274783</subfield><subfield code="c">MAG</subfield><subfield code="1">2345498200005504</subfield><subfield code="p">01</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="b">A100</subfield></datafield></marc:record>"""
        //val elem = parseRecord(line)
        val holdings = new TemplateTransformer(pureXml).transform(startTransformer)

        val dataObject = new XSLTDataObject
        dataObject.additions.put("holdingsStructure", holdings)
        dataObject.record = pureXml

        val weedHoldingsTransformation = new TemplateTransformer(dataObject.record).transform(weedTransformer)
        dataObject.record = weedHoldingsTransformation

        val step1Transformation = new TemplateTransformer(dataObject.record).transform(step1Transformer)
        dataObject.record = step1Transformation
        val specialGreenTransformation = new TemplateTransformer(dataObject.record).transform(specialGreenTransformer)

        dataObject.record = specialGreenTransformation

        step2Transformer.setParameter("holdingsStructure", dataObject.additions.get("holdingsStructure"))
        val step2Transformation = new TemplateTransformer(dataObject.record).transform(step2Transformer)

        //println(step2Transformation)

        //https://stackoverflow.com/questions/13976294/variable-in-cdata-in-scala
        val elem = parseRecord(step2Transformation)
        //println(elem)

        val last = usePostTransformations(elem).asInstanceOf[Node]
        //makes it probably slow(er) but nice output
        val p = new scala.xml.PrettyPrinter(0, 0, true)
        val sB = new mutable.StringBuilder
        p.format(last, sB)

        print(sB)
        //zahler += 1
        //print(sB.toString())
        //examples xml literals: https://alvinalexander.com/scala/how-to-create-scala-xml-literals/

      }

    }

  }

}
