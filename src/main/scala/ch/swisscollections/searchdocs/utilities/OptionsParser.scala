/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.utilities

object OptionsParser {

  type OptionMap = Map[Symbol, Any]

  def nextOption(map : OptionMap, list: List[String]) : OptionMap = {
    def isSwitch(s : String) = (s(0) == '-')
    list match {
      case Nil => map
      case "--indir" :: value :: tail =>
        nextOption(map ++ Map(Symbol("indir") -> value), tail)
      case "--outdir" :: value :: tail =>
        nextOption(map ++ Map(Symbol("outdir") -> value), tail)
      case "--outFileSlaves" :: value :: tail =>
        nextOption(map ++ Map(Symbol("outFileSlaves") -> value), tail)
      case "--inFileSlaveIds" :: value :: tail =>
        nextOption(map ++ Map(Symbol("inFileSlaveIds") -> value), tail)
      //case string :: opt2 :: tail if isSwitch(opt2) =>
      //  nextOption(map ++ Map('infile -> string), list.tail)
      //case string :: Nil =>  nextOption(map ++ Map('infile -> string), list.tail)
      case option :: tail => println("Unknown option "+option); nextOption(map , list.tail)
    }
  }


}
