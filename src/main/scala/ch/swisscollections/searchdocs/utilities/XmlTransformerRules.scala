/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.utilities

import ch.swisscollections.searchdocs.extensions.{CreateLibraryFacets,
  CreateSecondISBN, FulltextContentEnrichment,
  GNDContentEnrichment, SolrStringTypePreprocessor}

import scala.collection.Seq
import scala.xml.{Node, PCData, Unparsed}

object FilterDuplicates extends Function1[Seq[Node], Seq[Node]] {

  override def apply(v1: Seq[Node]): Seq[Node] = {

    //it's ugly - how to improve this?
    //val dup = for {

    (for {
      elem <- (v1 \\ "field")
      //setItem: String <- elem.text.split("##xx##").toSet if elem.text.split("##xx##").toSet.nonEmpty
      setItem: String <- elem.text.split("##xx##").toSet if elem.text.split("##xx##").toSet.nonEmpty
      //todo: make it exception secure with Try
    //} yield <field name={elem.attribute("name").get.text}>{setItem.trim}</field>
    } yield (elem.attribute("name").get.text,setItem.trim)).
      filter(t => t._2.nonEmpty).flatMap( item => <field name={item._1}>{item._2}</field> ++ Unparsed("\n")  )

    //dup.filter(t => t._2.nonEmpty).map( item => <field name={item._1}>{item._2}</field> )

  }
}

object EnrichFulltext extends Function1[String, Seq[Node]] {

  private lazy val plugstatus = AppProperties.pluginStatus(classOf[FulltextContentEnrichment].
    getCanonicalName)
  override def apply(v1: String): Seq[Node] = {

    plugstatus match {
      case _ : PluginActive =>
        val enricher = new FulltextContentEnrichment()
        val content = enricher.readURLContent(v1)
        if (content.nonEmpty)
          <field name="fulltext">{PCData({content})}</field>
        else
          Nil
      case _: PluginNotConfigured => Nil

      case _: PluginProxy =>
        <field name="fulltext">{PCData({s"ProxyValue: $v1"})}</field>
    }
  }
}

object CreateIsbn extends Function1[String, Seq[Node]] {

  private lazy val plugstatus = AppProperties.pluginStatus(classOf[CreateSecondISBN].
    getCanonicalName)

  override def apply(v1: String): Seq[Node] = {

    plugstatus match {
      case _: PluginNotConfigured => Nil
        //only in case of not configured return null, otherwise call plugin
      case _  =>
        val altIsbn = new CreateSecondISBN().getAlternativeISBN(v1)
        if (altIsbn.nonEmpty)
          <field name="variant_isbn_isn_mv">{altIsbn}</field>
        else
          Nil
    }

  }
}

object PadSequencesWithZeros extends Function1[String, String] {
  override def apply(text: String): String = {

    val numPattern = "[0-9]+".r
    val result = numPattern.replaceAllIn(text, m => pad(m.toString()))
    result
  }
  def pad(text:String): String = {
    "%05d".format(text.toInt)
  }
}

object CreateDSV11 extends Function1[String, Seq[Node]] {

  override def apply(v1: String): Seq[Node] = {

    //I think we do not need this anymore
      //<field name="author_additional_dsv11_txt_mv">{v1}</field>
    Nil
  }
}


object GndReferenceConcatenated extends Function2[String,String, Seq[Node]] {

  private lazy val plugstatus = AppProperties.pluginStatus(classOf[GNDContentEnrichment].
    getCanonicalName)

  /**
   *
   * @param v1 fieldname
   * @param v2 fieldvalue - obviously GND number for which we are using additional values to index
   * @return Seq[Node] - might be Nil (empty List)
   */
  override def apply(v1: String, v2: String): Seq[Node] = {



    plugstatus match {
      case _ : PluginActive =>
        if (v2.nonEmpty) {
          for {
            gndValue: String <- new GNDContentEnrichment().
              getReferencesConcatenated(v2).split("##xx##").
              toSet.toSeq if gndValue.nonEmpty
          }
            yield {
              <field name={v1}>{gndValue}</field>
            }

        } else
          Nil

      case _: PluginNotConfigured => Nil

      case _: PluginProxy =>
        <field name={v1}>{s"ProxyValue: $v2"}</field>

    }

  }
}



object GndReferenceConcatenatedTitles extends Function2[String,String, Seq[Node]] {

  private lazy val plugstatus = AppProperties.pluginStatus(classOf[GNDContentEnrichment].
    getCanonicalName)

  /**
   *
   * @param v1 fieldname
   * @param v2 fieldvalue - obviously GND number for which we are using additional values to index
   * @return Seq[Node] - might be Nil (empty List)
   */
  override def apply(v1: String, v2: String): Seq[Node] = {



    plugstatus match {
      case _ : PluginActive =>
        if (v2.nonEmpty) {
          for {
            gndValue: String <- new GNDContentEnrichment().
              getReferencesConcatenatedTitles(v2).split("##xx##").
              toSet.toSeq if gndValue.nonEmpty
          }
          yield {
            <field name={v1}>{gndValue}</field>
          }

        } else
          Nil

      case _: PluginNotConfigured => Nil

      case _: PluginProxy =>
        <field name={v1}>{s"ProxyValue: $v2"}</field>

    }

  }
}



object GndReferences5xxConcatenated extends Function2[String,String, Seq[Node]] {

  private lazy val plugstatus = AppProperties.pluginStatus(classOf[GNDContentEnrichment].
    getCanonicalName)


  /**
   *
   * @param v1 fieldname
   * @param v2 fieldvalue - obviously GND number for which we are using additional values to index
   * @return Seq[Node] - might be Nil (empty List)
   */
  override def apply(v1: String, v2: String): Seq[Node] = {

    plugstatus match {
      case _ : PluginActive =>
        if (v2.nonEmpty) {
          for {
            gndValue: String <- new GNDContentEnrichment().
              getReferences5xxConcatenated(v2).split("##xx##").
              toSet.toSeq if gndValue.nonEmpty
          }
            yield {
              <field name={v1}>{gndValue}</field>
            }

        } else
          Nil
      case _: PluginNotConfigured => Nil

      case _: PluginProxy =>
        <field name={v1}>{s"ProxyValue: $v2"}</field>
    }


  }
}

object CreateNavFieldCombinedSingleValue extends Function2[String, String, Seq[Node]] {

  private lazy val plugstatus = AppProperties.pluginStatus(classOf[SolrStringTypePreprocessor].
    getCanonicalName)

  /**
   @param v1: fieldname
   @param v2 fieldvalue
   */
  override def apply(v1: String, v2: String): Seq[Node] = {

    plugstatus match {
      case _ : PluginActive =>
        //todo: make SolrStringTypePreprocessor an object (no instantiation necessary - I guess...)
        val analyzedValue = new SolrStringTypePreprocessor().getNavFieldCombined(v2)
        if (v1.nonEmpty && analyzedValue.nonEmpty )
          <field name={v1}>{analyzedValue}</field>
        else
          Nil
      case _: PluginNotConfigured => Nil

      case _: PluginProxy =>
        <field name={v1}>{s"ProxyValue: $v2"}</field>
    }

  }
}


object CreateNavFieldCombined extends Function2[String,String, Seq[Node]] {

  //private val className = classOf[SolrStringTypePreprocessor].getCanonicalName
  private lazy val plugstatus = AppProperties.pluginStatus(classOf[SolrStringTypePreprocessor].
    getCanonicalName)

  /**
   *
   * @param v1 fieldnames - could be multiple fields
   * @param v2 fieldvalues - have to be deduplicated
   * @return Seq[Node] - might be Nil (empty List)
   */
  override def apply(v1: String, v2: String): Seq[Node] = {


    plugstatus match {
      case _ : PluginActive =>
        val analyzedValues = for {
          fieldNameValue: String <- v1.split("##xx##").toSet if fieldNameValue.nonEmpty
          fieldValue: String <- v2.split("##xx##").toSet if fieldValue.nonEmpty

        } yield {
          val analyzed = new SolrStringTypePreprocessor().getNavFieldCombined(fieldValue)
          <field name={fieldNameValue}>{analyzed}</field>
        }
        //todo: test the output with empty fields
        analyzedValues.toSeq

      case _: PluginNotConfigured => Nil

      case _: PluginProxy =>
        val proxyFieldsToBeAnalyzed = for {
          fieldNameValue: String <- v1.split("##xx##").toSet if fieldNameValue.nonEmpty
          fieldValue: String <- v2.split("##xx##").toSet if fieldValue.nonEmpty

        } yield {
          <field name={fieldNameValue}>{s"ProxyValue: $fieldValue"}</field>
        }
        proxyFieldsToBeAnalyzed.toSeq
    }

  }
}



/*
brauche ich das überhaupt???
object CreateNavMultipleFieldsCombined extends Function1[String, Seq[Node]] {

  override def apply(v1: String): Seq[Node] = {

    <field name="CreateNavMultipleFieldsCombined">
      ${v1}
      hier muss noch was gemacht werden, evtl können auch bisher getrennte Sachen zusammengefügt werden
    </field>
  }
}


 */

object CreateNavFieldForm extends Function2[String,String, Seq[Node]] {

  //private val className = classOf[SolrStringTypePreprocessor].getCanonicalName
  private lazy val plugstatus = AppProperties.pluginStatus(classOf[SolrStringTypePreprocessor].
    getCanonicalName)


  /**
   *
   * @param v1 fieldname - only one field for this case
   * @param v2 fieldvalues - have to be deduplicated
   * @return Seq[Node] - might be Nil (empty List)
   */
  override def apply(v1: String, v2: String): Seq[Node] = {


    plugstatus match {
      case _: PluginActive =>

        val analyzedValues = for {
          fieldValue: String <- v2.split("##xx##").toSet if fieldValue.nonEmpty
        } yield {
          val analyzed = new SolrStringTypePreprocessor().getNavFieldForm(fieldValue)
          <field name={v1}>{analyzed}</field>
        }
        analyzedValues.toSeq


      case _: PluginNotConfigured => Nil

      case _: PluginProxy =>

        val proxyFieldsToBeAnalyzed = for {
          fieldNameValue: String <- v1.split("##xx##").toSet if fieldNameValue.nonEmpty
          fieldValue: String <- v2.split("##xx##").toSet if fieldValue.nonEmpty

        } yield {
          <field name={fieldNameValue}>{s"ProxyValue: $fieldValue"}</field>
        }
        proxyFieldsToBeAnalyzed.toSeq
    }

  }
}




object LibraryFacets extends Function1[String, Seq[Node]] {

  private lazy val plugstatus = AppProperties.pluginStatus(classOf[CreateLibraryFacets].
    getCanonicalName)

  override def apply(v1: String): Seq[Node] = {

    //denke daran: hier muss auch noch eine Deduplizierung stattfinden
    //Lionel: not needed for HAN - so make it an empty list
    /*
    <field name="libraryfacets">
      ${v1}
      hier muss noch was gemacht werden
    </field>

     */
    Nil


  }
}

