/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.utilities

import ch.swisscollections.searchdocs.helper.{TemplateCreator, TemplateTransformer}
import javax.xml.transform.Transformer

import scala.collection.mutable
import scala.io.Source
import scala.xml.parsing.ConstructingParser
import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node, NodeSeq, PCData, Text, Unparsed, XML}


object XmlHelpers {

  def parseRecord(line: String): Elem = {
    val cp = ConstructingParser.fromSource(Source.fromString(line),preserveWS=false)
    cp.document().docElem.asInstanceOf[Elem]
  }

  def usePostTransformations(structure: Elem): collection.Seq[Node] = {

    val composedRules: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case <prepareddedup>{childfield @ _*}</prepareddedup> =>
          FilterDuplicates(childfield)
        case elem: Elem if elem.label == "preparedfulltext" => EnrichFulltext(elem.text)
        case <preparedisbnalternative>{Text(text)}</preparedisbnalternative> =>
          CreateIsbn(text)
        case elem: Elem if elem.label == "field" &&
          elem.attribute("name").head.text == "hierarchy_sequence" => <field name="hierarchy_sequence">{PadSequencesWithZeros(elem.text)}</field>
        case <prepareddsv11nolongerneeded>{Text(text)}</prepareddsv11nolongerneeded> =>
          CreateDSV11(text)
        //case <gndreferencesconcatenated>{Text(text)}</gndreferencesconcatenated> =>
        //  GndReferenceConcatenated(text)

        case gndreferencesconcatenatedTag @ <gndreferencesconcatenated/> =>
          val fieldname = (gndreferencesconcatenatedTag \ "@fieldName").text
          val fieldValue = (gndreferencesconcatenatedTag \ "@fieldValue").text
          GndReferenceConcatenated(v1 = fieldname, v2 = fieldValue)

        case gndreferencesconcatenatedtitleTag @ <gndreferencesconcatenatedtitle/> =>
          val fieldname = (gndreferencesconcatenatedtitleTag \ "@fieldName").text
          val fieldValue = (gndreferencesconcatenatedtitleTag \ "@fieldValue").text
          GndReferenceConcatenatedTitles(v1 = fieldname, v2 = fieldValue)

        case gndreferences5xxconcatenatedTag @ <gndreferences5xxconcatenated/> =>
          val fieldname = (gndreferences5xxconcatenatedTag \ "@fieldName").text
          val fieldValue = (gndreferences5xxconcatenatedTag \ "@fieldValue").text
          GndReferences5xxConcatenated(v1 = fieldname, v2 = fieldValue)

        case navFieldCombinedSingleValueTag @ <createNavFieldCombinedSingleValue/> =>
          val fieldname = navFieldCombinedSingleValueTag \ "@fieldname"
          val fieldValue = navFieldCombinedSingleValueTag \ "@fieldValue"
          CreateNavFieldCombinedSingleValue(v1 = fieldname.text, v2 = fieldValue.text)

        case navFieldCombinedTag @ <createNavFieldCombined/> =>
          val fieldname = navFieldCombinedTag \ "@fieldName"
          val fieldValue = navFieldCombinedTag \ "@fieldValue"
          CreateNavFieldCombined(v1 = fieldname.text, v2 = fieldValue.text)


        case createNavMultipleFieldsCombinedTag @ <createNavMultipleFieldsCombined/> =>
          val fieldnames = (createNavMultipleFieldsCombinedTag \ "@fieldNames").text
          val fieldValues = (createNavMultipleFieldsCombinedTag \ "@fieldValues").text
          CreateNavFieldCombined(v1 = fieldnames, v2 = fieldValues)


        case createNavFieldFormTag @ <createNavFieldForm/> =>
          val fieldname = (createNavFieldFormTag \ "@fieldName").text
          val fieldValues = (createNavFieldFormTag \ "@fieldValues").text
          CreateNavFieldForm(v1 = fieldname, v2 = fieldValues)


        case <libraryfacets>{Text(text)}</libraryfacets> =>
          LibraryFacets(text)
          //why do we get an empty tag?
          //consistent?
        case <libraryfacets/> =>
          Nil


        case item => item
      }
    }
    new RuleTransformer(composedRules).transform(structure)

  }

  lazy val removeNsTransformer: Transformer = {
    new TemplateCreator(AppProperties.templates.transfomerImpl,
      AppProperties.templates.removeAllNs).createTransformerFromResource
  }

  lazy val holdingsTransformer: Transformer = {
    new TemplateCreator(AppProperties.templates.transfomerImpl,
      AppProperties.templates.holdingsStructure).createTransformerFromResource
  }

  lazy val weedTransformer: Transformer = {
    new TemplateCreator(AppProperties.templates.transfomerImpl,
      AppProperties.templates.weedholdings).createTransformerFromResource
  }

  lazy val step1Transformer: Transformer = {
    new TemplateCreator(AppProperties.templates.transfomerImpl,
      AppProperties.templates.step1).createTransformerFromResource
  }

  lazy val specialGreenTransformer: Transformer = {
    new TemplateCreator(AppProperties.templates.transfomerImpl,
      AppProperties.templates.specialGreen).createTransformerFromResource
  }

  lazy val makeOneLiner: Transformer = {
    new TemplateCreator(AppProperties.templates.transfomerImpl,
      "oneline.xsl").createTransformerFromResource
  }


  lazy val step2Transformer: Transformer = {
    new TemplateCreator(AppProperties.templates.transfomerImpl,
      AppProperties.templates.step2).createTransformerFromResource
  }

  def transformNS(xml: String): String =
    new TemplateTransformer(xml).transform(removeNsTransformer)

  def transformCollectAllHoldings(xml: String): String =
    new TemplateTransformer(xml).transform(holdingsTransformer)

  def transformWeedHoldings(xml: String): String =
    new TemplateTransformer(xml).transform(weedTransformer)

  def transformSpecialGreen(xml: String): String =
    new TemplateTransformer(xml).transform(specialGreenTransformer)

  def transformStep1(xml: String): String =
    new TemplateTransformer(xml).transform(step1Transformer)

  def transformStep2(xml: String) (holdings: String): String = {
    step2Transformer.setParameter("holdingsStructure",holdings)
    new TemplateTransformer(xml).transform(step2Transformer)
  }

  def useOnlineStripSpace(xml: String): String = {
    new TemplateTransformer(xml).transform(makeOneLiner)
  }


  /*
  def makeXmlOneLiner(xml: Node): String = {
    val p = new scala.xml.PrettyPrinter(0, 0, true)
    val sB = new mutable.StringBuilder
    p.format(xml, sB)
    sB.toString()
  }

   */

}
