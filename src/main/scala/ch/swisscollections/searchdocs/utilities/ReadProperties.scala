/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.utilities

import java.util.Properties
import java.util.{HashMap => JHashMap}

import ch.swisscollections.searchdocs.helper.TemplateCreator
import org.yaml.snakeyaml.Yaml


object ReadProperties {

  lazy val appProperties: JHashMap[String, String] = {
    val appProps = new Properties()
    appProps.load(this.getClass.getResourceAsStream("/config/config.properties"))
    val config = new JHashMap[String,String]()
    appProps.forEach((key:Any,value: Any) =>
      config.put(key.toString.toUpperCase,value.toString)
    )
    config
  }

  //see also: https://stackoverflow.com/questions/30851141/scala-parse-a-yaml-file-using-snakeyaml
  lazy val yamlAppProperties = {
    val yaml = new Yaml()
    //val obj = yaml.load(this.getClass.getResourceAsStream("/config/pipeDefaultConfig.yaml")).
    val obj = yaml.load(this.getClass.getResourceAsStream("/config/config.yml")).
      asInstanceOf[java.util.Map[String, Any]]

    obj
  }

  //println(yamlAppProperties.get("xsltTemplates").asInstanceOf[java.util.Map[String, Any]].get("simpleValues"))
  println(yamlAppProperties)



}
