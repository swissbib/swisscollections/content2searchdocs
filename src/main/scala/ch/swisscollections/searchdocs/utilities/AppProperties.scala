/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.utilities

import java.util.{ArrayList => JArrayList, HashMap => JHashMap, Map => JMap}
import org.yaml.snakeyaml.Yaml
import ch.swisscollections.searchdocs.extensions.IDocProcPlugin
import scala.jdk.CollectionConverters._

trait Initializations {

  def pluginStatus(className: String): PluginStatus = {
    if (!plugins.use.contains(className)) {
      new PluginNotConfigured
    }
    else if (plugins.status.containsKey(className)) {
      plugins.status.get(className) match {
        case "proxy" => new PluginProxy
        case _ => new PluginActive
      }
    } else {
      new PluginActive
    }

  }


  protected lazy val config: JMap[String, Any] = {
    val yaml = new Yaml()
    yaml.load(this.getClass.getResourceAsStream("/config/config.yml")).
      asInstanceOf[JMap[String, Any]]
  }

  lazy val templates: XsltTemplates = {

    val struc = config
    val xslt = struc.get("xslt").asInstanceOf[JMap[String,Any]]
    val templates = xslt.get("templates").asInstanceOf[JMap[String,Any]]
    val transformer = xslt.get("transformerImpl").asInstanceOf[String]


    XsltTemplates(
      templates.get("holdingsStructure").toString,
      templates.get("weedholdings").toString,
      templates.get("step1").toString,
      templates.get("specialGreen").toString,
      templates.get("step2").toString,
      templates.get("removeallnamespaces").toString,
      transformer)
  }

  //todo: make it or "scala like"
  // read conversions between scala / java for 2.13
  // make the whole string stronger typed using means of the java yaml library
  lazy val plugins: Plugins = {
    val struct = config
    val plugconfs = struct.get("plugins").asInstanceOf[JMap[String,Any]]    //.asScala
    val usedPlugins = plugconfs.get("use").asInstanceOf[JArrayList[String]].asScala.toSeq
    val statusList = plugconfs.get("status").asInstanceOf[JArrayList[JHashMap[String,String]]] //.asScala
    val stati =  new JHashMap[String, String]()

    Option(statusList).foreach(list =>
      list.forEach(elem => {
        val optional = elem.keySet.stream.findFirst
        if (optional.isPresent) {
          stati.put(optional.get(), elem.get(optional.get()))
        }
      }
      ))

    //val fulltextConf = plugconfs.get("FulltextContentEnrichment").asInstanceOf[JHashMap[String, String]].asScala.toMap
    //val gndEnrichConf = plugconfs.get("GNDContentEnrichment").asInstanceOf[JHashMap[String, String]].asScala.toMap
    //val stringTypePreprocessor = plugconfs.get("SolrStringTypePreprocessor").asInstanceOf[JHashMap[String, String]].asScala.toMap

    val fulltextConf = plugconfs.get("FulltextContentEnrichment").asInstanceOf[JHashMap[String, String]]
    val gndEnrichConf = plugconfs.get("GNDContentEnrichment").asInstanceOf[JHashMap[String, String]]
    val stringTypePreprocessor = plugconfs.get("SolrStringTypePreprocessor").asInstanceOf[JHashMap[String, String]]


    //val tocuser = Option(AppProperties.TOC_USER)
    //val tocpasswd = Option(AppProperties.TOC_PASSWD)


    //if (tocuser.isDefined && tocpasswd.isDefined) {
    //  fulltextConf.put("USER", tocuser.get)
    //  fulltextConf.put("PASSWD", tocpasswd.get)
    //}

    //Plugins(usedPlugins, stati.asScala.toMap,fulltextConf,gndEnrichConf,stringTypePreprocessor)
    Plugins(usedPlugins, stati,fulltextConf,gndEnrichConf,stringTypePreprocessor)

  }


  def initTransformation(): Unit = {

    plugins.use.asJava.forEach { ext =>
      val zClass = Class.forName(ext)
      //todo: how to make this appropriate in the scala environment?
      val instance = zClass.getDeclaredConstructor().newInstance().asInstanceOf[IDocProcPlugin];
      //val instance = zClass.newInstance().asInstanceOf[IDocProcPlugin]
      //logger.info(s"initialising $zClass")
      instance.initPluginAppConfig()
    }

  }


}


/*
the initial idea:
to extend from abstract trait to give another singleton (Test) type
to overwrite values like MongoWrapperTest for test purposes
but this was too much work for the short time during my holidays until
end of November 2021
 */
object AppProperties extends Initializations {

  //todo: very ugly ... but necessary to finish quickly
  //var MONGO_USER: String = _
  //var MONGO_PASSWD: String = _
  //var TOC_USER: String = _
  //var TOC_PASSWD: String = _



}

case class XsltTemplates(holdingsStructure:String,
                        weedholdings: String,
                        step1: String,
                        specialGreen:String,
                        step2:String,
                        removeAllNs:String,
                        transfomerImpl: String)
/*
case class PluginsScala(use: Seq[String],
                   status: Map[String, String],
                  fulltextConf: Map[String, String],
                  gndConf: Map[String, String],
                  docValueProc: Map[String, String])


 */
case class Plugins(use: Seq[String],
                        status: JHashMap[String, String],
                        fulltextConf: JHashMap[String, String],
                        gndConf: JHashMap[String, String],
                        docValueProc: JHashMap[String, String])

sealed abstract class  PluginStatus

class PluginActive extends PluginStatus

class PluginNotConfigured extends PluginStatus

class PluginProxy extends PluginStatus

