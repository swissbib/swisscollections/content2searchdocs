/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.extensions;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.swisscollections.searchdocs.utilities.AppProperties;
import ch.swisscollections.searchdocs.utilities.PluginActive;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.regex.Pattern;

public class FulltextContentEnrichment implements IDocProcPlugin{

    // via another container in the same pod in kubernetes
    //private static final String TIKA_SERVER_URL = "http://127.0.0.1:9998/tika";

    //local via docker-compose
    private static final String TIKA_SERVER_URL = "http://127.0.0.1:9998/tika";

    private static Logger tikaexceptionLogger;
    private static Logger tikaContentLogger;
    private static Logger tikaNoContentLogger;
    private static Logger tikaContentProcessing;
    private static Logger timeDifference;

    private static Connection dbmsConnection = null;
    private static ArrayList<Pattern> patternsAllowed;
    private static ArrayList<Pattern> patternsNotAllowed;

    private static Proxy proxy;
    //private static String testPDF;
    private static ArrayList<String> allowedContentType;
    private static boolean execRemoteFetching;
    private static boolean  initialized;
    private static Pattern separator;
    private static HashMap<String, PreparedStatement> prepStats = new HashMap<String, PreparedStatement>();
    private static boolean inProductionMode = true;

    static {
        FulltextContentEnrichment.tikaContentLogger = LoggerFactory.getLogger("tikaContent");
        FulltextContentEnrichment.tikaNoContentLogger = LoggerFactory.getLogger("tikaContentNoMatch");
        FulltextContentEnrichment.tikaexceptionLogger = LoggerFactory.getLogger("tikaException");
        FulltextContentEnrichment.tikaContentProcessing = LoggerFactory.getLogger("contentProcessing");
        FulltextContentEnrichment.timeDifference = LoggerFactory.getLogger("timeDifference");

        patternsAllowed = new ArrayList<Pattern>();
        patternsNotAllowed = new ArrayList<Pattern>();

        allowedContentType = new ArrayList<String>();
        execRemoteFetching = true;
        initialized = false;
        separator = Pattern.compile(("^\\||\\|$"));
    }

    public String readURLContent(String url) {

        //initPluginAppConfig();//todo init at another place
        String content = "";
        long startTime;
        long endTime;
        long dateDiff;

        long startTimeForException = new Date().getTime();

        //if (!inProductionMode) return content;

        //if we couldn't fetch content from the server we (content length == 0) use Tika - if activated
        if (execRemoteFetching){
            boolean matched = false;
            for (Pattern p : patternsAllowed ) {
                if (p.matcher(url).find())
                {
                    matched = true;
                    //first check if this document is available in mariadb
                    if (this.dbmsConnectionActive()) {
                        content = fetchContentDBMS(url);
                        if (content.length() > 0) {
                            tikaContentLogger.info("got content from DBMS for url: " + url);
                            break;
                        }
                    }
                    //else parse the document with tika
                    if (content != null && content.length() == 0) {
                        try {
                            boolean httpFetchingAllowed = true;
                            for (Pattern p1 : patternsNotAllowed ){
                                if (p1.matcher(url).find()) {
                                    tikaContentLogger.info("URL: " + url + " not allowed");
                                    httpFetchingAllowed = false;
                                }
                            }
                            if (!httpFetchingAllowed) continue;
                            timeDifference.info("document: " + url);
                            startTime = new Date().getTime();
                            HttpURLConnection connection = getHTTPConnection(url);
                            connection.setInstanceFollowRedirects(false);
                            connection.setReadTimeout(300000);
                            endTime = new Date().getTime();
                            dateDiff = endTime - startTime;

                            timeDifference.info("getConnection: " + dateDiff + " ms");

                            int status = connection.getResponseCode();
                            // we need to check if the document is e.g. permanently moved
                            //https://github.com/swissbib/searchconf/issues/20
                            if (status != HttpURLConnection.HTTP_OK) {
                                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                                        || status == HttpURLConnection.HTTP_MOVED_PERM
                                        || status == HttpURLConnection.HTTP_SEE_OTHER) {
                                    // get redirect url from "location" header field
                                    String newUrl = connection.getHeaderField("Location");


                                    URL originalUrl = connection.getURL(); // get the original URL

                                    // Make sure the new URL is absolute
                                    if (!newUrl.startsWith("http")) {
                                        newUrl = new URL(originalUrl, newUrl).toString(); // construct absolute URL from relative URL
                                    }


                                    connection.disconnect();
                                    // open the new connnection again
                                    connection = getHTTPConnection(newUrl);
                                }
                            }
                            boolean fetch = true;
                            if (isContentTypeRestricted()) {
                                fetch = false;
                                String type = connection.getContentType();
                                for (String tType: allowedContentType) {
                                    if (type.equals(tType)) {
                                        fetch = true;
                                        break;
                                    }
                                }

                                if (!fetch && null != tikaContentLogger){
                                    tikaContentLogger.info("content type not allowed: " + type);
                                }
                            }

                            if (fetch) {
                                InputStream contentStream = (InputStream) connection.getContent();
                                startTime = new Date().getTime();
                                content = sendPdfToTika(contentStream, connection.getContentType());
                                content = content.replaceAll("\n"," ");
                                endTime = new Date().getTime();
                                dateDiff = endTime - startTime;
                                timeDifference.info("harvest file and parse with Tika: " + dateDiff + " ms");
                                if (null != tikaContentLogger) {
                                    tikaContentLogger.info("url fetched: " + url);
                                    if (content == null) {
                                        tikaContentLogger.info("content: \n" + "variable wasn't initialized - reason not clear");
                                    } else if(content.length() > 100) {
                                        tikaContentLogger.debug("content: \n" + content);
                                    } else {
                                        tikaContentLogger.info("content: content length less than 100 - won't be serialized in DB");
                                    }
                                }
                                if (null != dbmsConnection && content != null && content.length() > 100) {
                                    insertContentDBMS(url,content);
                                }
                                if (content == null) {
                                    content = "";
                                }
                                if (content.length() < 100 ) {
                                    content = "";
                                }
                                if (null !=  contentStream) {
                                    contentStream.close();
                                }
                            }
                            break;
                        } catch (Throwable th) {
                            long endTimeInException = new Date().getTime();
                            dateDiff = endTimeInException - startTimeForException;
                            timeDifference.info("in Throwable: " + dateDiff);

                            tikaexceptionLogger.error("Error while trying to fetch the remote doc with Tika");
                            tikaexceptionLogger.error(th.getMessage());
                            for (StackTraceElement sE: th.getStackTrace()) {
                                tikaexceptionLogger.error(sE.toString());
                            }
                            tikaexceptionLogger.error("URL which caused exception: " + url);
                        }
                    }
                }
            }
            if (!matched) {
                tikaNoContentLogger.info("no match URL: " + url + "\n");
            }
        }
        return content != null ? content : "";
    }

    /***
     *This method was made public because it is useful to fetch documents in advance from the database
     *in the "drop-usecase" related to Nebis
     * In "drop-usecase" - mode we are going to fetch all documents when the document processing is already finished
     * in batch mode. Because I want only documents which are really not in db I want to make a "pre-check"
     * This method is useful for this
     */
    public String fetchContentDBMS (String url) {
        String remoteContent = "";

        //PreparedStatement statement = prepStats.get("select");
        PreparedStatement statement = prepStats.get("selectOnlyUrl");
        try {
            //statement.setString(1,docid);
            //statement.setString(2,url);
            statement.setString(1,url);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                remoteContent = rs.getString("content");
            }
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        }
        return remoteContent;
    }

    private void insertContentDBMS (String url, String content) {
        PreparedStatement statement = prepStats.get("insert");

        java.util.Date today = new java.util.Date();
        java.sql.Date date =  new java.sql.Date(today.getTime());

        try {
            statement.setString(1,"");
            statement.setString(2,content);
            statement.setString(3,url);
            statement.setDate(4,date);
            statement.executeUpdate();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        }
    }

    private HttpURLConnection getHTTPConnection (String url) throws
            MalformedURLException, IOException
    {
        HttpURLConnection uc = null;

        URL u = new URL(url);
        if (proxy != null) {
            uc = (HttpURLConnection)u.openConnection(proxy);
            uc.setReadTimeout(2000);
            uc.connect();
        } else {
            uc = (HttpURLConnection)u.openConnection();
            uc.setReadTimeout(2000);
            uc.connect();
        }
        return uc;
    }

    public boolean isContentTypeRestricted() {
        return null != allowedContentType && allowedContentType.size() > 0;
    }

    @Override
    public void initPlugin(HashMap<String, String> configuration) {

        String className =  this.getClass().getName();
        if (configuration.containsKey("PLUGINS.IN.PRODUCTIONMODE") && configuration.get("PLUGINS.IN.PRODUCTIONMODE").contains(className) )
            inProductionMode = true;
        else
            return;
        initializeDBMS(configuration);
        initializeallowedDocuments(configuration);
        initializeNotallowedDocuments(configuration);
        initializeProxy(configuration);
        //initializeTestPDF(configuration);
        initializeAllowedContentType(configuration);
        initializeActivateRemoteFetching(configuration);
        initialized = true;
    }

    @Override
    public void finalizePlugIn() {
        if (null != dbmsConnection) {
            try {
                dbmsConnection.close();
            } catch (SQLException sqlExc) {
                tikaContentLogger.error("error while closing the DBMSconnection");
            }
        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void initPluginAppConfig() {
        String className =  this.getClass().getName();
        HashMap<String, String> fullConfig = AppProperties.plugins().fulltextConf();

        if (AppProperties.plugins().use().contains(className) &&
            AppProperties.pluginStatus(className) instanceof PluginActive)
            inProductionMode = true;
        else
            return;

        initializeDBMS(fullConfig);
        initializeallowedDocuments(fullConfig);
        initializeNotallowedDocuments(fullConfig);
        initializeProxy(fullConfig);
        initializeAllowedContentType(fullConfig);
        initializeActivateRemoteFetching(fullConfig);

        initialized = true;
    }

    private void initializeDBMS(HashMap<String, String> configuration) {
        String driver = configuration.get("JDBCDRIVER");

        String jdbcConnection = System.getenv("JDBCCONNECTION");
        String user = System.getenv("TOC_DB_USER");
        String password = System.getenv("TOC_DB_PASSWD");

        // Check if the environment variables are set
        if (jdbcConnection == null || jdbcConnection.isEmpty()) {
            throw new IllegalArgumentException("JDBCCONNECTION environment variable is not set or is empty.");
        }

        if (user == null || user.isEmpty()) {
            throw new IllegalArgumentException("TOC_DB_USER environment variable is not set or is empty.");
        }

        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("TOC_DB_PASSWD environment variable is not set or is empty.");
        }

        if (null != driver && driver.length() > 0) {
            try {
                //System.out.println(configsDBMS.getProperty("JDBCdriver"));
                Class.forName(driver).getDeclaredConstructor().newInstance();
                //Class.forName(driver).newInstance();
                tikaContentProcessing.info("appropriate driver: " + driver + " was loaded ");
                Connection conn = null;

                conn = DriverManager.getConnection(jdbcConnection, user, password);

                dbmsConnection = conn;
                prepareStatement(dbmsConnection);
            } catch (ClassNotFoundException cnfe) {
                tikaContentProcessing.error("error while class not found - no use of DBMS for cached content ",cnfe);
            } catch (InstantiationException ie) {
                tikaContentProcessing.error("error while instantiating database connection - no use of DBMS for cached content",ie);
            } catch (IllegalAccessException iae) {
                tikaContentProcessing.error("error illegalAccess - no use of DBMS for cached content",iae );
            } catch (SQLException sqlEx) {
                sqlEx.printStackTrace();
                tikaContentProcessing.error("sqlException - no use of DBMS for cached content",sqlEx );
            } catch (NoSuchMethodException | InvocationTargetException noMethod) {
                noMethod.printStackTrace();
                tikaContentProcessing.error("noMethod Exception - no use of DBMS for cached content",noMethod );
            }
        }
    }


    private static void prepareStatement (Connection dbConnection) {
        prepStats = new HashMap<String, PreparedStatement>();

        PreparedStatement tempPrepared = null;
        StringBuilder errorMessage;

        try {
            tempPrepared = dbConnection.prepareStatement( "select * from content " +
                    " where docid = ? and url = ? " );
            prepStats.put("select", tempPrepared);
            tempPrepared = dbConnection.prepareStatement( "select * from content " +
                    " where  url = ? " );

            prepStats.put("selectOnlyUrl", tempPrepared);
            tempPrepared = dbConnection.prepareStatement( "insert into content " +
                    " (docid, content, url, date) values (?,?,?,?)" );
            prepStats.put("insert", tempPrepared);
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        }
    }


    private void initializeallowedDocuments(HashMap<String, String> configuration) {
        String allowedDocs = configuration.get("ALLOWED_DOCUMENTS");
        tikaContentProcessing.info("\n => Loading ALLOWED_DOCUMENTS: " + allowedDocs);

        if (allowedDocs != null && allowedDocs.length() > 0) {
            ArrayList<String> regexs;
            regexs = new ArrayList<String>();
            regexs.addAll(Arrays.asList(allowedDocs.split("###")));
            //ArrayList<Pattern> patterns = new ArrayList<Pattern>();
            for (String regex : regexs ) {
                patternsAllowed.add(Pattern.compile(regex));
            }
            //confContainer.setDocumentPatterns(patterns);
        }
    }

    private void initializeAllowedContentType(HashMap<String, String> configuration) {
        String propallowedContentType = configuration.get("ALLOWED_CONTENTTYPE");
        tikaContentProcessing.info("\n => Loading ALLOWED_CONTENTTYPE: " + propallowedContentType);

        if (propallowedContentType != null && propallowedContentType.length()>0) {
            allowedContentType.addAll(Arrays.asList(propallowedContentType.split("###")));
        }
    }


    private void initializeActivateRemoteFetching(HashMap<String, String> configuration) {
        String sExec = configuration.get("REMOTEFETCHING");
        tikaContentProcessing.info("\n => Loading REMOTEFETCHING: " + sExec);
        execRemoteFetching = Boolean.parseBoolean(sExec);
    }



    private void initializeProxy(HashMap<String, String> configuration) {
        String proxyProp = configuration.get("PROXYSERVER");
        if (proxyProp != null && proxyProp.length()>0) {
            try {
                String proxyServer = null;
                Integer proxyPort = 0;
                if (proxyProp.contains(":")) {
                    String [] serverParts = proxyProp.split(":");
                    proxyServer = serverParts[0];
                    proxyPort = Integer.valueOf(serverParts[1]);

                    proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyServer, proxyPort));
                }
                else {
                    proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyServer, 80));
                }
                tikaContentProcessing.info("proxy - object: " + proxyProp + " initialized");
            } catch (Throwable th) {
                tikaContentProcessing.error("\n => Error while creating Proxy");
                tikaContentProcessing.error(th.getMessage());
                proxy = null;
            }
        }
    }




    private void initializeNotallowedDocuments(HashMap<String, String> configuration) {
        String notAllowedDocs = configuration.get("HTTP_FETCH_NOT_ALLOWED");
        tikaContentProcessing.info("\n => Loading HTTP_FETCH_NOT_ALLOWED: " + notAllowedDocs);
        if (notAllowedDocs != null && notAllowedDocs.length() > 0) {
            ArrayList<String> regexs;
            regexs = new ArrayList<String>();
            regexs.addAll(Arrays.asList(notAllowedDocs.split("###")));
            //ArrayList<Pattern> patterns = new ArrayList<Pattern>();
            for (String regex : regexs ) {
                patternsNotAllowed.add(Pattern.compile(regex));
            }
            //confContainer.setDocumentPatterns(patterns);
        }
    }

    public boolean dbmsConnectionActive() {
        return null != dbmsConnection;
    }

    private static String sendPdfToTika(InputStream pdfStream, String contentType) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPut uploadFile = new HttpPut(TIKA_SERVER_URL);

            // Set up the file stream and content type
            InputStreamEntity pdfEntity = new InputStreamEntity(pdfStream);
            //uploadFile.setHeader("Content-encoding", "UTF-8");
            uploadFile.setEntity(pdfEntity);
            uploadFile.setHeader("Content-type", contentType);
            uploadFile.setHeader("Accept", "text/plain");

            // Execute the request
            try (CloseableHttpResponse response = httpClient.execute(uploadFile)) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    return EntityUtils.toString(entity, StandardCharsets.UTF_8);
                } else {
                    return "";
                }
            }
        }
    }
}
