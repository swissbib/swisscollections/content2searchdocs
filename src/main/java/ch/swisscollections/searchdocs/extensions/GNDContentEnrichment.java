/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.extensions;

import ch.swisscollections.searchdocs.SettingsFromFile;
import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.swisscollections.searchdocs.utilities.AppProperties;
import ch.swisscollections.searchdocs.utilities.PluginActive;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.*;
import java.text.MessageFormat;
import java.text.Normalizer;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;




public class GNDContentEnrichment implements IDocProcPlugin{


    private static Logger gndProcessing;
    private static final String GND_SEARCHFIELD = "gndid";
    private static final String RESULT_APPEND_PATTERN = "##xx##";
    private static boolean initialized;

    //an error occured while trying to initialize -> processing shouldn't take place
    private static boolean errorInitializing;
    private static MongoCollection<Document> searchCollection = null;
    private static MongoClient mongoClient = null;
    private static boolean inProductionMode = false;
    private static final TransformerFactory transformerFactory;
    private static Transformer gndTransformerRelated = null;
    private static Transformer gndTransformerVariants = null;
    private static Transformer gndTransformerVariantsTitles = null;




    static {
        GNDContentEnrichment.gndProcessing = LoggerFactory.getLogger("gndProcessing");
        initialized = false;
        errorInitializing = false;

        System.setProperty("javax.xml.transform.TransformerFactory","net.sf.saxon.TransformerFactoryImpl");
        transformerFactory = TransformerFactory.newInstance();
        try {

            //initialize xslt Transformers
            ClassLoader classLoader = GNDContentEnrichment.class.getClassLoader();
            InputStream in = classLoader.getResourceAsStream("gnd.related.xsl");
            gndTransformerRelated = transformerFactory.newTransformer(new StreamSource(in));

            in = classLoader.getResourceAsStream("gnd.variants.xsl");
            gndTransformerVariants = transformerFactory.newTransformer(new StreamSource(in));

            in = classLoader.getResourceAsStream("gnd.variantsTitles.xsl");
            gndTransformerVariantsTitles = transformerFactory.newTransformer(new StreamSource(in));


        } catch (TransformerConfigurationException c) {
            c.printStackTrace();
        }


    }


    @Override
    public void initPlugin(HashMap<String, String> configuration) {

        //in any case if the method is called the plugin will be marked as initialized
        //an error during initialization might occur - but this is another case

        //this method shouldn't be used anymore - from times before swisscollections

    }

    @Override
    public void finalizePlugIn() {
        if (mongoClient != null) {
            mongoClient.close();
        }
    }

    @Override
    public void initPluginAppConfig() {

        String className =  this.getClass().getName();

        if (AppProperties.plugins().use().contains(className)  &&
                AppProperties.pluginStatus(className) instanceof PluginActive)
            inProductionMode = true;
        else
            return;

        HashMap<String, String> config =  AppProperties.plugins().gndConf();

        initialized = true;

        try {

            initializeMongoConnection(config);

        } catch (Exception ex) {
            errorInitializing = true;
        }

    }


    public String getReferences5xxConcatenated(String gndID) {

        String toReturn = "";
        if (!this.checkReadyForProcessing()) return toReturn;
        if (!errorInitializing) {
                toReturn =  fetchGndRecord(gndID, () -> gndTransformerRelated);
                //gndProcessing.debug("getReferencesConcatenated: gndID: " + gndID + " / references: " + toReturn);
            return toReturn;
        }
        return toReturn;
    }



    public String getReferencesConcatenated(String gndID) {

        String toReturn = "";
        if (!checkReadyForProcessing()) return toReturn;
        if (!errorInitializing) {
            toReturn =  fetchGndRecord(gndID, () -> gndTransformerVariants);
            //gndProcessing.debug("getReferencesConcatenated: gndID: " + gndID + " / references: " + toReturn);
        }
        return toReturn;

    }

    public String getReferencesConcatenatedTitles(String gndID) {

        String toReturn = "";
        if (!checkReadyForProcessing()) return toReturn;
        if (!errorInitializing) {
            toReturn =  fetchGndRecord(gndID, () -> gndTransformerVariantsTitles);
        }
        return toReturn;

    }

    private boolean checkReadyForProcessing() {

        if (!inProductionMode) return false;
        if (!initialized)  {
            initDefaultValues();
        }
        return true;
    }




    private Optional<String> getDeCompressedRecord(byte[] zippedByteStream, String gndid) {
        Optional<String> opt = Optional.empty();

        try {
            Inflater decompresser = new Inflater();
            decompresser.setInput(zippedByteStream);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(zippedByteStream.length);
            byte[] buffer = new byte[8192];
            while (!decompresser.finished()) {
                int size = decompresser.inflate(buffer);
                bos.write(buffer, 0, size);
            }
            opt = Optional.of(bos.toString());
            decompresser.end();
        } catch (DataFormatException dfe) {
            gndProcessing.error(String.format("couldn't get the zipped GND record from id: %s", gndid));
        }

        return opt;
    }


    private void initializeMongoConnection(HashMap<String, String> configuration)
    {

        //use environament variables instead of own configuration
        //mongoClient = MongoClients.create(System.getenv("MONGO_URI"));
        mongoClient = MongoClients.create(SettingsFromFile.getMongoConnection());
        //MongoDatabase db = mongoClient.getDatabase(System.getenv("MONGO_DB"));
        MongoDatabase db = mongoClient.getDatabase(SettingsFromFile.getMongoDB());
        //searchCollection = db.getCollection(System.getenv("MONGO_COLLECTION"));
        searchCollection = db.getCollection(SettingsFromFile.getMongoCollection());

    }



    private void initDefaultValues () {
        HashMap<String,String>   configuration = new HashMap<String, String>();
        initPlugin(configuration);

    }

    public String fetchGndRecord(String gndNumber, Supplier<Transformer> suppliedTransformer) {

        String toReturn = "";

        StringBuilder concatReferences = new StringBuilder();
        try {
            BasicDBObject bdbo = new BasicDBObject(GND_SEARCHFIELD, gndNumber);
            Document doc = searchCollection.find(bdbo).first();
            boolean append = false;
            if (null != doc) {

                Binary binaryRecord = doc.get("record", org.bson.types.Binary.class);
                Optional<String> unzippedRecord = this.getDeCompressedRecord(binaryRecord.getData(), gndNumber);
                if (unzippedRecord.isPresent()) {
                    //Source gndRecord = new StreamSource(new StringReader(testRecord()));
                    Source gndRecord = new StreamSource(new StringReader(unzippedRecord.get()));
                    StringWriter result = new StringWriter();
                    Result xsltGndResult = new StreamResult(result);
                    Transformer transformer = suppliedTransformer.get();
                    transformer.transform(gndRecord, xsltGndResult);
                    String extractedGNDValues = result.toString();
                    if (!extractedGNDValues.isEmpty()) {
                        for (String line : extractedGNDValues.split("\n")) {
                            if (line.isEmpty()) continue; //do not consider empty lines
                            append = true;
                            concatReferences.append(line).append(RESULT_APPEND_PATTERN);
                        }
                    }
                }
            }
            toReturn = concatReferences.toString();
            if (append) {
                toReturn = toReturn.substring(0,toReturn.length()-6);

            }

        } catch (Exception excep) {
            //todo: this does not make a lot of sense.... (GH)
            excep.printStackTrace();
        }
        gndProcessing.debug("getReferencesConcatenated: gndID: " + gndNumber + " / references: " + toReturn);
        return  toReturn;

    }


}


