/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs.helper;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


public class TemplateCreator {

    private final String templatePath;

    public TemplateCreator(String transformerFactory,
                           String templatePath) {
        this(templatePath);
        //not sure if the necessary saxon.TransformerFactoryImpl is correctly set by configuration
        //so, make it explicit
        System.setProperty("javax.xml.transform.TransformerFactory","net.sf.saxon.TransformerFactoryImpl");
    }

    public TemplateCreator(String templatePath) {
        this.templatePath = templatePath;
        //as a more general solution we do not use net.sf.saxon.TransformerFactoryImpl as default
        //System.setProperty("javax.xml.transform.TransformerFactory","net.sf.saxon.TransformerFactoryImpl");

    }


    public Transformer createTransformer() {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        StreamSource source = null;

        Transformer transformer = null;

        if (new File(templatePath).exists()) {
            source = new StreamSource(templatePath);
            try {
                transformer = transformerFactory.newTransformer(source);
            } catch (TransformerConfigurationException ex) {
                ex.printStackTrace();

            }
        }
        return transformer;
    }


    public Transformer createTransformerFromResource() {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        StreamSource source = null;

        Transformer transformer = null;
        //ClassLoader classLoader = TemplateCreator.class.getClassLoader();
        ClassLoader classLoader = TemplateCreator.class.getClassLoader();
        //try (InputStream is =  getClass()
        //        .getClassLoader().getResourceAsStream(templatePath)) {
        try (InputStream is =  classLoader.getResourceAsStream(templatePath)) {
            source = new StreamSource(is);
            transformer = transformerFactory.newTransformer(source);
        } catch (TransformerConfigurationException | IOException exc) {
            throw new RuntimeException(exc);
        }

        return transformer;
    }



}
