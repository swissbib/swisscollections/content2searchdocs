/*
 * content2searchdocs
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.searchdocs.utilities

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source
import scala.xml.{Node, XML}

class XmlHelpersTest extends AnyFunSuite {

  private def getTextFromResourcesFolder(filepath: String): String = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    text
  }

  test("Test presence of hierarchy_parent_id") {
    val xml = getTextFromResourcesFolder("record/han_record_no_namespace.xml")

    val result = XmlHelpers.transformSpecialGreen(xml)
    //val result = Marc2SearchDocTransform.apply("", singleRecord)
    //print(result)
    assert(result.contains("<hierarchy_parent_id>(HAN)000109604DSV05</hierarchy_parent_id>"))
   }
}
