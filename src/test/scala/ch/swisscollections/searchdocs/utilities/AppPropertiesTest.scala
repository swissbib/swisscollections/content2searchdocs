/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.searchdocs.utilities

import ch.swisscollections.searchdocs.extensions.IDocProcPlugin
import ch.swisscollections.searchdocs.utilities.AppProperties.plugins
import java.util.{ArrayList => JArrayList, HashMap => JHashMap, Map => JMap}
import scala.jdk.CollectionConverters._

object AppPropertiesTest extends ch.swisscollections.searchdocs.utilities.Initializations {


  override lazy val plugins: Plugins = {


    val struct = config
    val plugconfs = struct.get("plugins").asInstanceOf[JMap[String,Any]]    //.asScala
    val usedPlugins = plugconfs.get("use").asInstanceOf[JArrayList[String]].asScala.toSeq
    val statusList = plugconfs.get("status").asInstanceOf[JArrayList[JHashMap[String,String]]] //.asScala
    val stati =  new JHashMap[String, String]()

    Option(statusList).foreach(list =>
      list.forEach(elem => {
        val optional = elem.keySet.stream.findFirst
        if (optional.isPresent) {
          stati.put(optional.get(), elem.get(optional.get()))
        }
      }
      ))

    //val fulltextConf = plugconfs.get("FulltextContentEnrichment").asInstanceOf[JHashMap[String, String]].asScala.toMap
    //val gndEnrichConf = plugconfs.get("GNDContentEnrichment").asInstanceOf[JHashMap[String, String]].asScala.toMap
    //val stringTypePreprocessor = plugconfs.get("SolrStringTypePreprocessor").asInstanceOf[JHashMap[String, String]].asScala.toMap

    val fulltextConf = plugconfs.get("FulltextContentEnrichment").asInstanceOf[JHashMap[String, String]]
    val gndEnrichConf = plugconfs.get("GNDContentEnrichment").asInstanceOf[JHashMap[String, String]]
    val stringTypePreprocessor = plugconfs.get("SolrStringTypePreprocessor").asInstanceOf[JHashMap[String, String]]

    gndEnrichConf.put("MONGO_WRAPPER","ch.swisscollections.searchdocs.utilities.MongoWrapperTest")
    Plugins(usedPlugins, stati,fulltextConf,gndEnrichConf,stringTypePreprocessor)

  }



}
