
package ch.swisscollections.searchdocs.utilities

import scala.io.Source

object OpenResource {
  def apply(path: String): String = {
    Source.fromInputStream(getClass.getResourceAsStream(path)).getLines().mkString
  }
}
