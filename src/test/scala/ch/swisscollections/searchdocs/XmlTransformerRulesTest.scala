/*
 * content2searchdocs
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs


import ch.swisscollections.searchdocs.utilities.{PadSequencesWithZeros, XmlHelpers}
import org.scalatest.funsuite.AnyFunSuite

class XmlTransformerRulesTest extends AnyFunSuite {

  test("Pad with 0") {
    assert (PadSequencesWithZeros("1") == "00001")
    assert (PadSequencesWithZeros("22") == "00022")
    assert (PadSequencesWithZeros("Letter 22") == "Letter 00022")
    assert (PadSequencesWithZeros("Letter 22 of Band 3245") == "Letter 00022 of Band 03245")
    assert (PadSequencesWithZeros("Brief 01") == "Brief 00001")
    assert (PadSequencesWithZeros("Brief 10.25") == "Brief 00010.00025")
    assert (PadSequencesWithZeros("123Brief 100000") == "00123Brief 100000")
  }

}
