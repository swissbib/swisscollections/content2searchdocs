/*
 * content2searchdocs
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.searchdocs

import ch.swisscollections.searchdocs.extensions.GNDContentEnrichment
import ch.swisscollections.searchdocs.utilities.{AppProperties, AppPropertiesTest}
import org.scalatest.funsuite.AnyFunSuite

import scala.sys.env

class MongoAccessTest extends AnyFunSuite {

  ignore ("test gnd access directly on existing mongo db - therefor atomatic test is ignored") {

    //use this in case you are able to access a mongo db e.g. via tunnel
    //types like MongoWrappers are too complicated for a quicker solution

    //val passwd = SettingsFromFile.getTocDBPassword
    //val user = SettingsFromFile.getTocDBUser

    AppProperties.initTransformation()

    val gndExtension = new GNDContentEnrichment
    //gndExtension.initPluginAppConfig()
    val variantValues = gndExtension.getReferencesConcatenated("(DE-588)1101472588")
    //val variantValues = gndExtension.getReferencesConcatenated("(DE-588)1101472bla")
    assert(variantValues == """Deutsche Zentrumspartei in Bayern r. d. Rh.##xx##Deutsche Zentrumspartei in Bayern Rechts des Rheins##xx##Deutsche Zentrumspartei Rechtsrheinisches Bayern""")

    val referencedValues = gndExtension.getReferences5xxConcatenated("(DE-588)1101469501")

    //there are no references for this gnd number
    assert(referencedValues == "")
    val bla = gndExtension.getReferences5xxConcatenated("(DE-588)11014695bla")

    //response for not existing gnd is empty string
    assert(bla == "")

  }

}
