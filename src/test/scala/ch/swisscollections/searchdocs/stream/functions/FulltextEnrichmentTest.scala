/*
 * content2searchdocs
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.searchdocs.stream.functions

import ch.swisscollections.searchdocs.extensions.FulltextContentEnrichment
import ch.swisscollections.searchdocs.utilities.AppProperties
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.io.Source
import scala.xml.XML

class FulltextEnrichmentTest extends AnyFunSuite with Matchers {
  //ignore for the moment as this doesn't work on gitlab
  //but it works locally with dummy environment variables
  ignore("Fulltext eperiodica") {
    AppProperties.initTransformation()
    val enricher = new FulltextContentEnrichment()
    val content = enricher.readURLContent("https://www.e-periodica.ch/cntmng?type=pdf&pid=scs-003:1969:56::192")
    //println("hello")
    //println(content)
    assert(content.startsWith(" Zeitschrift: Schweizer Schule  Band: 56 (1969)  Heft: 14-15  Artikel: Erfahrungen mit programmiertem Unterricht im Ausland  Autor: Flammer, August  DOI: https://doi.org/10.5169/seals-533503  Nutzungsbedingungen Die ETH-Bibliothek ist die Anbieterin der digitalisierten Zeitschriften. Sie besitzt keine Urheberrechte an den Zeitschriften und ist nicht verantwortlich für deren Inhalte"))



  }

  ignore("Fulltext scripta") {
    AppProperties.initTransformation()
    val enricher = new FulltextContentEnrichment()
    //this pdf is ok
    val content2 = enricher.readURLContent("https://scripta.bbf.dipf.de/viewer/api/v1/records/1007863048_1792/sections/LOG_0026/pdf/")

    assert(content2.startsWith(" Amalia Eliabeth"))

  }

  ignore("Fulltext scripta with redirect") {
    AppProperties.initTransformation()
    val enricher = new FulltextContentEnrichment()

    // this one has a 301 redirect (add / at the end)
    val content3 = enricher.readURLContent("https://scripta.bbf.dipf.de/viewer/api/v1/records/1007863048_1792/sections/LOG_0026/pdf")
    assert(content3.startsWith(" Amalia Eliabeth"))

  }

  ignore("Fulltext Enrichment e-rara") {
    AppProperties.initTransformation()
    val enricher = new FulltextContentEnrichment()

    // this one has a 301 redirect (add / at the end)
    val content3 = enricher.readURLContent("https://www.e-rara.ch/download/pdf/25583780.pdf")
    //val content3 = enricher.readURLContent("/home/lionel/Bureau/1007863048_1792_LOG_0026.pdf")
    assert(content3.startsWith(" www.e-rara.ch  Lesebuch für die Unterklassen schweizerischer Volksschulen  Eberhard, Gerold"))
  }

}
