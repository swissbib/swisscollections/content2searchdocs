/*
 * content2searchdocs
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.searchdocs.stream.functions

import ch.swisscollections.searchdocs.utilities.AppProperties
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.io.Source
import scala.xml.XML
import scala.xml.PrettyPrinter

class Marc2SearchDocTransformTest extends AnyFunSuite with Matchers {

  private def getTextFromResourcesFolder(filepath: String): String = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    text
  }

  test("Test presence of hierarchy_parent_id") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_with_header.xml"))
    //println(result)
    assert(result.contains("<field name=\"hierarchy_parent_id\">(HAN)000109604DSV05</field>"))
    assert(!result.contains("<field name=\"hierarchy_parent_id\">(HAN)000000000000109604DSV05</field>"))

   }

  test("Bestand") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_Bestand.xml"))
    assert(!result.contains("<field name=\"hierarchy_parent_id\">"))
  }

  test("navAuthor hierarchical") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_with_111.xml"))
    assert(result.contains("0/Grenzen, Räume und Identitäten am Oberrhein und seine Nachbarregionen von der Antike bis zum Hochmittelalter, Freiburg im Breisgau Veranstaltung (2013)/"))

  }

  test("navAuthor relationship") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_multiple-relations.xml"))
    assert(result.contains("0/Vogel, Wladimir (1896-1984)/"))
    assert(result.contains("1/Vogel, Wladimir (1896-1984)/aut/"))
    assert(result.contains("1/Vogel, Wladimir (1896-1984)/rcp/"))
    assert(result.contains("0/Mahler, Elsa/"))
    assert(result.contains("1/Mahler, Elsa/top/"))
    assert(result.contains("1/Curtius Rufus, Quintus (ca. 1.\\/2. Jh.)/aut/"))
    assert(!result.contains("<field name=\"author_rcp_brw_mv\">Vogel, Wladimir (1896-1984)</field>"))
    assert(result.contains("<field name=\"author_rcp_brw_mv\">(DE-588)118627473</field>"))
    assert(result.contains("<field name=\"author_brw_mv\">(DE-588)116431520</field>"))

  }

  test("format") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_gnd-content.xml"))
    assert(result.contains("<field name=\"format_hierarchy_str_mv\">0/PR/"))
    assert(result.contains("<field name=\"format_hierarchy_str_mv\">1/PR/journal/"))
    assert(result.contains("<field name=\"format_str_mv\">journal"))
    assert(result.contains("<field name=\"format_parent_str_mv\">PR"))
    assert(result.contains("<field name=\"icon_code_str_mv\">PR"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_gnd-music.xml"))
    assert(result2.contains("<field name=\"format_hierarchy_str_mv\">0/SM/"))
    assert(result2.contains("<field name=\"format_hierarchy_str_mv\">1/SM/score/"))
    assert(result2.contains("<field name=\"format_hierarchy_str_mv\">1/SM/parts/"))
    assert(result2.contains("<field name=\"format_str_mv\">score"))
    assert(result2.contains("<field name=\"format_str_mv\">parts"))
    assert(result2.contains("<field name=\"format_parent_str_mv\">SM"))

    val result3 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_ZB-965.xml"))
    assert(result3.contains("<field name=\"format_hierarchy_str_mv\">0/MP/"))
    assert(result3.contains("<field name=\"format_hierarchy_str_mv\">1/MP/panorama/"))
    assert(result3.contains("<field name=\"format_hierarchy_str_mv\">1/MP/atlas/"))
    assert(result3.contains("<field name=\"format_str_mv\">panorama"))
    assert(result3.contains("<field name=\"format_str_mv\">atlas"))
    assert(result3.contains("<field name=\"format_parent_str_mv\">MP"))

    val result4 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_with-351.xml"))
    assert(result4.contains("<field name=\"format_hierarchy_str_mv\">0/AR/"))
    assert(result4.contains("<field name=\"format_hierarchy_str_mv\">0/MS/"))
    assert(result4.contains("<field name=\"format_hierarchy_str_mv\">1/AR/item/"))
    assert(result4.contains("<field name=\"format_hierarchy_str_mv\">1/AR/file/"))
    assert(result4.contains("<field name=\"format_hierarchy_str_mv\">1/MS/letter/"))
    assert(result4.contains("<field name=\"format_hierarchy_str_mv\">1/AR/letter/"))
    assert(result4.contains("<field name=\"format_hierarchy_str_mv\">1/MS/autograph/"))
    assert(result4.contains("<field name=\"format_str_mv\">letter"))
    assert(result4.contains("<field name=\"format_str_mv\">autograph"))
    assert(result4.contains("<field name=\"format_str_mv\">item"))
    assert(result4.contains("<field name=\"format_str_mv\">file"))
    assert(result4.contains("<field name=\"format_parent_str_mv\">AR"))

    val result5 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_basler-bibliographie.xml"))
    assert(result5.contains("<field name=\"format_hierarchy_str_mv\">0/BE/"))
    assert(result5.contains("<field name=\"format_hierarchy_str_mv\">1/BE/basel/"))
    assert(result5.contains("<field name=\"format_hierarchy_str_mv\">1/BE/zurich/"))
    assert(result5.contains("<field name=\"format_str_mv\">basel"))
    assert(result5.contains("<field name=\"format_str_mv\">zurich"))
    assert(!result5.contains("<field name=\"format_str_mv\">BE"))
    assert(result5.contains("<field name=\"format_parent_str_mv\">BE"))

    val result6 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_cd.xml"))
    assert(result6.contains("<field name=\"format_hierarchy_str_mv\">0/MR/"))
    assert(result6.contains("<field name=\"format_hierarchy_str_mv\">1/MR/cd/"))
    assert(result6.contains("<field name=\"format_str_mv\">cd"))
    assert(result6.contains("<field name=\"format_parent_str_mv\">MR"))

    val result7 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bibliography-map.xml"))
    assert(result7.contains("<field name=\"format_hierarchy_str_mv\">0/BE/"))
    assert(result7.contains("<field name=\"format_hierarchy_str_mv\">1/BE/basel/"))
    assert(!result7.contains("<field name=\"format_hierarchy_str_mv\">1/MP/basel/"))
    assert(!result7.contains("<field name=\"format_hierarchy_str_mv\">1/BE/atlas/"))
    assert(result7.contains("<field name=\"format_hierarchy_str_mv\">0/MP/"))
    assert(result7.contains("<field name=\"format_hierarchy_str_mv\">1/MP/atlas/"))
    assert(result7.contains("<field name=\"format_str_mv\">basel"))
    assert(result7.contains("<field name=\"format_str_mv\">atlas"))
    assert(result7.contains("<field name=\"format_parent_str_mv\">MP"))
    assert(!result7.contains("<field name=\"format_parent_str_mv\">BE"))

    val result8 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_autograph-map.xml"))
    assert(result8.contains("<field name=\"format_hierarchy_str_mv\">0/MP/"))
    assert(result8.contains("<field name=\"format_hierarchy_str_mv\">1/MP/mapms/"))
    assert(!result8.contains("<field name=\"format_hierarchy_str_mv\">1/MP/autograph/"))
    assert(result8.contains("<field name=\"format_hierarchy_str_mv\">0/MS/"))
    assert(result8.contains("<field name=\"format_hierarchy_str_mv\">1/MS/mapms/"))
    assert(result8.contains("<field name=\"format_hierarchy_str_mv\">1/MS/autograph/"))
    assert(result8.contains("<field name=\"format_hierarchy_str_mv\">0/AR/"))
    assert(result8.contains("<field name=\"format_hierarchy_str_mv\">1/AR/item/"))
    assert(result8.contains("<field name=\"format_str_mv\">autograph"))
    assert(result8.contains("<field name=\"format_str_mv\">mapms"))
    assert(result8.contains("<field name=\"format_str_mv\">item"))
    assert(result8.contains("<field name=\"format_parent_str_mv\">AR"))
    assert(!result8.contains("<field name=\"format_parent_str_mv\">MS"))
    assert(!result8.contains("<field name=\"format_parent_str_mv\">MP"))

    val result9 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_dia.xml"))
    assert(result9.contains("<field name=\"format_hierarchy_str_mv\">0/IO/"))
    assert(result9.contains("<field name=\"format_hierarchy_str_mv\">1/IO/fotografie/"))

    val result10 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_ldr-p.xml"))
    assert(result10.contains("<field name=\"format_hierarchy_str_mv\">0/AR/"))
    assert(result10.contains("<field name=\"format_hierarchy_str_mv\">1/AR/series/"))
    assert(result10.contains("<field name=\"format_str_mv\">series"))
    assert(!result10.contains("<field name=\"format_str_mv\">AR"))
    assert(result10.contains("<field name=\"format_parent_str_mv\">AR"))

    val result11 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_mapprint.xml"))
    assert(!result11.contains("<field name=\"format_hierarchy_str_mv\">1/DS/mapprint"))

    /*val result12 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_Trogen_No_Tectonics.xml"))
    assert(result12.contains("<field name=\"format_hierarchy_str_mv\">1/AR/typoskript"))*/

    val result13 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-an-bernoulli.xml"))
    assert(!result13.contains("<field name=\"format_hierarchy_str_mv\">1/MS/textms"))
    assert(result13.contains("<field name=\"format_hierarchy_str_mv\">1/MS/letter"))
    assert(result13.contains("<field name=\"format_hierarchy_str_mv\">1/MS/autograph"))
  }

  test("245 without indicator 2") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_245_no_indicator2.xml"))
    assert(result.contains("<field name=\"title_sort\">StimmederHeimat<"))
  }

  test("publplace facet") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_orte-264-751.xml"))
    assert(result.contains("<field name=\"navPublPlace_str_mv\">ProxyValue: Strassburg"))
    assert(result.contains("<field name=\"navPublPlace_str_mv\">ProxyValue: Freiburg im Breisgau"))
    assert(result.contains("<field name=\"navPublPlace_str_mv\">ProxyValue: Strassburg"))
    assert(result.contains("<field name=\"navPublPlace_str_mv\">ProxyValue: Basel"))
    assert(!result.contains("<field name=\"navPublPlace_str_mv\">ProxyValue: Erscheinungsort nicht ermittelbar"))
    assert(!result.contains("<field name=\"navPublPlace_str_mv\">ProxyValue: S.l."))
  }

  test("sublocal") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_sublocal.xml"))
    assert(result.contains("<field name=\"sublocal\">Theater Basel"))
    assert(result.contains("<field name=\"sublocal_ubs-AC_txt_mv\">Theater Basel"))
    assert(!result.contains("<field name=\"sublocal\">Theaterarbeit"))
    assert(result.contains("<field name=\"classif_ubs-FA\">4.4.1.4.48"))
    assert(result.contains("<field name=\"turcode_str_mv\">ZueBiSACH32_Z01"))
    assert(result.contains("<field name=\"turperson_brw_mv\">(DE-588)12345678"))
    assert(result.contains("<field name=\"browse_subpers_brw_mv\">(DE-588)12345678"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_genre.xml"))
    assert(result2.contains("<field name=\"nekrolog_brw_mv\">Kindweiler-Beck, Salome (1620-1667)"))
    assert(result2.contains("<field name=\"nekrolog_brw_mv\">(DE-588)1037510807"))
    assert(!result2.contains("<field name=\"tectonics_str_mv\">Genath, Johann Rudolph"))

  }

  test("holding") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_holding.xml"))
    assert(result.contains("<field name=\"institution\">A100"))
    assert(result.contains("<field name=\"institution\">Z01"))
  }

  test("genre") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_genre.xml"))
    assert(result.contains("<field name=\"navSubform\">ProxyValue: Kasualia"))
    assert(!result.contains("<field name=\"navSubform\">ProxyValue: Aquarell"))
    assert(result.contains("<field name=\"navSubform\">ProxyValue: Allegorische Darstellung"))
    assert(result.contains("<field name=\"navSubform\">ProxyValue: Abstrakte Kunst"))
    assert(result.contains("<field name=\"navSubform\">ProxyValue: Nachruf"))
    assert(result.contains("<field name=\"navSubform\">ProxyValue: Filmmusik"))
    assert(!result.contains("<field name=\"navSubform\">ProxyValue: Altkarte"))
    assert(!result.contains("<field name=\"navSubform\">ProxyValue: CD"))
    assert(result.contains("<field name=\"navSubform\">ProxyValue: Rezepte"))
    assert(!result.contains("<field name=\"formImage_str_mv\">"))
    assert(result.contains("<field name=\"sublocal_uzb-ZG_str_mv\">Abstrakte Kunst</field>"))
    assert(result.contains("<field name=\"sublocal_uzb-ZG_txt_mv\">Abstrakte Kunst [Formschlagwort Sondersammlungen]</field>"))
  }

  test("medium") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_with-382.xml"))
    assert(result.contains("<field name=\"submedium_idsmusi_txt_mv\">Mezzosopran"))
    assert(result.contains("<field name=\"submedium_idsmusi_txt_mv\">Klavier"))
    assert(result.contains("<field name=\"submedium_idsmusi_brw_mv\">Mezzosopran (1), Klavier (1)"))
    assert(result.contains("<field name=\"submedium_idsmusi_brw_mv\">Chor, SATB (2), Orchester (1)"))
    assert(result.contains("<field name=\"submedium_idsmusi_brw_mv\">Klavier, 4-händig (1)"))
    assert(result.contains("<field name=\"submedium_idsmusi_brw_mv\">Singstimme (mehrere), Orchester (1)"))
    assert(result.contains("<field name=\"submedium_idsmusi_brw_mv\">Violine (5), Klavier, 8-händig (1)"))
    assert(result.contains("<field name=\"submedium_idsmusi_brw_mv\">Akkordeon</field>"))
    assert(result.contains("<field name=\"musicGenre_str_mv\">Arie, 18. Jh.</field>"))
    assert(result.contains("<field name=\"musicGenre_txt_mv\">Arie, 18. Jh.</field>"))
    assert(result.contains("<field name=\"formImage_str_mv\">Postkarte</field>"))
    assert(result.contains("<field name=\"formImage_txt_mv\">Postkarte</field>"))
    assert(result.contains("<field name=\"navSub_medium\">ProxyValue: Chor, SATB (2)</field>"))
    assert(!result.contains("<field name=\"navSub_medium\">ProxyValue: Chor</field>"))
    assert(!result.contains("<field name=\"navSub_medium\">ProxyValue: Chor (2)</field>"))
    assert(!result.contains("<field name=\"navSub_medium\">ProxyValue: Chor, SATB</field>"))
    assert(result.contains("<field name=\"navSub_medium\">ProxyValue: Orchester (1)</field>"))
    assert(result.contains("<field name=\"navSub_medium\">ProxyValue: Klavier, 4-händig (1)</field>"))
    assert(result.contains("<field name=\"navSub_medium\">ProxyValue: Singstimme (mehrere)</field>"))
    assert(result.contains("<field name=\"navSub_medium\">ProxyValue: Violine (5)</field>"))
    assert(result.contains("<field name=\"navSub_medium\">ProxyValue: Klavier, 8-händig (1)</field>"))
    assert(result.contains("<field name=\"submedium_idsmusi_full_str_mv\">Violine 5 Klavier 1 8-händig</field>"))
    assert(result.contains("<field name=\"submedium_idsmusi_full_str_mv\">Chor 2 SATB Orchester 1</field>"))
    assert(result.contains("<field name=\"submedium_idsmusi_full_str_mv\">Mezzosopran 1 Klavier 1</field>"))
  }

  test("collect/weed item and holding") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_hol-item.xml"))
    assert(result.contains("<field name=\"callnumber\">UBH DJ 2198"))
    assert(result.contains("<field name=\"callnumber_A100_str_mv\">UBH DJ 2198"))
    assert(result.contains("<field name=\"institution\">A100</field>"))
    assert(result.contains("<field name=\"institution\">A115</field>"))
    assert(!result.contains("<field name=\"institution\">A999</field>"))
    assert(!result.contains("<field name=\"institution\">BFREE</field>"))
  }

  test("tectonics") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_SWA_Dossier.xml"))

    val p = new scala.xml.PrettyPrinter(80, 4)
    //println(p.format(scala.xml.XML.loadString(result)))

    assert(result.contains("<field name=\"tectonics_str_mv\">0/A125/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">1/A125/Wirtschaftsarchive/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">4/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Fischwirtschaft/Schweizerische Ausstellung für Land- und Forstwirtschaft und Fischerei, Bern (1895)/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">4/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Werbewirtschaft/Schweizerische Ausstellung für Land- und Forstwirtschaft und Fischerei, Bern (1895)/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">4/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Forstwirtschaft/Schweizerische Ausstellung für Land- und Forstwirtschaft und Fischerei, Bern (1895)/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">3/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Forstwirtschaft/"))
    assert(!result.contains("<field name=\"tectonics_str_mv\">3/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Werbewirtschaft-GND/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">2/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">4/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Werbewirtschaft/Schweizerische Ausstellung für Land- und Forstwirtschaft und Fischerei, Bern (1895)/"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_SWA_Personen_Nachlass.xml"))
    //println(result2)
    assert(result2.contains("<field name=\"tectonics_str_mv\">2/A125/Wirtschaftsarchive/Personenarchive und Nachlässe/"))
    assert(result2.contains("<field name=\"tectonics_str_mv\">3/A125/Wirtschaftsarchive/Personenarchive und Nachlässe/Schindler, Hans (1896-1984)/"))

    val result3 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_Bestand.xml"))
    assert(result3.contains("<field name=\"tectonics_str_mv\">3/A100/Nachlässe/Personen/Cullmann, Oscar (1902-1999)/"))
    assert(result3.contains("<field name=\"tectonics_str_mv\">2/A100/Nachlässe/Personen/"))
    assert(result3.contains("<field name=\"tectonics_str_mv\">1/A100/Nachlässe/"))
    assert(result3.contains("<field name=\"tectonics_str_mv\">0/A100/"))
    assert(!result3.contains("<field name=\"tectonics_str_mv\">0/A125/"))
    assert(!result3.contains("<field name=\"tectonics_str_mv\">1/A100/Autographen/"))
    assert(result3.contains("<field name=\"topic_tectonic_str_mv\">0/Wissenschaft/"))
    assert(result3.contains("<field name=\"topic_tectonic_str_mv\">1/Wissenschaft/Cullmann, Oscar (1902-1999)/"))
    //println(result3)

    val result4 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_Autograph.xml"))
    //println(result4)
    assert(result4.contains("<field name=\"tectonics_str_mv\">2/A100/Autographen/Gelzer, Heinrich (1888-1963)/"))
    assert(result4.contains("<field name=\"tectonics_str_mv\">1/A100/Autographen/"))
    assert(result4.contains("<field name=\"tectonics_str_mv\">0/A100/"))
    assert(!result4.contains("<field name=\"tectonics_str_mv\">0/A125/"))
    assert(!result4.contains("<field name=\"tectonics_str_mv\">1/A100/Nachlässe/"))

    val result5 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_Trogen_Nachlass.xml"))
    //println(result5)
    assert(result5.contains("<field name=\"tectonics_str_mv\">2/SGARK/Archive von Personen und Organisationen/Amann, Hans (1922-)/"))
    assert(result5.contains("<field name=\"tectonics_str_mv\">1/SGARK/Archive von Personen und Organisationen/"))
    assert(result5.contains("<field name=\"tectonics_str_mv\">0/SGARK/"))
    assert(!result5.contains("<field name=\"tectonics_str_mv\">0/A125/"))
    assert(!result5.contains("<field name=\"tectonics_str_mv\">1/A100/Nachlässe/"))

    val result6 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_Trogen_No_Tectonics.xml"))
    //println(p.format(scala.xml.XML.loadString(result6)))
    assert(!result6.contains("<field name=\"tectonics_str_mv\">"))
    //check that angle brackets are removed
    assert(result6.contains("<field name=\"title_in_hierarchy\">38 : Der Staubteufel / von Aleister Crowley ; übersetzt von Herbert Schmolke (1913-1960)</field>"))

    val result7 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_SWA_WirtschaftsdokumentationDoksF.xml"))
    //println(result7)
    assert(result7.contains("<field name=\"business_doc_str_mv\">2/A125/Firmen und Organisationen/Chemieindustrie/"))

    val result8 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_SWA_WirtschaftsdokumentationDoksS.xml"))
    //println(result8)
    assert(result8.contains("<field name=\"business_doc_str_mv\">2/A125/Sachthemen Wirtschaft/Öffentliche Finanzen/"))

    val result9 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_SWA_WirtschaftsdokumentationDoksB.xml"))
    //println(result9)
    assert(result9.contains("<field name=\"business_doc_str_mv\">1/A125/Personen Wirtschaft/"))

    val result10 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_sob.xml"))
    assert(result10.contains("<field name=\"tectonics_str_mv\">0/B415/"))
    assert(result10.contains("<field name=\"tectonics_str_mv\">1/B415/Archive von Personen und Organisationen/"))
    assert(result10.contains("<field name=\"tectonics_str_mv\">2/B415/Archive von Personen und Organisationen/Rossica Europeana/"))

    val result11 = Marc2SearchDocTransform.apply("",getTextFromResourcesFolder("record_tectonics/han_record_haller.xml"))
    assert(result11.contains("<field name=\"tectonics_str_mv\">0/B404/"))
    assert(result11.contains("<field name=\"tectonics_str_mv\">1/B404/Druckbelege und Geschäftsunterlagen Druckerei Haller (1800-1859)/"))

    val result12 = Marc2SearchDocTransform.apply("",getTextFromResourcesFolder("record_tectonics/zbc_bestand.xml"))
    assert(result12.contains("<field name=\"tectonics_str_mv\">0/Z01/</field>"))
    assert(result12.contains("<field name=\"tectonics_str_mv\">1/Z01/Personennachlässe/</field>"))
    assert(result12.contains("<field name=\"tectonics_str_mv\">2/Z01/Personennachlässe/Steinfels, Heinrich Wilhelm (1874-1929) : Sammlung/</field>"))
  }

  test("remove holdings from non swisscollections libraries") {
    AppProperties.initTransformation()
    val result6 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_cd.xml"))
    assert(!result6.contains("<subfield code=\"b\">A164</subfield>"))
    assert(result6.contains("<subfield code=\"b\">Z05</subfield>"))

  }

  test("creator roles for browse fields") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_arranger.xml"))

    assert(result.contains("<field name=\"author_arr_txt_mv\">Behrend, Siegfried, 1933-1990</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">Musik für die Gitarre</field>"))
    assert(result.contains("<field name=\"author_cmp_txt_mv\">Purcell, Henry, 1659-1695</field>"))
    val p = new scala.xml.PrettyPrinter(80, 4)
    //println(p.format(scala.xml.XML.loadString(result)))
  }

  test("remove price") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_cd.xml"))
    assert(result.contains("Dummy Content"))
    assert(result.contains("Dummy Content 2"))
    assert(!result.contains("The cost was 1'000'000'000 francs"))
    assert(!result.contains("Sotheby"))
    assert(!result.contains("Länggassstrasse 51, CH-3012 Bern"))
    assert(result.contains("Fondation de France"))

  }

  test("remove internal item and holding notes") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_cd.xml"))

    assert(!result.contains("<subfield code=\"x\">Geschenk"))
    assert(result.contains("<subfield code=\"j\">UBH Rb 2176"))
    assert(!result.contains("<subfield code=\"x\">Geheim"))
  }

  test("browse titles") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_cd.xml"))
    assert(!result.contains("<field name=\"title_browse_brw_mv\">Kammermusik. Auswahl</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">String quartets, Piano quintet</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">Arcadiana : op. 12 (1994) for string quartet (19:03)</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">The four quarters : op. 28 (2010) for string quartet (17:26)</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">Piano quintet : (2000) for piano and string quartet (21:45)</field>"))
    assert(!result.contains("<field name=\"title_browse_brw_mv\">Arcadiana</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">(DE-588)30057231X</field>"))
    assert(!result.contains("<field name=\"title_browse_brw_mv\">Quintett, Violine (2), Viola, Violoncello, Klavier. op. 20</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">(DE-588)300832664</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">Fantasy, Violine (2), Viola, Violoncello, Klavier. op. 20</field>"))
    assert(!result.contains("<field name=\"title_browse_brw_mv\">(DE-588)130651222</field>"))
    assert(result.contains("<field name=\"work_browse_brw_mv\">Adès (1971-) / Streichquartette, Klavierquintett</field>"))
    assert(result.contains("<field name=\"work_browse_brw_mv\">Adès (1971-) / Kammermusik. Auswahl</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">(DE-588)1140498347</field>"))
    assert(!result.contains("<field name=\"work_browse_brw_mv\">Adès (1971-) / Quintett, Violine (2), Viola, Violoncello, Klavier. op. 20</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">(DE-588)300832664</field>"))
    assert(!result.contains("<field name=\"work_browse_brw_mv\">Adès (1971-) / Arcadiana</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">(DE-588)30057231X</field>"))
    assert(!result.contains("<field name=\"work_browse_brw_mv\">Adès (1971-) / The four quarters</field>"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">(DE-588)113913227X</field>"))
    assert(result.contains("<field name=\"work_browse_brw_mv\">Adès (1971-) / Fantasy, Violine (2), Viola, Violoncello, Klavier. op. 20</field>"))
    assert(!result.contains("<field name=\"work_browse_brw_mv\">Gex, Regula /<field>"))
    assert(result.contains("<field name=\"title\">Streichquartette, Klavierquintett</field>"))
    assert(result.contains("<field name=\"title_short\">Streichquartette, Klavierquintett</field>"))
    assert(result.contains("<field name=\"title_alt\">Arcadiana : op. 12 (1994) for string quartet (19:03)</field>"))
    assert(result.contains("<field name=\"work_browse_brw_mv\">Lualdi, Adriano&lt;&lt;di&gt;&gt;(1885-1971) /&lt;&lt;La&gt;&gt;figlia del re</field>"))
    assert(result.contains("<field name=\"contributor_brw_mv\">Dammann, Guy</field>"))
    assert(result.contains("<field name=\"contributor_brw_mv\">Gex, Regula</field>"))
    assert(!result.contains("<field name=\"contributor_brw_mv\">DoelenKwartet</field>"))
    assert(!result.contains("<field name=\"contributor_brw_mv\">Adès (1971-)</field>"))
    assert(!result.contains("<field name=\"author_brw_mv\">Adès (1971-)</field>"))
    assert(result.contains("<field name=\"author_brw_mv\">(DE-588)129843970</field>"))


    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_work-title.xml"))

    assert(!result2.contains("<field name=\"title_browse_brw_mv\">Quintette, Violine (2), Viola, Violoncello, Klavier. B 28, A-Dur</field>"))
    assert(result2.contains("<field name=\"title_browse_brw_mv\">(DE-588)300047851</field>"))
    assert(result2.contains("<field name=\"title_browse_brw_mv\">Bibel. Altes Testament</field>"))
    assert(!result2.contains("<field name=\"work_browse_brw_mv\">Dvořák, Antonín (1841-1904) / Quintette, Violine (2), Viola, Violoncello, Klavier. B 28, A-Dur</field>"))
    assert(result2.contains("<field name=\"work_browse_brw_mv\">Dvořák, Antonín (1841-1904) / Quintette, Violine (2), Viola, Violoncello, Klavier. B 999, A-Dur</field>"))
    assert(result2.contains("<field name=\"work_browse_brw_mv\">Dvořák, Antonín (1841-1904) / Piano quintets : = Klavierquintette</field>"))
  }

  test("titles") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform("", getTextFromResourcesFolder("record/han_record_work-title.xml"))
    assert(result.contains("<field name=\"title\">Piano quintets : = Klavierquintette</field>"))
    assert(result.contains("<field name=\"title_short\">Piano quintets</field>"))
    assert(result.contains("<field name=\"title_sub\">= Klavierquintette</field>"))
    assert(result.contains("<field name=\"title_alt\">Quint</field>"))
    assert(result.contains("<field name=\"title_alt\">Klavierquintette</field>"))
    assert(result.contains("<field name=\"title_alt\">Viola</field>"))
    assert(result.contains("<field name=\"title_alt\">A-Dur</field>"))
    assert(result.contains("<field name=\"title_alt\">Piano quintet op. 81 in A</field>"))
    assert(result.contains("<field name=\"title_alt\">Piano Quintet op. 5 in A</field>"))
    assert(result.contains("<field name=\"title_alt\">Quintette</field>"))
    assert(result.contains("<field name=\"title_alt\">Quintette Test</field>"))
    assert(result.contains("<field name=\"title_alt\">Quintette andere Schrift</field>"))
    assert(result.contains("<field name=\"title_additional_gnd_txt_mv\">ProxyValue: (DE-588)30004786X</field>"))

    val result2 = Marc2SearchDocTransform("", getTextFromResourcesFolder("record/han_record_autograph-map.xml"))
    assert(result2.contains("<field name=\"title\">Carte des possessions anglaises et françaises du continent de l'Amérique septentrionale</field>"))
    assert(result2.contains("<field name=\"title_short\">Carte des possessions anglaises et françaises du continent de l'Amérique septentrionale</field>"))
    assert(result2.contains("<field name=\"title_sub\"/>"))
  }

  test("basel bibliographies hierarchy") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_basler-bibliographie.xml"))
    //println(result)
    assert(result.contains("<field name=\"bibliographies_str_mv\">0/A100/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/A100/1"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">2/A100/1/1.5"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/A100/4"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">2/A100/4/4.3"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">3/A100/4/4.3/4.3.2"))
    assert(!result.contains("<field name=\"bibliographies_str_mv\">3/A100/1/1.1/1.1.1"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">2/A100/1/1.1"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">4/A100/4/4.2/4.2.5/4.2.5.8/"))
    assert(!result.contains("<field name=\"bibliographies_str_mv\">4/A100/4/4.2/4.2.5/4.2.5.8./"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">3/A100/2/2.4/2.4.1"))
    assert(!result.contains("<field name=\"bibliographies_str_mv\">1/A100//"))
    assert(!result.contains("<field name=\"bibliographies_str_mv\">2/A100//./"))
  }

  test("bern bibliographies hierarchy") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_berner-bibliographie.xml"))
    //println(result)
    assert(result.contains("<field name=\"bibliographies_str_mv\">0/B400/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/B400/2/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">2/B400/2/2.13/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">3/B400/2/2.13/2.13.1/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/B400/1/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">2/B400/1/1.4"))

  }

  test("zurich bibliographies hierarchy") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_zurcher-bibliographie.xml"))
    //println(result)
    assert(result.contains("<field name=\"bibliographies_str_mv\">0/Z01/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/Z01/ZueBiSACH08_Z01/"))

  }

  test ("solothurn bibliographies hierarchy") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_solothurner-bibliographie.xml"))
    //println(result)
    assert(result.contains("<field name=\"bibliographies_str_mv\">0/SO/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/SO/08/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/SO/12/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/SO/04/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">2/SO/04/04.2/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">3/SO/04/04.2/04.2.1/"))
    assert(!result.contains("<field name=\"bibliographies_str_mv\">2/SO/08/8./</field>"))
    assert(result.contains("<field name=\"format_hierarchy_str_mv\">0/BE/"))
    assert(result.contains("<field name=\"format_hierarchy_str_mv\">1/BE/solothurn/"))
    assert(result.contains("<field name=\"format_str_mv\">solothurn"))
    assert(result.contains("<field name=\"format_parent_str_mv\">BE"))
    assert(result.contains("<field name=\"sobibagent_brw_mv\">Rüdt-Ineichen, Maria. 1879-1980"))
    assert(result.contains("<field name=\"sobibgeo_brw_mv\">Solothurn"))
  }

  test ("aargau bibliography") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_aargau-bibliography.xml"))
    //println(result)
    assert(result.contains("<field name=\"bibliographies_str_mv\">0/AG/"))
    assert(result.contains("<field name=\"bibliographies_str_mv\">1/AG/08"))
    assert(result.contains("<field name=\"format_hierarchy_str_mv\">0/BE/"))
    assert(result.contains("<field name=\"format_hierarchy_str_mv\">1/BE/aargau/"))
  }

  test("good number and year as part of the hierarchy") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_part_cullmann_nachlass.xml"))
    val p = new scala.xml.PrettyPrinter(80, 4)
    //println(p.format(scala.xml.XML.loadString(result)))

    assert(result.contains("<field name=\"hierarchy_sequence\">00004</field>"))
    assert(result.contains("<field name=\"title_in_hierarchy\">IV : Druckfahnen mit Korrekturen von Oscar Cullmann (1902-1999)</field>"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_solothurner-bibliographie.xml"))
    //println(result2)
    assert(result2.contains("<field name=\"title_in_hierarchy\">Seiten 11-29 : Das freie Notariat im Kanton Solothurn: Einordnung und Streiflichter aus der Praxis der Aufsicht / Franz Fürst (2022)</field>"))
  }

  test("get parent via 773 field") {
      AppProperties.initTransformation()
      val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_brief773.xml"))

      val p = new scala.xml.PrettyPrinter(80, 4)
      //println(p.format(scala.xml.XML.loadString(result)))

      assert(result.contains("<field name=\"hierarchy_parent_id\">(HAN)000108017DSV05</field>"))
      assert(result.contains("<field name=\"hierarchy_sequence\">00189</field>"))
      assert(result.contains("<field name=\"title_in_hierarchy\">Bl.189-190 : Brief an Samuel Grynäus / von Joh[ann] Jacob Frey (1632)</field>"))



  }

  test("IZ id from 986") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_brief773.xml"))
    //println(result)
    assert(result.contains("<field name=\"ctrlnum\">9972411465005504</field>"))
    assert(result.contains("(41SLSP_UBS)9972411465005504</field>"))
  }

  test("conference") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_conference.xml"))
    assert(result.contains("<field name=\"navAuthor_full\">ProxyValue: Ein Arbeits- und Sozialversicherungsrechtler mit Weitblick - Symposium zum rechtswissenschaftlichen Wirken von Professor Hans Peter Tschudi, Basel (2019)</field>"))
    assert(result.contains("<field name=\"navAuthor_hierarchical\">ProxyValue: 0/Ein Arbeits- und Sozialversicherungsrechtler mit Weitblick - Symposium zum rechtswissenschaftlichen Wirken von Professor Hans Peter Tschudi, Basel (2019)/</field>"))
    assert(result.contains("<field name=\"navAuthor_full\">ProxyValue: Symposium Hans Peter Tschudi, Basel (2019)</field>"))
    assert(result.contains("<field name=\"navAuthor_hierarchical\">ProxyValue: 0/Symposium Hans Peter Tschudi, Basel (2019)/</field>"))
  }

  test("teilbestand") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_kryptonachlass_teichmuller.xml"))
    assert(result.contains("<field name=\"hierarchy_parent_id\">(HAN)000000001DSV05</field>"))
  }

  test("musik nachlass") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_musik_nachlass.xml"))
    //println(result)
    assert(result.contains("<field name=\"tectonics_str_mv\">2/A100/Nachlässe/Personen/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">1/A100/Nachlässe/"))
    assert(result.contains("<field name=\"tectonics_str_mv\">0/A100/"))
  }

  test("Rorschach archive") {
    AppProperties.initTransformation()
    //val p = new scala.xml.PrettyPrinter(80, 4)
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_Rorschach.xml"))
    //println(p.format(scala.xml.XML.loadString(result)))
    //println(result)

    assert(result.contains("<subfield code=\"b\">B583RO</subfield>"))
    assert(result.contains("<subfield code=\"b\">B583</subfield>"))
  }

  test("year to current only for actually running publications") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_year-m.xml"))
    assert(result.contains("<field name=\"publishDate\">1768</field>"))
    assert(result.contains("<field name=\"publishDateSort\">17680101</field>"))
    assert(result.contains("<field name=\"freshness\">1768-01-01T00:00:00Z</field>"))
    assert(!result.contains("<field name=\"publishDate\">202"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_year-c-no-year1.xml"))
    assert(result2.contains("<field name=\"publishDate\">202"))
    assert(!result2.contains("<field name=\"publishDateSort\">"))
    assert(result2.contains("<field name=\"freshness\">202"))

    val result3 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_year-c-year1.xml"))
    assert(result3.contains("<field name=\"publishDate\">2014</field>"))
    assert(result3.contains("<field name=\"publishDate\">2020</field>"))
    assert(result3.contains("<field name=\"publishDateSort\">20140101</field>"))
    assert(result3.contains("<field name=\"freshness\">202"))
  }

  test("sorting on full date") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_year-m.xml"))
    assert(result.contains("<field name=\"publishDateSort\">17680101</field>"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-an-extern.xml"))
    assert(result2.contains("<field name=\"publishDateSort\">17241230</field>"))

    val result3 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_245_no_indicator2.xml"))
    assert(result3.contains("<field name=\"publishDateSort\">19380101</field>"))
  }

  <!-- only usable with local mongo connection, ignored for automatic testing-->
  ignore("gnd enrichment") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform("", getTextFromResourcesFolder("record/han_record_gnd-enrich.xml"))

    assert(result.contains("<field name=\"publplace_additional_gnd_txt_mv\">Helvetia"))
    assert(result.contains("<field name=\"author_additional_gnd_txt_mv\">Kuruman"))
    assert(result.contains("<field name=\"title_additional_gnd_txt_mv\">Klavierquintett B 155"))
    assert(result.contains("<field name=\"title_additional_gnd_txt_mv\">Elder Edda"))
    assert(result.contains("<field name=\"subgnd_enriched\">Pathopsychologie"))
    assert(result.contains("<field name=\"subgnd_enriched\">Mode de vie"))
    assert(result.contains("<field name=\"related_gnd_txt_mv\">Kunstsammlung der Stadt Thun"))
    assert(result.contains("<field name=\"related_gnd_txt_mv\">Rowling, J. K."))
    assert(result.contains("<field name=\"related_gnd_txt_mv\">Leningrad"))
  }

  //this needs to be done better, as this makes problems at every changes...
  ignore("complete file") {
    AppProperties.initTransformation()
    // Transform and replace time_processed with a predefined time_processed
    val result = Marc2SearchDocTransform("", getTextFromResourcesFolder("record/han_record_holding.xml")).replaceFirst("time_processed\">.*Z<", "time_processed\">1900-01-01T17:49:31.342Z<")


    val p = new scala.xml.PrettyPrinter(80, 4)
    //println(p.format(scala.xml.XML.loadString(result)))


    assert(XML.loadString(result) == XML.loadString(getTextFromResourcesFolder("results/result1.xml")))


  }

  test("archive no aktenbildner") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_SWA_Fonds_No_Aktenbildner.xml"))
    assert(result.contains("<field name=\"tectonics_str_mv\">4/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Textilindustrie/Diverse/"))
  }

  test("good number when multiple 490") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_multitree.xml"))

    //val p = new scala.xml.PrettyPrinter(80, 4)
    //println(p.format(scala.xml.XML.loadString(result)))

    assert(result.contains("1/A : [Titelblatt Courtalon] (1774)"))
    assert(result.contains("<field name=\"hierarchy_sequence\">00001.A</field>"))
    assert(result.contains("<field name=\"title_in_hierarchy\">0/1 : [Titelblatt Courtalon] (1774)</field>"))
    assert(result.contains("<field name=\"hierarchy_sequence\">00000.00001</field>"))

  }

  test("Tectonics Konferenz") {
    AppProperties.initTransformation()
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_tectonics/han_record_SWA_Konferenz.xml"))

    //val p = new scala.xml.PrettyPrinter(80, 4)
    //println(p.format(scala.xml.XML.loadString(result)))

    assert(result.contains("<field name=\"tectonics_str_mv\">4/A125/Wirtschaftsarchive/Firmen- und Verbandsarchive/Werbewirtschaft/Schweizerische Ausstellung \"Land- und Ferienhaus\"/</field>"))

  }

  test ("ecodices thumbnail") {
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_ecodices.xml"))
    assert(result.contains("datafield tag=\"856\" ind1=\"4\" ind2=\"8\"><subfield code=\"u\">https://e-codices.ch/loris/zbs/zbs-SII-0043/zbs-SII-0043_023v.jp2</subfield><subfield code=\"z\">Thumbnail e-codices"))
    //println(result)
  }

  test ("Keller bibliography") {
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-about.xml")) /*IDSZ1GK*/
    assert(result.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/about"))


    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-image.xml")) /*IDSZ5gkn*/
    assert(result2.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result2.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/image"))

    val result3 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-library.xml")) /* Signatur 42 */
    assert(result3.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result3.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/library"))

    val result4 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-manuscript.xml")) /* Signatur Ms GK */
    assert(result4.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result4.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/manuscript"))

    val result5 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-translation.xml")) /*IDSZ1GK*/
    assert(result5.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result5.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/translation"))

    val result6 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-work.xml")) /*GK_Z01*/
    assert(result6.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result6.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/work"))

    val result7 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-multi.xml"))
    assert(result7.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result7.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/work"))
    assert(result7.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/library"))

    val result8 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-image-only.xml"))
    assert(result8.contains("<field name=\"biblio_special_str_mv\">0/GKZ01/"))
    assert(result8.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/image"))
    assert(!result8.contains("<field name=\"biblio_special_str_mv\">1/GKZ01/translation"))
    assert(!result8.contains("<field name=\"biblio_special_str_mv\">1/GKZ01//"))
    //println(result8)

  }

  test("Record with multiple 596 3 0 t") {
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_multiple_596_t.xml"))
    assert(result.contains("<field name=\"title_browse_brw_mv\">&gt;Annotationes in libros Aristotelis de anima. Prologus&lt;. Interpretaturi tractatus, quos Aristoteles de anima instituit ... - ... ad hoc munus est necessaria conversio ad phantasmata. Explicit liber tertius de anima.</field>"))
    assert(result.contains("Annotationes in libros Aristotelis de anima"))
    //val p = new scala.xml.PrettyPrinter(80, 4)
    //println(p.format(scala.xml.XML.loadString(result)))
  }

  test ("Bernoulli bibliography") {
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-an-extern.xml"))
    assert(result.contains("<field name=\"biblio_special_str_mv\">0/Bernoulli-Briefinventar (BIBB)/"))
    assert(result.contains("<field name=\"biblio_special_str_mv\">1/Bernoulli-Briefinventar (BIBB)/Hermann, Jacob (1678-1733)/"))
    assert(result.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Hermann, Jacob (1678-1733)/Blumentrost, Laurentius (1692-1755)/"))
    assert(!result.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Hermann, Jacob (1678-1733)/Hermann, Jacob (1678-1733)/"))


    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-von-extern.xml"))
    assert(result2.contains("<field name=\"biblio_special_str_mv\">0/Bernoulli-Briefinventar (BIBB)/"))
    assert(result2.contains("<field name=\"biblio_special_str_mv\">1/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/"))
    assert(result2.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/Blumentrost, Laurentius (1692-1755)/"))

    val result3 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-an-bernoulli.xml"))
    assert(result3.contains("<field name=\"biblio_special_str_mv\">0/Bernoulli-Briefinventar (BIBB)/"))
    assert(result3.contains("<field name=\"biblio_special_str_mv\">1/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/"))
    assert(result3.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/Bernoulli, Nicolaus I (1687-1759)/"))
    assert(result3.contains("<field name=\"biblio_special_str_mv\">1/Bernoulli-Briefinventar (BIBB)/Bernoulli, Nicolaus I (1687-1759)/"))
    assert(result3.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Bernoulli, Nicolaus I (1687-1759)/Bernoulli, Daniel I (1700-1782)/"))

    val result4 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-an-extern-no-gnd.xml"))
    assert(result4.contains("<field name=\"biblio_special_str_mv\">0/Bernoulli-Briefinventar (BIBB)/"))
    assert(result4.contains("<field name=\"biblio_special_str_mv\">1/Bernoulli-Briefinventar (BIBB)/Bernoulli, Jacob I (1655-1705)/"))
    assert(result4.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Bernoulli, Jacob I (1655-1705)/Paravicini, Vincentius (1644-1726)/"))

    val result5 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-multi.xml"))
    assert(result5.contains("<field name=\"biblio_special_str_mv\">0/Bernoulli-Briefinventar (BIBB)/"))
    assert(result5.contains("<field name=\"biblio_special_str_mv\">1/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/"))
    assert(result5.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/Bernoulli, Johann (1744-1807)/"))
    assert(result5.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/Euler, Leonhard (1707-1783)/"))
    assert(result5.contains("<field name=\"biblio_special_str_mv\">2/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/Alembert, Jean Le Rond d' (1717-1783)/</field>"))

    val result6 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_bernoulli-doppelt.xml"))
    //println(result6)
    assert(result6.contains("<field name=\"biblio_special_str_mv\">0/Bernoulli-Briefinventar (BIBB)/"))
    assert(result6.contains("<field name=\"biblio_special_str_mv\">1/Bernoulli-Briefinventar (BIBB)/Bernoulli, Daniel I (1700-1782)/"))

  }

  test ("geigy-hagenbach A118") {
    val result = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_A118.xml"))
    assert(!result.contains("<field name=\"institution\">A118</field>"))
    //println(result)
  }

  test ("bgs facets") {
    val result1 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_bgs/schulwandbild.xml"))
    assert(result1.contains("<field name=\"navMedia_bgs_str_mv\">bild</field>"))
    assert(result1.contains("<field name=\"icon_code_str_mv\">bild</field>"))
    assert(!result1.contains("<field name=\"icon_code_str_mv\">VM</field>"))
    assert(!result1.contains("<field name=\"navPublPlace_bgs_str_mv\">"))
    assert(result1.contains("<field name=\"navSubform_bgs_str_mv\">ProxyValue: Schulwandbild</field>"))
    assert(result1.contains("<field name=\"subform_bgs\">Schulwandbild</field>"))
    assert(result1.contains("<field name=\"collection_bgs_hierarchy_str_mv\">0/stp/</field>"))
    assert(result1.contains("<field name=\"collection_bgs_hierarchy_str_mv\">1/stp/stp-swb/</field>"))
    assert(!result1.contains("<field name=\"collection_bgs_hierarchy_str_mv\">2/stp/"))
    assert(result1.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: stp</field>"))
    assert(result1.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: stp-swb</field>"))
    assert(!result1.contains("<field name=\"filter_bgs_str_mv\">ONL"))
    assert(result1.contains("<field name=\"callno_bgs_strl_mv\">SWB_1-451"))
    assert(result1.contains("<field name=\"format_additional_bgs_txt_mv\">Tableau mural"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_bgs/result-aip4.xml"))
    assert(result2.contains("<field name=\"navMedia_bgs_str_mv\">textpdf</field>"))
    assert(result2.contains("<field name=\"icon_code_str_mv\">textpdf</field>"))
    assert(!result2.contains("<field name=\"navPublPlace_bgs_str_mv\">"))
    assert(result2.contains("<field name=\"collection_bgs_hierarchy_str_mv\">0/ep/</field>"))
    assert(result2.contains("<field name=\"collection_bgs_hierarchy_str_mv\">1/ep/ep-snb/</field>"))
    assert(result2.contains("<field name=\"collection_bgs_hierarchy_str_mv\">2/ep/ep-snb/ep-aip-001/"))
    assert(result2.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: ep</field>"))
    assert(!result2.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: ep-snb</field>"))
    assert(result2.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: ep-aip-001</field>"))
    assert(result2.contains("<field name=\"filter_bgs_str_mv\">ONL"))

    val result3 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_bgs/zeichnungen.xml"))
    assert(result3.contains("<field name=\"navPublPlace_bgs_str_mv\">ProxyValue: Richterswil, Schweiz</field>"))

    val result4 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_bgs/result-25583780-mods.xml"))
    assert(!result4.contains("<field name=\"navSubform_bgs_str_mv\">ProxyValue: Lesebuch</field>"))
    assert(!result4.contains("<field name=\"navSubform_bgs_str_mv\">ProxyValue: Konferenzschrift</field>"))
    assert(result4.contains("<field name=\"navSubform_bgs_str_mv\">ProxyValue: Buch/Heft</field>"))
    assert(result4.contains("<field name=\"collection_bgs_hierarchy_str_mv\">0/era/</field>"))
    assert(result4.contains("<field name=\"collection_bgs_hierarchy_str_mv\">1/era/era-stp/</field>"))
    assert(result4.contains("<field name=\"collection_bgs_hierarchy_str_mv\">0/stp/</field>"))
    assert(result4.contains("<field name=\"collection_bgs_hierarchy_str_mv\">1/stp/era-stp/</field>"))
    assert(result4.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: era</field>"))
    assert(result4.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: era-stp</field>"))
    assert(result4.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: stp</field>"))
    assert(result4.contains("<field name=\"collection_bgs_str_mv\">ProxyValue: stp-era</field>"))
    //println(result4)
  }

  test ("language") {
    val result1 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record_bgs/result-25583780-mods.xml"))
    assert(result1.contains("<field name=\"language_bgs_strl_mv\">ger</field>"))
    assert(!result1.contains("<field name=\"language\">ger</field>"))

    val result2 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_keller-about.xml"))
    assert(!result2.contains("<field name=\"language_bgs_strl_mv\">ger</field>"))
    assert(result2.contains("<field name=\"language\">ger</field>"))
  }

  test ("portfolio") {
    val result1 = Marc2SearchDocTransform.apply("", getTextFromResourcesFolder("record/han_record_portfolio.xml"))
    assert(!result1.contains("<field name=\"institution\">AFREE</field>"))

  }

}
