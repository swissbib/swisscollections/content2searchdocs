import Dependencies._

ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  }
  else {
    None
  }
}



lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "SOLR SearchDocs transformations",
    assembly / assemblyJarName := "app.jar",
    assembly / mainClass := Some("ch.swisscollections.searchdocs.App"),
    assembly / test := {},
    //assembly / logLevel := Level.Debug,
    assembly / assemblyMergeStrategy := {
      case "log4j.properties" => MergeStrategy.first
      //case "MANIFEST.MF" => MergeStrategy.first
      case "log4j2.xml" => MergeStrategy.first
      //case other if other.contains("module-info.class") => MergeStrategy.discard
      case "org/joda/time/tz/data/Pacific/*" => MergeStrategy.first
      //case x =>
      //  val oldStrategy = (assemblyMergeStrategy in assembly).value
      //  oldStrategy(x)
      case PathList("META-INF", xs @ _*) if xs.exists(_.contains("log4j")) => MergeStrategy.first
      case PathList("META-INF", xs @ _*) if xs.exists(_.contains("Log4j")) => MergeStrategy.first
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard //to get a valid MANIFEST.MF file
      case x => MergeStrategy.first
    },



    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven",
      "Restlet Repository" at "https://maven.restlet.talend.com/"
    ),
    libraryDependencies ++= Seq(
      kafkaStreams,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      memobaseServiceUtils excludeAll (ExclusionRule(organization =
        "org.slf4j"
      )),
      scalatic,
      scalaUri,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test,
      scala_xml,
      saxon,
      solr_analysis,
      mariadb,
      mongoclient,
      snakeyaml
    )
  )

