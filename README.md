# Content2searchdocs

Transform xmlmarc records to xml ready for the indexing in solr. Do a lot of normalization and enrichments.

## Integration Tests

Three external systems are needed : kafka cluster, mariadb (to store fulltexts), mongo (for the gnd) You can do some integration testing manually. In `docker-compose.yml`, you can find a setup for a Kafka Cluster via docker-compose.

How to test the whole system.

Launch the kafka cluster and create the topics

```
make up
```

Produce some test data (wait 10 seconds that the kafka cluster is ready)

```
./scripts/produce-test-data.sh
```

Set the following environment variables

```
MONGO_DB=swisscollections-test
MONGO_URI=mongodb://localhost:27013
JDBCCONNECTION=jdbc:mysql://localhost:3309/swisscollections_fulltexts
KAFKA_BOOTSTRAP_SERVERS=localhost:29092
CLIENT_ID=test
GROUP_ID=test
TOPIC_IN=swisscollections-deduplicated
TOPIC_OUT=swisscollections-solrdoc
TOC_DB_USER=user
TOC_DB_PASSWD=password
MONGO_URI=mongodb://localhost:27013
MONGO_DB=test
APPLICATION_ID=appid
MONGO_COLLECTION=collection
```
TODO mariadb config


Run the program (click the green arrow in Main.scala in IntelliJ) or

```
sbt run
```

check the data in the solr output topic


## Build the container locally

docker build . -t content2searchdocs

docker run -e MONGO_DB=swisscollections-test \
    -e MONGO_URI=mongodb://localhost:27013 \
    -e KAFKA_BOOTSTRAP_SERVERS=localhost:29092 \
    -e CLIENT_ID=test \
    -e GROUP_ID=test \
    -e TOPIC_IN=swisscollections-deduplicated \
    -e TOPIC_OUT=swisscollections-solrdoc \
    -e TOC_DB_USER=user \
    -e TOC_DB_PASSWD=password \
    -e MONGO_DB=test \
    -e APPLICATION_ID=appid \
    -e MONGO_COLLECTION=collection \
content2searchdocs

