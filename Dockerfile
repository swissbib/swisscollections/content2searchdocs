# use multi-stage build, as artefacts are too big for switch gitlab
FROM sbtscala/scala-sbt:eclipse-temurin-jammy-21.0.2_13_1.10.4_3.5.2 AS build

ADD . /
WORKDIR /
RUN sbt -Dsbt.global.base=sbt-cache/sbtboot -Dsbt.boot.directory=sbt-cache/boot -Dsbt.ivy.home=sbt-cache/ivy -Dsbt.rootdir=true assembly

# Build deployable Docker image
FROM eclipse-temurin:21-jre-noble
COPY --from=build target/scala-2.13/app.jar /app/app.jar
CMD java -jar /app/app.jar
