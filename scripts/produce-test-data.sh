# load some data
# with kafka message headers
docker compose exec kafka bash -c "
  kafka-console-producer \
  --bootstrap-server kafka:29092 \
  --topic swisscollections-deduplicated \
  --property 'parse.key=true' \
  --property 'key.separator=|' < records.txt"
