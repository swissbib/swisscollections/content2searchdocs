echo "Waiting for Kafka to come online..."

cub kafka-ready -b kafka:9092 1 20

# delete topics
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-deduplicated \
  --delete

  kafka-topics \
    --bootstrap-server kafka:9092 \
    --topic swisscollections-solrdoc \
    --delete

# create topic
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-deduplicated \
  --replication-factor 1 \
  --partitions 4 \
  --create

kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-solrdoc \
  --replication-factor 1 \
  --partitions 4 \
  --create

sleep infinity
